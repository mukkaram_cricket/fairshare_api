<?php
return [
    'role' => [
        'admin' => '1',
        'user' => '2'
    ],
    'redirect' => [
        'admin' => 1,
        'user' => '/user/wishlist'
    ],
    'date' => [
        'Ymd' => 'Y-m-d'
    ],
    'exception' => [
        '400' => 'Something went wrong'
    ],
    'user_invitation_status' => [
        'log' => 'login',
        'reg' => 'register'
    ],
    'messages' => [
        'invitation' => 'Due to invalid Information reference detail not saved!',
    ],
    'status' => [
        'active' => 1,
        'inActive' => 0,
    ]
];
