<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessEmail implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $tries = 5;
    public $email_data = '';
    public $email_type = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_type,$email_data) {
        $this->email_type = $email_type;
        $this->email_data = $email_data;
    }

    /**
     * Execute the job.
     *
     */
    public function handle() {
        send_email($this->email_type,$this->email_data);
    }

}
