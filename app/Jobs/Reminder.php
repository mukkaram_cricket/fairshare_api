<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \App\Traits\CommonTraits;
use Carbon\Carbon;

class Reminder implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        CommonTraits;

    public $tries = 5;
    protected $user_id;
    protected $job_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->user_id = $data['user_id'];
        $this->job_id = $data['job_id'];
    }

    /**
     * Execute the job.
     *
     */
    public function handle() {
        $job = \App\Models\Job::find($this->job_id);
        $today = Carbon::today();
        $end = Carbon::parse($job->end_date);
        $duration = $end->diffInDays($today);
        if ($today <= $job->end_date) {
            $this->saveNotification($this->user_id, 'job_reminder', 'Job Reminder', 'Job Reminder: ' . $job->title . " will expire after " . $duration . " Days", $this->job_id, 'generals', '', '', '{"slug":"' . $job->slug . '"}');
            if ($today < $job->end_date) {
                Reminder::dispatch(['user_id' => $this->user_id, 'job_id' => $this->job_id,])->onQueue('low')->delay(now()->addMinutes(1440));
            }
        }
    }

}
