<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Video;

class UploadVideoToS3 implements ShouldQueue {

    use \App\Traits\CommonTraits;
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $tries = 5;
    public $video_file_name = '';
    public $video_id = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $video_id) {
        $this->video_file_name = $name;
        $this->video_id = $video_id;
    }

    /**
     *
     * @return void
     */
    public function handle() {
        $ext = pathinfo($this->video_file_name, PATHINFO_EXTENSION);
        $awsVideoId = $this->video_id . "-" . str_random(6);
        $fileToUpload = storage_path(config('app.images_relative_path') . '/videos/' . $this->video_file_name);
        $s3 = \Illuminate\Support\Facades\Storage::disk('s3');
        $destinationPath = config('app.S3_DOMAIN') . "/{$awsVideoId}.{$ext}";
        $s3->put($destinationPath, file_get_contents($fileToUpload), 'public');
        $video = Video::find($this->video_id);
        $video->aws_video_id = $awsVideoId;
        $video->save();
        unlink($fileToUpload);
    }

}
