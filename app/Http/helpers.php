<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Twilio\Rest\Client;

//use Aws\S3\MultipartUploader;
//use Aws\Exception\MultipartUploadException;

function prepareResult($status, $data, $msg) {
    header(200);
    header("Content-Type: application/json; charset=UTF-8", true);
    header("pm_version: " . config('app.pm_version', '1.0'), true);
    $response = ['status' => $status, 'message' => $msg, 'data' => clearNull($data)];
    //--------------Simple Cache Technique----------- after version 7.0
//    $request = request();
//    if ($request->cache_key) {
//        Redis::command('SETEX', [$request->cache_key, $request->cache_expiry, json_encode($response)]);
//    }
    //-------------------Creating Cache (if)-------------------
//    $request = request();
//    if ($request->cache_key) {
//        $expiry = $request->cache_expiry ? 'EX ' . $request->cache_expiry : '';
//        Redis::command('SET', [$request->cache_key, json_encode($response)], $expiry);
//    }
    //----------------------------------------------------
    return $response;
}

/**
 * Return from cache, if exists. Otherwise, generate cache key.
 *
 * @param string $tag Tag is used to group similar APIs. e.g Settings for all user setting APIs. Cache will not be set if empty.
 * @param int $sport_id Sport ID only used in specific cache which are sport dependent.
 * @param int $expiry Sets the expiry of cache. Two possible formats "EX seconds" or "PX milliseconds".
 * @param boolean $has_params Whether to include parameters in cache key.
 * @param boolean $global Whether cache is independent of user or not.
 * 
 * @return bool True if cache is bypassed.
 */
function checkCache($cache_key = '', $expiry = 86400) {
    if (!config('app.enable_cache') || $cache_key === '') {
        return true;
    }
//    $response = Redis::get($cache_key);
//    if ($response) {
//        response(json_decode($response, true), 200, ['Content-Type' => 'application/json; charset=UTF-8', 'pm_version' => config('app.pm_version', '1.0')])->send();
//        die;
//    }
//    $request = request();
//    $request->cache_key = $cache_key;
//    $request->cache_expiry = $expiry;
}

function getMultiHashCache($hash = '', $keys = []) {
    if (!config('app.enable_cache') || $hash == '') {
        return false;
    }
    $values = Redis::hmget($hash, $keys);
    if ($values) {
        $data = [];
        foreach ($values as $value) {
            $data[] = json_decode($value);
        }
        return $data;
    }
    return false;
}

//hgetall data_by_country_167
function setMultiHashCache($hash = '', $key_values = []) {
    if (!config('app.enable_cache') || $hash == '' || empty($key_values)) {
        return false;
    }
    $data = [];
    foreach ($key_values as $key => $value) {
        $data[$key] = json_encode($value);
    }
    $response = Redis::hmset($hash, $data);
    if ($response) {
        return $response;
    }
    return false;
}

function getHashCache($hash = '', $key = '') {
    if (!config('app.enable_cache') || $hash == '' || $key == '') {
        return false;
    }
    $value = Redis::hget($hash, $key);
    if ($value) {
        return json_decode($value);
    }
    return false;
}

function setHashCache($hash = '', $key = '', $value = []) {
    if (!config('app.enable_cache') || $hash == '' || $key == '' || empty($value)) {
        return false;
    }
    $response = Redis::hset($hash, $key, json_encode($value));
    if ($response) {
        return $response;
    }
    return false;
}

function delHashCache($hash = '', $keys = []) {
    if (!config('app.enable_cache') || $hash == '') {
        return false;
    }
    Redis::hdel($hash, ...$keys);
    return true;
}

function getCache($cache_key = '') {
    if (!config('app.enable_cache') || $cache_key === '') {
        return false;
    }
    $response = Redis::get($cache_key);
    if ($response) {
        return $response;
    }
    return false;
}

function setCache($cache_key = '', $data = array(), $expiry = 86400) {
    if (!config('app.enable_cache') || $cache_key === '' || empty($data)) {
        return false;
    }
    $cache_expiry = $expiry ? 'EX ' . $expiry : '';
    Redis::command('SET', [$cache_key, $data], $cache_expiry);
}

/**
 * Delete caches by matching keys criteria.
 *
 * @param string $criteria Key format for criteria is
 *  "{$tag}|{$user_id}|{$sport_id}|{$api_path}|{$lang}|{array of params}".
 */
function deleteCache($criteria = '') {  //$criteria => "{$tag}|{$user_id}|{$sport_id}|{$api_path}|{$lang}*"
    if (!config('app.enable_cache')) {
        return true;
    }
    $keys = Redis::command('KEYS', [$criteria]);
    deleteCacheByKeys($keys);
}

function deleteCacheByKeys($keys = []) {  //$criteria => "{$tag}|{$user_id}|{$api_path}|{$lang}*"
    if (!config('app.enable_cache') || empty($keys)) {
        return true;
    }
    Redis::command('DEL', $keys);
}

function flushCache() {  //$criteria => "{$tag}|{$user_id}|{$api_path}|{$lang}*"
    if (!config('app.enable_cache')) {
        return true;
    }
    Redis::flushall();
}

function clearNull($data) {
    if (request()->header('Device-Type', 'web') != "web") {
        return json_decode(json_encode($data));
    }
    return json_decode(preg_replace('/(\s*"[^"]+")\s*:\s*null/', '$1:""', json_encode($data)));
}

function clearNull2($data) {
    if (request()->header('Device-Type', 'web') != "web") {
        return $data;
    }
//    $data = json_decode(json_encode($data), true);
    $data_type = gettype($data) == 'object' ? 'object' : 'array';
    $data = ($data_type == 'object') ? get_object_vars($data) : $data;
    foreach ($data as $key => $value) {
        if (is_array($value) || is_object($value)) {
            if ($data_type == 'array') {
                $data[$key] = clearNull($value);
            } else {
                $data->$key = clearNull($value);
            }
        }
        if (is_null($value)) {
            if ($data_type == 'array') {
                $data[$key] = '';
            } else {
                $data->$key = '';
            }
        }
    }
    return $data;
}

function type_cast_to_string($data) {
    $data_type = gettype($data);
    foreach ($data as $key => $value) {
        if (is_array($value) || is_object($value)) {
            if ($data_type == 'array') {
                $data[$key] = type_cast_to_string($value);
            } else {
                $data->$key = type_cast_to_string($value);
            }
        } elseif (gettype($value) != 'string') {
            if ($data_type == 'array') {
                $data[$key] = (string) $data[$key];
            } else {
                $data->$key = (string) $data->$key;
            }
        }
    }
    return $data;
}

function search_field_options($fieldOptions, $label) {
    foreach ($fieldOptions as $option) {
        if ($option['label'] == $label) {
            return $option['key'];
        }
    }
    return false;
}

//-------------------------------------
function encryptionAlgo($string, $action) {
    $secret_key = 'Dra53Gd7VBdh';
    $secret_iv = 'HSJnfsd675';
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'e') {
        $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
    } else if ($action == 'd') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function encrypted($string) {
    return $string ? encryptionAlgo($string, 'e') : "";
}

function decrypted($string) {

    return $string ? encryptionAlgo($string, 'd') : "";
}

//function translateColumn($as = 'name') {
//    $locale = config('app.locale');
//    if ($locale != 'en_US') {
//        return "name_{$locale} as {$as}";
//    }
//    return 'name';
//}
//------------------------------------

function send_email($view, $data) {
    $language = $temp = '';
    set_time_limit(500);
//    $filename = 'C:/xampp/htdocs/images/Under16ParentConsentForm.docx';
    $filename = storage_path(config('app.images_relative_path') . '/file/' . 'UnderAgeParentConsentForm.docx');
    if ($view != "email_change" && $view != 'contact_us_email' && $view != 'contact_us_thanks_email' && $view != 'email_invitation') {
        $language = \App\Models\User::where('email', encrypted($data['to']))->first(['locale'])->locale;
    }
    if ($language) {
        $temp = App::getLocale();
        App::setLocale($language);
    }
    \Mail::send($view, $data, function($message) use ($data, $language, $temp, $filename, $view) {
//            $message->getHeaders()->addTextHeader('Reply-To', $replyTo);
//            $message->getHeaders()->addTextHeader('Return-Path', $replyTo);
        $message->to($data['to']);
        $message->subject($data['subject']);
        $message->from('info@playermatch.com');
        $message->sender('info@playermatch.com', 'PlayerMatch');

        if ($language) {
            App::setLocale($temp);
        }
        if ($view == 'under_age_email') {
            $message->attach($filename);
        }

        if ($message) {
            return true;
        } else {
            return false;
        }
    });
}

function string_remove_space($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function isClubOfficial($club_id, $user_id) {
    return \App\Models\ClubOfficial::where(['club_id' => $club_id, 'user_id' => $user_id])->whereRaw("(status = 'Accepted' OR status = 'Author')")->exists();
}

// chunk uploading to S3 for 5GB greater file
//function uploadToS3($sourceFile, $destinationFile) {
//    $s3 = \Illuminate\Support\Facades\Storage::disk('s3');
//    $uploader = new MultipartUploader($s3, $sourceFile, [
//        'bucket' => 'playermatch-bucket',
////        'bucket' => config('filesystems.disks.s3.key'),
//        'key' => $destinationFile,
//    ]);
//    try {
//        $result = $uploader->upload();
//        logger("Upload complete: {$result['ObjectURL']}");
//    } catch (MultipartUploadException $e) {
//        echo $e->getMessage() . "\n";
//    }
//}
//--------------------------Simple Cache Technique-------------------------------
function isCached($tag = 'all', $expiry = 86400, $global = false) {
    $request = request();
    $user_id = $global ? 0 : $request->loginUserId;
    $lang = $request->loginUser ? $request->loginUser->locale : 'en_US';
    $cache_key = "{$tag}|{$user_id}|{$lang}|{$request->path()}|" . json_encode($request->all());
    $response = Redis::get($cache_key);
    if ($response) {
        response(json_decode($response, true), 200, ['Content-Type' => 'application/json; charset=UTF-8', 'pm_version' => config('app.pm_version', '1.0')])->send();
        die;
    }
    $request->cache_key = $cache_key;
    $request->cache_expiry = $expiry;
}

function removeCache($criteria = '') {  //$criteria => "{$tag}|{$user_id}|{$api_path}*"
    $keys = Redis::command('KEYS', [$criteria]);
    if ($keys) {
        Redis::command('DEL', $keys);
    }
}

function imagesPath($path = '') {
    return config('app.img_url') . "/" . $path;
}

function getRelativeImagePath($path = '') {
    return ltrim($path, config('app.img_url') . "/");
}

function getObjectById($arrayOfObjects, $needle, $key = 'id') {
    $neededObject = array_values(array_filter(
                    $arrayOfObjects, function ($e)use($needle, $key) {
                return $e[$key] == $needle;
            }
    ));
    return count($neededObject) > 0 ? $neededObject[0] : Null;
}

function utc($dateTimeString) {
    return \Carbon\Carbon::parse($dateTimeString, config('app.timezone'))->setTimezone('UTC')->toDateTimeString();
}

function convertHeight($value, $unit) {
    if ($unit == 'cm') {
        return $value;
//        return $value * 2.54;
    }
    if ($unit == 'inch') {
        return $value;
//        return $value / 2.54;
    }
}

function convertWeight($value, $unit) {
    if ($unit == 'kgs') {
        return $value;
//        return $value / 2.205;
    }
    if ($unit == 'lbs') {
        return $value;
//        return $value * 2.205;
    }
}

function makeLinksToAnchor($string) {
    $regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
    $regex .= "(\:[0-9]{2,5})?"; // Port 
    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 

    return preg_replace_callback("/^$regex$/i", function($matches) {
        $link_formated = $link = $matches[0];
        if (substr($link, 0, 28) == 'src="//www.youtube.com/embed') {
            return $link;
        }
//        $link_formated = "//" . ltrim(ltrim($link_formated, "https://"), "http://");
        $re = '/^(?:https?:\/\/)?(?:www\.)?/i';
        $x = preg_replace($re, '', $link_formated);
        return "<a href='//$x' target='_blank'>$x</a>";
    }, $string);
}

function convertToPhoneFormat($phone) {
//    $phone = str_replace("+", "00", $phone);
    return trim(preg_replace("/[() -]/i", "", $phone));
}

function generateActiviationKey() {
    $characters = '0123456789';
    $activation_key = '';
    for ($i = 0; $i < 6; $i++) {
        $activation_key .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $activation_key;
}

function sendActivationKeyViaSMS($to) {
    $activation_key = generateActiviationKey();
    Cache::put($to, $activation_key, 10);
    $AccountSid = config('app.twillio_id');
    $token = config('app.twillio_token');
    $twilio_api = new Client($AccountSid, $token);
    $twilio_api->messages->create(
            $to, array(
        'from' => config('app.twillio_phone_no'),
        'body' => $activation_key . ' ' . __('is your account activation code for Playermatch'),
            )
    );
}

function sendSMS($to, $text) {
    try {
        $AccountSid = config('app.twillio_id');
        $token = config('app.twillio_token');
        $twilio_api = new Client($AccountSid, $token);
        $twilio_api->messages->create(
                $to, array(
            'from' => config('app.twillio_phone_no'),
            'body' => $text,
                )
        );
    } catch (\Exception $exc) {
        return prepareResult(422, new stdClass(), 'Invalid Phone Number');
    }
}


function merge_user_data($data=array(),$request){

    $result = array(
        'user_id' => $data['user_id']
    );

    $result = array_merge($result,$request);
    return $result;
}

?>
