<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserWishlistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:user_wishlists,name',
            'date' => 'required|date_format:'.config('constants.date.Ymd')
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'name.unique' => 'The name has already been used.',
            'date.required' => 'Date is required!',
            'date.date_format' => 'Date format Invalid!',
        ];
    }
}
