<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'email' => '',
            'hashed_email' => 'unique:users,email',
            'password' => 'required|min:8|string|regex:/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*).{8,}$/',
            'role' => 'required|exists:roles,id',
        ] ;
    }


}
