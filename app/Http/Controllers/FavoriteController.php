<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;
class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request['product_id']){
        $result = $request->all();
        $data_exist = Favorite::where($result)->exists();

        if(empty($data_exist)){
            Favorite::create($result);
        }
        $count = Favorite::where($result)->count();
        return prepareResult(200, ['data' => $count], "Success");
        }
        else{
            return prepareResult(404,['data' => 'Missing Parameters'], "Error");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {


        $favorites = Favorite::select('product_id')->where('user_id',$request->loginUserId)->get()->toArray();
        $products = Product::with('productImages')->whereIn('id',$favorites)->get();
        return prepareResult(200,['products' => $products], 'success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorite $favorite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $result = $request->all();
        Favorite::where($result)->delete();
        $count = Favorite::where($result)->count();
        return prepareResult(200, ['data' => $count], "Success");
    }
}
