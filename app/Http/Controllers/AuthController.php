<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\UserInvitationHistory;
use App\Models\UserWishlist;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserApi;
use Hash;
use \stdClass;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rule;

class AuthController extends Controller {

    private function generateToken(Request $request, $user) {
        $useragent = $request->header('User-Agent');
        $device = $request->header('Device-Type', 'web');
        $randomString = str_random(30);
        $token = base64_encode($user->id . ':' . $randomString);
        UserApi::insert(['user_id' => $user->id, 'fcm_token' => $request->fcm_token, 'token' => $randomString, 'status' => 1, 'login_type' => $device, 'user_agent' => $useragent, 'timezone' => $request->get('timezone', 'utc'), 'last_used' => now('utc')]);
        return $user_data = [
            'id' => $user->id,
            'token' => $token,
            'fcm_token' => $request->fcm_token,
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'slug' => $user->slug,
            'avatar' => $user->avatar,
            'in_panel' => $user->in_panel,
            'role_id' => $user->role_id,
            'role' => $user->role,
            'locale' => $user->locale,
            'is_verified' => $user->status == 'Active',
            'is_email_verified' => $user->is_email_verified,
            'is_phone_verified' => $user->is_phone_verified
        ];
    }

    public function login(Request $request) {

        //        $user_token = $request['user_token'] ?: '5dcc07af5e7f6';
//        $wishlist_name   = $request['wishlist'] ?: 'new name';
//
        $user_token      = $request['user_token'] ?: '';
        $wishlist_name   = $request['wishlist'] ?: '';

        $emailLogin = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
        $rules['email'] = $emailLogin ? 'required|email' : 'required|string|phone';
        $this->validate($request, $rules + [
            'password' => 'required|string',
                ], [], ['email' => 'phone', 'hashed_email' => 'phone']);

        $user = User::where("email", $request->hashed_email)->orWhere('phone', $request->hashed_email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            if ($user->status == 'Active' && (($emailLogin && $user->is_email_verified) || (!$emailLogin && $user->is_phone_verified) )) {
                $user_data = $this->generateToken($request, $user);

                /*
                 * Invitaion details same for register
                 */
                $invitation_result = '';
                if($user_token && $wishlist_name){
                    $invitation_data['user_token']      = $user_token;
                    $invitation_data['wishlist_name']   = $wishlist_name;
                    $invitation_data['user_id']         = $user_data['id'];
                    $invitation_data['type']            = config('constants.user_invitation_status.log');
                    $invitation_result                  = $this->user_invitation_save($invitation_data);
                }

                $data_meg               = new stdClass();
                $data_meg->user         = $user_data;
                $data_meg->invitaion    = $invitation_result;
//                return prepareResult(200, $data_meg, "success");

                return prepareResult(200, $data_meg, "success");
            } else {
                return prepareResult(200, ['user' => ['email' => $request->email, 'phone' => $request->email, 'is_verified' => false]], 'User not verified yet!');
            }
        } else {
            return prepareResult(402, new stdClass, "Invalid Credentails!");
        }
    }

    public function logoutApi(Request $request) {
        $user_session = UserApi::where(["user_id" => $request->loginUserId, 'token' => $request->loginUserToken, 'status' => 1])->first();
        if ($user_session) {
            $user_session->delete();
            return prepareResult(200, new stdClass, __("Successfully logout"));
        } else {
            return prepareResult(400, new stdClass, "Some thing went wrong");
        }
    }

    public function signup(UserRequest $request) {

//        $user_token = $request['user_token'] ?: '5dcc07af5e7f6';
//        $wishlist_name   = $request['wishlist'] ?: 'new name';
//
        $user_token      = $request['user_token'] ?: '';
        $wishlist_name   = $request['wishlist'] ?: '';


        $user = User::updateOrCreate([
                    'name' => $request->first_name,
                    'slug' => '',
                    'status' => 'Active',
                    'is_email_verified' => 1,
                    'is_phone_verified' => 1,
                    'email' => $request->hashed_email,
                    'role_id' => $request->role,
                    'user_token' => uniqid(),
                    'password' => bcrypt($request['password']),
                    'locale' => $request->get('language', 'en_US')
        ]);
        $user->slug = str_slug($user->name . "-" . $user->id);
        $user->save();

        /*
        * User register for someone wishlist
        * add user_id and new register user_id in user_invitation_history
        */

        $last_insert_id = $user->id;
        if($user_token && $wishlist_name){
            $invitation_data['user_token']      = $user_token;
            $invitation_data['wishlist_name']   = $wishlist_name;
            $invitation_data['user_id']        = $last_insert_id;
            $invitation_data['type']            = config('constants.user_invitation_status.reg');
            $invitation_result                  =  $this->user_invitation_save($invitation_data);
        }

        $data_meg               = new stdClass();
        $data_meg->register     = 'User Registered successfully';
        $data_meg->invitaion    = $invitation_result;
        return prepareResult(200, $data_meg, "success");

        /*         * * OLD ** */
        /*
          $this->validate($request, [
          'first_name' => 'required|string|max:255',
          'email' => '',
          'hashed_email' => 'unique:users,email',
          'phone' => 'required|string|phone',
          'hashed_phone' => Rule::unique('users', 'phone')->where(function ($query) use ($request) {
          return $query->where('is_phone_verified', 1);
          }),
          'password' => 'required|min:8|string|regex:/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*).{8,}$/',
          'sport' => 'required|exists:sports,id',
          'role' => 'required|exists:roles,id',
          ], ['hashed_phone.unique' => 'Your phone number is already registered please login']);
          //------------Register via Email-------------------------------------
          $activation_key = generateActiviationKey();
          Cache::put($request->email, $activation_key, 10);
          $unsub_link = config('app.VUE_APP_URL') . '/users/' . $user->slug . '/settings/notifications';
          $data_var = [
          'name' => $request->first_name,
          'link' => $activation_key,
          'subject' => 'Email Verification',
          'to' => $request->email,
          'unsub_link' => $unsub_link
          ];
          send_email('mail', $data_var);
          //--------------------Register via Phone-----------------------------
          try {
          sendActivationKeyViaSMS($request['phone']);
          } catch (\Exception $exc) {
          return prepareResult(422, new stdClass(), 'Invalid Phone Number');
          }
          //----------------------------------------------

          $user = User::updateOrCreate(['phone' => $request->hashed_phone], [
          'name' => $request->first_name,
          'slug' => '',
          'email' => $request->hashed_email,
          'role_id' => $request->role,
          'sport_id' => $request->sport,
          'status' => 'InActive',
          'password' => bcrypt($request['password']),
          'locale' => $request->get('language', 'en_US'),
          'user_type' => 'real',
          ]);
          $user->slug = str_slug($user->name . "-" . $user->id);
          $user->save();

          return prepareResult(200, new stdClass(), "User Registered successfully");
         */
    }

    /*
     * user_invitation_save get array of data
     * Save user reference details againts the wishllist
     *
     */

    public function user_invitation_save($data=array())
    {

        extract($data);
        $invitation_status = '';
        if($user_token && $wishlist_name && $user_id){

            //Check token exist then get user_id
            $reference_id = User::where('user_token',$user_token)->get()->first();
            if($reference_id) {
                $reference_id = $reference_id->id;
                if ($reference_id) {

                    //get wishlist name against user_id and wishlist name
                    $wishlist_name_id = UserWishlist::where(['name' => $wishlist_name, 'user_id' => $reference_id])->get()->first();
                    if ($wishlist_name_id) {

                        $wishlist_name_id = $wishlist_name_id->id ?: '';
                    }
                }
                //user_id and wishlist exist then save invitation detail

                //user_id and reference_id not be same. It means user login with his own token
                if ($reference_id != $user_id) {

                    if ($reference_id && $wishlist_name_id) {
                        $invitation_create = array(
                            'user_id' => $user_id,
                            'reference_id' => $reference_id,
                            'user_wishlist_id' => $wishlist_name_id,
                            'type' => $type
                        );
                        $invitation_status = UserInvitationHistory::updateOrCreate($invitation_create);
                        return $invitation_status->save();

                    } else {
                        $invitation_status = config('constants.messages.invitation');
                    }
                }
                else{
                    $invitation_status = false;
                }
            }
            else{
                $invitation_status = false;
            }

            return $invitation_status;
        }


    }
    public function email_verification(Request $request) {
        $emailLogin = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
        $rules['email'] = $emailLogin ? 'required|email' : 'required|string|phone';
        $this->validate($request, $rules + [
            'key' => 'required',
                ], [], ['email' => 'phone', 'hashed_email' => 'phone']);

        if (Cache::has($request->email) && Cache::get($request->email) == $request->key) {
            $user = User::where('email', $request->hashed_email)->orWhere('phone', $request->hashed_email)->first();
            $user->status = 'Active';
            $user->register_date = now('utc');
            if ($emailLogin) {
                $user->is_email_verified = 1;
            } else {
                $user->is_phone_verified = 1;
            }
            $user->save();
            $user_data = $this->generateToken($request, $user);
            return prepareResult(200, ["user" => $user_data], "Account Verification Completed");
        }
        return prepareResult(401, new stdClass, __("Verification Code is Invalid Or Expired"));
    }

    public function resend_email_verification(Request $request) {
        $emailLogin = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
        $rules['hashed_email'] = $emailLogin ? 'required|exists:users,email' : 'required|exists:users,phone';
        $this->validate($request, $rules, [], ['email' => 'phone', 'hashed_email' => 'phone']);

        $user = User::where('email', $request->hashed_email)->orWhere('phone', $request->hashed_email)->first();
        //----------------------------------------------------
        if ($emailLogin) {
            $activation_key = generateActiviationKey();
            Cache::put($request->email, $activation_key, 10);
            $unsub_link = config('app.VUE_APP_URL') . '/users/' . $user->slug . '/settings/notifications';
            $data_var = [
                'name' => $user->name,
                'code' => $activation_key,
                'subject' => 'Email Verification',
                'to' => $user->email,
                'unsub_link' => $unsub_link
            ];
            send_email('email_verification', $data_var);
            $responce_msg = "Verification code Sent On Your Email";
        } else {
            try {
                sendActivationKeyViaSMS($user->phone);
                $responce_msg = "Verification code Sent On Your Phone Number";
            } catch (\Exception $exc) {
                return prepareResult(422, new stdClass(), 'Invalid Phone Number');
            }
        }
        return prepareResult(200, new stdClass, __($responce_msg));
    }

    public function forget_password(Request $request) {
        $emailLogin = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
        $rules['hashed_email'] = $emailLogin ? 'required|exists:users,email' : 'required|exists:users,phone';
        $this->validate($request, $rules, [], ['email' => 'phone', 'hashed_email' => 'phone']);
        //-------------------------------------------------
        $user = User::where('email', $request->hashed_email)->orWhere('phone', $request->hashed_email)->first();
        $responce_msg = '';
        if ($emailLogin && $user->is_email_verified) {
            $activation_key = generateActiviationKey();
            Cache::put($request->email, $activation_key, 10);
            $unsub_link = config('app.VUE_APP_URL') . '/users/' . $user->slug . '/settings/notifications';
            $data_var = [
                'name' => $user->name,
                'link' => $activation_key,
                "subject" => "Forget password",
                "to" => $user->email,
                'unsub_link' => $unsub_link
            ];
            send_email('forget_password', $data_var);
            $responce_msg = "Password Reset Code Sent On Your Email";
        } else if (!$emailLogin && $user->is_phone_verified) {
            sendActivationKeyViaSMS($request->email);
            $responce_msg = 'Password Reset Code Sent On Your Phone Number';
        }
        return prepareResult(200, new stdClass(), $responce_msg);
    }

    public function reset_password(Request $request) {
        $emailLogin = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
        $rules['hashed_email'] = $emailLogin ? 'required|exists:users,email' : 'required|exists:users,phone';
        $this->validate($request, $rules + [
            'key' => 'required|string',
            'password' => 'required|min:8|string|regex:/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*).{8,}$/|confirmed',
                ], [], ['email' => 'phone', 'hashed_email' => 'phone']);

        if (Cache::has($request->email) && Cache::get($request->email) == $request->key) {
            $user = User::where(['email' => $request->hashed_email])->orWhere('phone', $request->hashed_email)->first();
            $user->password = bcrypt($request->password);
            if ($emailLogin) {
                $user->is_email_verified = 1;
            } else {
                $user->is_phone_verified = 1;
            }
            $user->save();
            $user_data = $this->generateToken($request, $user);
            return prepareResult(200, ["user" => $user_data], "Your password has been reset successfully!");
        } else {
            return prepareResult(401, new stdClass, __("Verification Code is Invalid Or Expired"));
        }
    }

    public function ActivateUser(Request $request) {
        $this->validate($request, [
            'slug' => 'required|exists:users,slug',
        ]);
        if ($request->loginUser->is_pm_admin) {
            User::where('slug', $request->slug)->update(['status' => 'active']);
            return prepareResult(200, new stdClass, "User activated");
        }
    }

    public function UpdateUser(Request $request) {
        $this->validate($request, [
            'sport_id' => 'exists:sports,id',
            'role_id' => 'exists:roles,id',
        ]);
        if ($request->sport_id) {
            $request->loginUser->sport_id = $request->sport_id;
        }
        if ($request->role_id) {
            $request->loginUser->role_id = $request->role_id;
        }
        $request->loginUser->save();
        $data['user_data'] = [
            'role_id' => $request->loginUser->role_id,
            'role' => $request->loginUser->role,
            'sport_id' => $request->loginUser->sport_id,
            'sport' => $request->loginUser->sport,
        ];
        return prepareResult(200, $data, "");
    }


}
