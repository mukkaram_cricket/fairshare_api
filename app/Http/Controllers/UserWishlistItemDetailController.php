<?php

namespace App\Http\Controllers;

use App\UserWishlistItemDetail;
use Illuminate\Http\Request;

class UserWishlistItemDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserWishlistItemDetail  $userWishlistItemDetail
     * @return \Illuminate\Http\Response
     */
    public function show(UserWishlistItemDetail $userWishlistItemDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserWishlistItemDetail  $userWishlistItemDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(UserWishlistItemDetail $userWishlistItemDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserWishlistItemDetail  $userWishlistItemDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserWishlistItemDetail $userWishlistItemDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserWishlistItemDetail  $userWishlistItemDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserWishlistItemDetail $userWishlistItemDetail)
    {
        //
    }
}
