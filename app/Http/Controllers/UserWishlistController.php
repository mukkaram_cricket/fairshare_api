<?php

namespace App\Http\Controllers;


use App\Http\Requests\UserWishlistRequest;
use App\Models\Base\UserInvitationHistory;
use Illuminate\Http\Request;
use App\Models\UserWishlist;

class UserWishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//

        $allowed_ids = UserInvitationHistory::where('user_id',$request->loginUserId)->pluck('user_wishlist_id')->toArray();
        $result['my_wishlist'] = UserWishlist::whereIn('id',$allowed_ids)->orwhere('user_id',$request->loginUserId)->get();

        return prepareResult(200, $result, "Success");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(UserWishlistRequest $request)
    {

            $validated_data = $request->validated();
            UserWishlist::create($request->all());
            return prepareResult(200, 'true', "Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserWishlist  $userWishlist
     * @return \Illuminate\Http\Response
     */
    public function show(UserWishlist $userWishlist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserWishlist  $userWishlist
     * @return \Illuminate\Http\Response
     */
    public function edit(UserWishlist $userWishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserWishlist  $userWishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserWishlist $userWishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserWishlist  $userWishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserWishlist $userWishlist)
    {
        //
    }


    /*
     * Show all the wishlist name of users
     *
     */
    public function get_user_wishlist(Request $request)
    {
        $result['my_wishlist'] = UserWishlist::select('name','id')
            ->where(['user_id'=> $request->loginUserId, 'status' => config('constants.status.active')])->get();
        return prepareResult(200, $result, "Success");
    }
}
