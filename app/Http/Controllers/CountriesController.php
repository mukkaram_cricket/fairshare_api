<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\City;
use App\Models\AllClub;
use App\Models\League;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \stdClass;

class CountriesController extends Controller {

    public function GetCountries(Request $request) {
        $result['countries'] = Country::all();
        return prepareResult(200, $result, "Success");
    }

    public function getCities(Request $request) {
        $this->validate($request, [
            'country_id' => 'required',
        ]);
        $result['cities'] = City::whereCountryId($request->country_id)->get(['id', 'name']);
        return prepareResult(200, $result, "Success");
    }

    public function searchCities(Request $request) {
        $this->validate($request, [
            'criteria' => 'required|min:3',
        ]);
        $result['cities'] = City::basedOnCriteria()->with('country:id,name,code')->take(20)->get(['id', 'name', 'country_id']);
        return prepareResult(200, $result, "Success");
    }

    public function GetData(Request $request) {
        $this->validate($request, [
            'related_id' => 'required|exists:countries,id',
            'params' => 'required|array|min:1',
            'params.*' => 'required|string',
        ]);
        $hash = 'data_by_country_' . $request->related_id;
        $keys = [];
        $country_data_array = array(
            'city' => array(
                'key' => 'city_' . $request->loginUser->locale,
                'value' => [],
                'query' => function () use ($request) {
                    return City::where('country_id', $request->related_id)->orderBy('name', 'asc')->select(['id as key', 'name as label'])->get()->toArray();
                }
            ),
            'club' => array(
                'key' => "club_{$request->loginUser_sport_id}",
                'value' => [],
                'query' => function () use ($request) {
                    return AllClub::where('sport_id', $request->loginUser_sport_id)->where('country_id', $request->related_id)->get(['id as key', 'name as label'])->toArray();
                }
            ),
            'league' => array(
                'key' => "league_{$request->loginUser_sport_id}",
                'value' => [],
                'query' => function () use ($request) {
                    return League::where('sport_id', $request->loginUser_sport_id)->where('country_id', $request->related_id)->get(['id as key', 'name as label'])->toArray();
                }
            ),
        );
        for ($index = 0; $index < count($request->params); $index++) {
            $keys[$index] = $country_data_array[$request->params[$index]]['key'];
        }
        $values = getMultiHashCache($hash, $keys);
        if ($values) {
            for ($index = 0; $index < count($request->params); $index++) {
                if ($values[$index]) {
                    $country_data_array[$request->params[$index]]['value'] = $values[$index];
                }
            }
        }
        $cache_keys = [];
        $result = new stdClass();
        foreach ($request->params as $param) {
            if ($country_data_array[$param]['value']) {
                $result->{$param} = $country_data_array[$param]['value'];
            } else {
                $result->{$param} = $country_data_array[$param]['query']();
                $cache_keys[$country_data_array[$param]['key']] = $result->{$param};
            }
        }
        if ($cache_keys) {
            setMultiHashCache($hash, $cache_keys);
        }
        return prepareResult(200, $result, "Success");
    }

}
