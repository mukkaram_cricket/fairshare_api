<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class NotificationsController extends Controller {

    public function markAsRead(Request $request) {
        $this->validate($request, [
            'notification_id' => 'required|exists:notifications,id'
        ]);
        Notification::where(['user_id' => $request->loginUserId, 'id' => $request->notification_id])->update(['is_read' => 1]);
        return prepareResult(200, new \stdClass(), 'Notification Marked As Read');
    }

    public function markAllAsViewed(Request $request) {
//        $this->validate($request, [
//            'category' => 'required'
//        ]);
        Notification::where(['user_id' => $request->loginUserId, 'is_viewed' => 0, ['module', 'LIKE', $request->get('category', '%%')]])->update(['is_viewed' => 1]);
        delHashCache('notifications', ['count_' . $request->loginUserId]);
        return prepareResult(200, new \stdClass(), 'Notifications Marked As Viewed');
    }

    public function getNotifications(Request $request) {
        checkCache();
//        $this->validate($request, [
//            'category' => 'in:generals,friends,messages'
//            order_by
//        ]);
        $notifications = Notification::where(['user_id' => $request->loginUserId, ['module', 'LIKE', $request->get('category', '%%')]]);
        $notifications = ($request->order_by == 'oldest') ? $notifications->oldest('id') : $notifications->latest('id');
        $notifications = $notifications->with('sender:id,name,slug,avatar')->paginate(PAGINATION_PER_PAGE);
        Notification::where(['user_id' => $request->loginUserId, 'is_viewed' => 0, ['module', 'LIKE', $request->get('category', '%%')]])->update(['is_viewed' => 1]);
        delHashCache('notifications', ['count_' . $request->loginUserId]);

        $result = $notifications->toArray();
        $notifications = $result['data'];

        foreach ($notifications as $key => $notification) {
            switch ($notification['type']) {
                case 'feed_like':
                case 'feed_rating':
                case 'feed_comment':
                case 'feed_taged':
                case 'comment_reply':
                case 'request_accepted':
                case 'friend_request':
                case 'new_msg':
                case 'msg_reply':
                case 'skill_endorsment':
                case 'recommendation':
                case 'job_apply':
                case 'profile_view':
                case 'club_official_req':
                case 'job_invitation':
                case 'official_req_accepted':
                case 'comment_mention':
                case 'match_request':
                case 'match_request_accepted':
                case 'match_request_rejected':
                case 'player_invite_match':
                case 'player_invite_team':
                case 'match_joining_accepted':
                case 'match_joining_rejected':
                case 'match_result_added':
                case 'player_result_added':
                case 'job_reminder':
                case 'interest_shown':
                    $notifications[$key]['description'] = (config('app.locale') == 'ar_AR' || config('app.locale') == 'ur_PK') ? $notification['description'] . " " . $notification['sender']['name'] : $notification['sender']['name'] . " " . $notification['description'];
                    break;
                default:
                    break;
            }
//            $notifications[$key]['user'] = $notification['sender'];
//            unset($notifications[$key]['sender']);
        }

        $data['notifications'] = $notifications;
        unset($result['data']);
        $data['pagination'] = $result;
        return prepareResult(200, $data, 'Notifications');
    }

    public function getUnViewCount(Request $request) {
        $data['un_viewed_counts'] = getHashCache('notifications', 'count_' . $request->loginUserId);
        if ($data['un_viewed_counts']) {
            return prepareResult(200, $data, 'Un-View-Count');
        }
        $counts = Notification::where(['user_id' => $request->loginUserId, 'is_viewed' => 0])
                        ->select('module', \DB::raw('count(*) as total'))
                        ->groupBy('module')->pluck('total', 'module');
        $generals = $counts->get('generals', 0);
        $friends = $counts->get('friends', 0);
        $messages = $counts->get('messages', 0);

        $un_viewed_counts = new \stdClass();
        $un_viewed_counts->generals = $generals;
        $un_viewed_counts->friends = $friends;
        $un_viewed_counts->messages = $messages;
        $un_viewed_counts->total = $generals + $friends + $messages;

//--------------------------------New Feed Bubble Handling--------------------------------        
        $request['checkingForNewFeeds'] = true;
        $request['feed_type'] = 'all';
        $feedController = new FeedsController();
        //-----------New Activity bubble---------------
        $un_viewed_counts->new_activity = false;
        $type = $request['feed_main_type'] = 'activity';
        $activityFeeds = $feedController->getFeeds($request);
        $activityFeeds = $activityFeeds['result']->feeds;
        if (count($activityFeeds)) {
            $maxActivityId = max(array_column($activityFeeds, 'id'));
            $key = "USER_{$request->loginUser->id}_SPORT_{$request->loginUser->sport_id}_{$type}_MAX_ID";
            if ($maxActivityId > Cache::get($key, 0)) {
                $un_viewed_counts->new_activity = true;
            }
        }
        //-----------New Events bubble---------------
        $un_viewed_counts->new_event = false;
        $type = $request['feed_main_type'] = 'event';
        $activityFeeds = $feedController->getFeeds($request);
        $activityFeeds = $activityFeeds['result']->feeds;
        if (count($activityFeeds)) {
            $maxActivityId = max(array_column($activityFeeds, 'id'));
            $key = "USER_{$request->loginUser->id}_SPORT_{$request->loginUser->sport_id}_{$type}_MAX_ID";
            if ($maxActivityId > Cache::get($key, 0)) {
                $un_viewed_counts->new_event = true;
            }
        }
        //-----------New Transfer bubble---------------
        $un_viewed_counts->new_transfer = false;
        $type = $request['feed_main_type'] = 'transfer';
        $activityFeeds = $feedController->getFeeds($request);
        $activityFeeds = $activityFeeds['result']->feeds;
        if (count($activityFeeds)) {
            $maxActivityId = max(array_column($activityFeeds, 'id'));
            $key = "USER_{$request->loginUser->id}_SPORT_{$request->loginUser->sport_id}_{$type}_MAX_ID";
            if ($maxActivityId > Cache::get($key, 0)) {
                $un_viewed_counts->new_transfer = true;
            }
        }
        //-----------New Match bubble---------------
        $un_viewed_counts->new_match = false;
        $request['type'] = 'all';
        $matchController = new MatchesController();
        $matchController = $matchController->getMatches($request);
        $matchController = $matchController['result']->matches;
        if (count($matchController)) {
            $maxActivityId = max(array_column($matchController, 'id'));
            $key = "USER_{$request->loginUser->id}_SPORT_{$request->loginUser->sport_id}_match_MAX_ID";
            if ($maxActivityId > Cache::get($key, 0)) {
                $un_viewed_counts->new_match = true;
            }
        }
//---------------------------------------------------------------------------------------

        $data['un_viewed_counts'] = $un_viewed_counts;
        return prepareResult(200, $data, 'Un-View-Count');
    }

}
