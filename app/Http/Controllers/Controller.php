<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        //===================Handeling Pagination Per Page for Web and Mobile Devices====================================
        $request = request();
        if ($request->has('per_page')) {
            $paginationPerPage = $request->get('per_page', config("RECORDS_PER_PAGE_WEB", 15));
        } else {
            switch ($request->header('Device-Type', 'web')) {
                case 'ios':
                case 'android':
                    $paginationPerPage = config("RECORDS_PER_PAGE_MOBILE", 15);
                    break;

                default:
                    $paginationPerPage = config("RECORDS_PER_PAGE_WEB", 15);
                    break;
            }
        }
        if (!defined("PAGINATION_PER_PAGE"))
            define("PAGINATION_PER_PAGE", $paginationPerPage);
        //===============================================================================================================
    }

}
