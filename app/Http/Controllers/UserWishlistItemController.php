<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\UserWishlist;
use App\Models\UserWishlistItem;

use Illuminate\Http\Request;

class UserWishlistItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request['product_id'] && $request['user_wishlist_id'] && $request['user_id']) {
            $result = $request->all();
            unset($result['user_id']);

            UserWishlist::where(['user_id' => $request['user_id'], 'id' => $request['user_wishlist_id']])->firstOrFail();
            Product::findOrFail($request['product_id']);
            UserWishlistItem::updateOrCreate($result);
            return prepareResult(200, ['data' => 'saved'], "Success");

        } else {
            return prepareResult(404, ['data' => 'Missing Parameters'], "Error");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserWishlistItem  $userWishlistItem
     * @return \Illuminate\Http\Response
     */
//    public function show(UserWishlistItem $userWishlistItem)

    public function show($id)
    {

        /*
         * join('user_wishlists','user_wishlist_items.user_wishlist_id','user_wishlists.id')
         *  ->select('user_wishlist_items.*','user_wishlists.name','user_wishlists.date')
         */

        $wishlist_items = UserWishlist::with(['product' => function($query){
            return $query->where('status',config('constants.status.active'));
        }])->where('id',$id)->findOrFail($id);
        return prepareResult(200, ['wishlist_items' => $wishlist_items], "success");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserWishlistItem  $userWishlistItem
     * @return \Illuminate\Http\Response
     */
    public function edit(UserWishlistItem $userWishlistItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserWishlistItem  $userWishlistItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserWishlistItem $userWishlistItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserWishlistItem  $userWishlistItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserWishlistItem $userWishlistItem)
    {
        //
    }
}
