<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Friendship;
use App\Jobs\ProcessEmail;
use \stdClass;
use DateTime;
use App\Http\Libraries\Uploader;
use Response;
use DB;

class UsersController extends Controller {

    use \App\Traits\CommonTraits;

    public function searchUserFriend(Request $request) {
        checkCache();
        $this->validate($request, [
            'name' => 'required|string|min:3'
        ]);
        $result['users'] = User::
//                whereHas('friends', function($query) use($request) {
//                            $query->where('friend_id', $request->loginUserId)->where('is_confirmed', 1);
//                        })
                        where('name', 'LIKE', "%" . $request->name . '%')
                        //->orderBy('name', 'asc')
                        ->orderByBestMatch($request->name, 'name')
                        ->orderUserNearToMeFirst()
                        ->take(10)->get(['id', 'name', 'avatar', 'slug']);
        return prepareResult(200, $result, "Success");
    }

    public function searchFriendsOrPlayers(Request $request) {
        checkCache();
        $this->validate($request, [
            'name' => 'required|string|min:3'
//            'job_id'
        ]);
        $result = User::where('name', 'LIKE', "%" . $request->name . '%')->where('id', '!=', $request->loginUserId)
                ->where('id', '!=', $request->job_user_id)
                ->whereHas('profileStat');
//        if ($request->job_id && \App\Models\Job::find($request->job_id)->user_id == $request->loginUserId) {
//            $result = $result->where('role_id', 1); // only players
//        } 
//        else {
//            $result = $result->whereHas('friends', function($query) use($request) {
//                $query->where('friend_id', $request->loginUserId)->where('is_confirmed', 1);
//            });
//        }
        $data['users'] = $result
//                ->orderBy('name', 'asc')
                        ->orderByBestMatch($request->name, 'name')
                        ->take(10)->get(['id', 'name', 'avatar', 'slug']);
        return prepareResult(200, $data, "Success");
    }

    public function SearchAllUser(Request $request) {
        checkCache();
        $this->validate($request, [
            'name' => 'min:3'
        ]);
        $data['users'] = User::whereHas('profileStat')->where('name', 'LIKE', "%{$request->criteria}%")
                        ->orderByBestMatch($request->criteria, 'name')
                        ->take(6)->get(['id', 'name', 'slug', 'avatar']);
        return prepareResult(200, $data, "Success");
    }

    public function SearchAllPlayers(Request $request) {
        checkCache();
        $this->validate($request, [
            'criteria' => 'required|min:3',
            'with_my_dummy_users' => '',
        ]);
        $users = User::where(function($query)use ($request) {
                    $query->where('name', 'LIKE', "%{$request->criteria}%")->orWhere('email', encrypted($request->criteria));
                });
        if ($request->with_my_dummy_users) {
            $users = $users->includeMyDummyUsers();
        } else {
            $users = $users->whereHas('profileStat');
        }
        $users = $users->orderByBestMatch($request->criteria, 'name')
                ->take(25)
                ->get(['id', 'name', 'slug', 'avatar', 'user_type', 'date_updated as last_active']);
        return prepareResult(200, ['users' => $users], "Success");
    }

    public function OnlineFriends(Request $request) {
        checkCache();
        $friends = $request->loginUser->friends->where('is_confirmed', 1)->pluck('friend_id')->toArray();
        $data['friends'] = User::whereHas('friends', function($query) use($request) {
                    $query->where('friend_id', $request->loginUserId)->where('is_confirmed', 1);
                })
                ->online()
                ->select('id', 'name', 'avatar', 'slug')
                ->get();
        return prepareResult(200, $data, "Success");
    }

    public function JobsInvitation(Request $request) {
        $this->validate($request, [
            'jobs' => 'required|array',
            'user_id' => 'required',
        ]);
        $user = \App\Models\User::find($request->user_id);
        $jobs = \App\Models\Job::whereIn('id', $request->jobs)->get(['id', 'slug']);
        $current_user_link = config('app.VUE_APP_URL') . "/users/" . $request->loginUser->slug;
        $unsub_link = config('app.VUE_APP_URL') . '/users/' . $user->slug . '/settings/notifications';
        foreach ($jobs as $job) {
            $job_link = config('app.VUE_APP_URL') . "/jobs/" . $job->slug;
            $data = [
                'name' => $user->name,
                'link' => $job_link,
                'subject' => 'Job Invitation',
                'to' => $user->email,
                'from' => $request->loginUser->name,
                'from_link' => $current_user_link,
                'unsub_link' => $unsub_link
            ];
            \App\Jobs\ProcessEmail::dispatch('job_invitation', $data)->onQueue('low');
            $this->saveNotification($user->id, 'job_invitation', 'New Job Invitation', ' has invited you to the job', $job->id, 'generals', '', '', '{"slug":"' . $job->slug . '"}');
            //send_email('job_invitation', $data);
        }
        return prepareResult(200, new \stdClass(), "Invitation Sent");
    }

    public function sendEmailInvitations(Request $request) {
        $this->validate($request, [
            'emails' => 'required|array',
            'emails.*' => 'email',
            'subject' => 'required',
            'message' => 'required',
        ]);
        foreach ($request->emails as $email) {
            $data = [
                'from' => $request->loginUser->name,
                'from_email' => $request->loginUser->email,
                'from_link' => config('app.VUE_APP_URL') . "/users/" . $request->loginUser->slug,
                'link' => config('app.VUE_APP_URL') . "/register",
                'subject' => $request->subject,
                'content' => $request->message,
                'to' => $email,
            ];
            ProcessEmail::dispatch('email_invitation', $data)->onQueue('low');
            //send_email('email_invitation', $data);
        }
        return prepareResult(200, new \stdClass(), "Invitation Email Sent");
    }

    public function nonRegisteredEmails(Request $request) {
        $this->validate($request, [
            'emails' => 'required|array',
            'emails.*' => 'email',
        ]);
        $registeredUsers = User::whereIn('email', $request->hashed_emails)->get(['id', 'email']);
        $allContacts = $request->emails;
        $nonRegistered['emails'] = array_values(array_diff($allContacts, $registeredUsers->pluck("email")->toArray()));
        //--------
//        $friendController = new \App\Http\Controllers\FriendshipController();
//        foreach ($registeredUsers->pluck("id")->toArray() as $friendId) {
//            $request['id'] = $friendId;
//            $friendController->send_request($request);
//        }

        foreach ($registeredUsers->pluck("id")->toArray() as $friendId) {
            Friendship::updateOrCreate(['user_id' => $friendId, 'friend_id' => $request->loginUserId, 'sender_id' => $request->loginUserId], ['is_confirmed' => 1]);
            Friendship::updateOrCreate(['user_id' => $request->loginUserId, 'friend_id' => $friendId, 'sender_id' => $request->loginUserId], ['is_confirmed' => 1]);
        }
        return prepareResult(200, $nonRegistered, "Non Registered Emails");
    }

    public function nonRegisteredNumbers(Request $request) {
        $this->validate($request, [
            'phones' => 'required|array',
        ]);
        $registeredUsers = User::whereIn('phone', $request->hashed_phones)->get(['id', 'phone']);
        $allContacts = $request->phones;
        $nonRegistered['phones'] = array_values(array_diff($allContacts, $registeredUsers->pluck("phone")->toArray()));
        //----------------------------------------------------
        /*
          $friendController = new \App\Http\Controllers\FriendshipController();
          foreach ($registeredUsers->pluck("id")->toArray() as $friendId) {
          $request['id'] = $friendId;
          $friendController->send_request($request);
          }
         */
        //-----------Make Friends
        foreach ($registeredUsers->pluck("id")->toArray() as $friendId) {
            Friendship::updateOrCreate(['user_id' => $friendId, 'friend_id' => $request->loginUserId, 'sender_id' => $request->loginUserId], ['is_confirmed' => 1]);
            Friendship::updateOrCreate(['user_id' => $request->loginUserId, 'friend_id' => $friendId, 'sender_id' => $request->loginUserId], ['is_confirmed' => 1]);
        }
        return prepareResult(200, $nonRegistered, "Non Registered Phones");
    }

    public function SearchUsers(Request $request) {
        checkCache();
        $this->validate($request, [
            'name' => 'required|string|min:2',
        ]);
        $result = User::where(function($query)use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->name . '%')->where('id', '!=', $request->loginUser->id)
                    ->orWhere('email', encrypted($request->name));
                })
                ->addSelect(['id', 'name', 'avatar', 'slug', 'date_created', 'date_updated as last_active', 'role_id', 'sport_id', 'country_id'])
                ->withCount(['friends as friends_count' => function($query) {
                        $query->where('is_confirmed', 1);
                    }])
                ->withCount(['friends as is_friend' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, 'is_confirmed' => 1]);
                    }])
                ->withCount(['friends as is_request_sent' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->withCount(['friends as is_awating_responce' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '!=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->whereHas('profileStat')
                ->where('status', 'Active')
                ->with('country:id,name', 'sport:id,name', 'role:id,name')
                ->with('profileStat:user_id,position_id', 'profileStat.position:id,name')
//                ->orderBy('name', 'asc')
                ->orderByBestMatch($request->name, 'name')
                ->orderByFrndAndFoF()
                ->orderUserNearToMeFirst()
                ->paginate(PAGINATION_PER_PAGE);
        $result = $result->toArray();
        $data['users'] = $result['data'];
        unset($result['data']);
        $data['pagination'] = $result;

        $data['clubs'] = \App\Models\Club::where('title', 'LIKE', '%' . $request->name . '%')->whereType('club')
                        ->orderByBestMatch($request->name, 'title')
                        ->take(5)->get(['id', 'user_id', 'slug', 'title', 'logo']);
        return prepareResult(200, $data, "Success");
    }

//    public function userUnderSixteen(Request $request) {
//
//        $today = new DateTime('now');
//        $newtime = $today->modify('-13 year')->format('Y-m-d');
//        $users = User::whereHas('profileStat', function($query) use($newtime) {
//                    $query->where('dob', '>', $newtime);
//                })
//                ->get(['id', 'name', 'slug', 'email']);
//        $upload_link = config('app.VUE_APP_URL') . '/under-age-user-consent';
//        foreach ($users as $user) {
//            $unsub_link = config('app.VUE_APP_URL') . '/users/' . $user->slug . '/settings/notifications';
//            $data_var = [
//                'name' => $user->name,
//                'subject' => 'Under age parent consent',
//                'to' => $user->email,
//                'unsub_link' => $unsub_link,
//                'upload_link' => $upload_link,
//            ];
//            ProcessEmail::dispatch('under_age_email', $data_var)->onQueue('low');
////            send_email('under_age_email', $data_var);
//        }
//        return prepareResult(200, $users, "Success");
//    }

    public function uploadParentConsentFile(Request $request) {

        $this->validate($request, [
            'file' => 'required|mimetypes:application/pdf,text/*,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation',
                ], ['mimetypes' => __("Please Select Document File")]);
        $file = '';
        if ($request->hasFile('file')) {
            $uploader = new Uploader('file');
            if ($uploader->isValidFile()) {
                $uploader->upload('ParentalConsent', $request->loginUser->id . '-' . $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $file = $uploader->getUploadedPath();
                }
            }
        }
        \App\Models\ParentalConsent::updateOrCreate([
            'user_id' => $request->loginUser->id], [
            'status' => 'consent_sent',
            'consent_file' => $file
        ]);
        $admins = User::where('is_pm_admin', true)->get(['id', 'name', 'slug', 'email']);
        $user_link = config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug;
        $file_link = config('app.img_url') . '/' . $file;
        foreach ($admins as $admin) {
            $unsub_link = config('app.VUE_APP_URL') . '/users/' . $admin->slug . '/settings/notifications';
            $data_var = [
                'name' => $admin->name,
                'subject' => 'Under age parent consent',
                'to' => $admin->email,
                'user_name' => $request->loginUser->name,
                'user_id' => $request->loginUser->id,
                'user_link' => $user_link,
                'file_name' => $file,
                'file_link' => $file_link,
                'unsub_link' => $unsub_link
            ];
            ProcessEmail::dispatch('parental_control_notification', $data_var)->onQueue('low');
//            send_email('parental_control_notification', $data_var);
        }
        return prepareResult(200, new stdClass, "Success");
    }

    public function uploadAgentLicenseFile(Request $request) {
        $this->validate($request, [
            'file' => 'required|mimetypes:application/pdf,text/*,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation',
                ], ['mimetypes' => __("Please Select Document File")]);
        $file = '';
        if ($request->loginUser->role_id == 4 || $request->loginUser->role_id == 5) {
            if ($request->hasFile('file')) {
                $uploader = new Uploader('file');
                if ($uploader->isValidFile()) {
                    $uploader->upload('AgentLicense', $request->loginUser->id . '-' . $uploader->fileName);
                    if ($uploader->isUploaded()) {
                        $file = $uploader->getUploadedPath();
                    }
                }
            }
            \App\Models\ProfileStat::where('user_id', $request->loginUser->id)->update(['agent_license' => $file]);
        }
        return prepareResult(200, ['file_path' => config('app.img_url') . "/" . $file], "Thank you for submitting your license");
    }

    public function uploadGeneralFile(Request $request) {
        $this->validate($request, [
            'file' => 'required|mimetypes:image/*,application/pdf,text/*,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'path' => 'required'
                ], ['mimetypes' => __("Please Select The Correct File")]);
        $file = '';
//        if (!is_dir(imagesPath($request->path))) {
//
//            return prepareResult(200, new stdClass(), "Invalid Path");
//        }
        if ($request->hasFile('file')) {
            $uploader = new Uploader('file');
            if ($uploader->isValidFile()) {
                $uploader->upload($request->path, $request->loginUser->id . '-' . $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $file = $uploader->getUploadedPath();
                }
            }
        }
        return prepareResult(200, ['full_path' => imagesPath($file), 'relative_path' => $file] + pathinfo($file), "File Uploaded");
    }

//    public function UsersWithConsentFile(Request $request) {
//        if ($request->loginUser->is_pm_admin) {
//            $user = User::whereHas('parentalConsent')
//                    ->with(['parentalConsent' => function($query) {
//                            $query->select(['user_id', 'status']);
//                        }])
//                    ->with('country:id,name')
//                    ->get(['id', 'name', 'email', 'slug','country_id']);
//            return prepareResult(200, $user, "Success");
//        } else {
//            return prepareResult(205, new stdClass, "You are unauthorized person");
//        }
//    }

    public function GetAllUser(Request $request) {
        if ($request->loginUser->is_pm_admin) {
            $id = 0;
            $email = $name = '';
            if ($request->filled('search')) {
                if ((strpos($request->search, '@') !== false)) {
                    $email = $request->search;
                } else if (is_numeric($request->search)) {
                    $id = $request->search;
                } else {
                    $name = $request->search;
                }
            }
            $user = User::withCount(['friends as is_friend' => function($query) use($request) {
                            $query->where(['friend_id' => $request->loginUserId, 'is_confirmed' => 1]);
                        }])
                    ->withCount(['friends as is_request_sent' => function($query) use($request) {
                            $query->where(['friend_id' => $request->loginUserId, ['sender_id', '=', $request->loginUserId], 'is_confirmed' => 0]);
                        }])
                    ->when($request->country_id, function ($query) use ($request) {
                        return $query->where('country_id', $request->country_id);
                    })
                    ->when($request->register_date, function ($query) use ($request) {
                        return $query->whereDate('register_date', substr($request->register_date, 0, 10));
                    })
                    ->when($id, function ($query) use ($request) {
                        return $query->where('id', $request->search);
                    })
                    ->when($email, function ($query) use ($request) {
                        return $query->where('email', 'LIKE', "%" . encrypted($request->search) . '%');
                    })
                    ->when($name, function ($query) use ($request) {
                        return $query->where('name', 'LIKE', "%" . $request->search . '%');
                    })
                    ->addSelect('id', 'name', 'email', 'slug', 'is_email_verified', 'phone', 'is_phone_verified', 'country_id', 'role_id', 'register_date')
                    ->with('country:id,name')
                    ->orderBy('register_date', 'desc')
                    ->paginate(30);
            $countries = User::where('country_id', '!=', '0')
                    ->select('country_id', \DB::raw('count(country_id) as total'))
//                    ->addSelect('country_id')
                    ->with('country:id,name')
                    ->groupBy('country_id')
                    ->get();
            $user = $user->toArray();
            $data['users'] = $user['data'];
            foreach ($data['users'] as $key => $usr) {
                $request->merge(
                        ['loginUserId' => $usr['id']]
                );
                $request->loginUserId = $usr['id'];
                $progress = $this->ProfileProgress($request)['result'];
                $data['users'][$key]['profile_action'] = $progress->percentage == 100 ? 'none' : $progress->action;
                $data['users'][$key]['profile_percent'] = $progress->percentage;
            }
            $data['countries'] = $countries;

            unset($user['data']);
            $data['pagination'] = $user;
            return prepareResult(200, $data, "Success");
        } else {
            return prepareResult(201, new stdClass, "You are unauthorized person");
        }
    }

    public function GetUserEmails(Request $request) {
        $data = $email = $name = '';
        if ($request->loginUser->is_pm_admin) {
            if ($request->param == 'all') {

                $data = User::get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'all';
            } else if ($request->param == 'players') {

                $data = User::where('role_id', 1)->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'players';
            } else if ($request->param == 'coaches') {

                $data = User::where('role_id', 2)->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'coaches';
            } else if ($request->param == 'club-admins') {

                $data = User::where('role_id', 3)->orWhere('role_id', 6)->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'club admins';
            } else if ($request->param == 'agents') {
                $data = User::where('role_id', 4)->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'agents';
            } else if ($request->param == 'no_basic_info') {

                $data = User::doesntHave('profileStat')->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'basic info';
            } else if ($request->param == 'no_career') {

                $data = User::doesntHave('userCareers')->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'career';
            } else if ($request->param == 'no_skills') {

                $data = User::doesntHave('user_skills')->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'skills';
            } else if ($request->param == 'no_avatar') {

                $data = User::where('avatar', null)->get(['email']);
                foreach ($data as $dat) {
                    $email = $email . $dat->email . ', ';
                }
                $name = 'avatar';
            }
            return prepareResult(200, $email, $name);
        } else {
            return prepareResult(201, new stdClass, "You are unauthorized person");
        }
    }

    function arrayUnique($array, $preserveKeys = false) {
        $arrayRewrite = array();
        $arrayHashes = array();
        foreach ($array as $key => $item) {
            $hash = md5(serialize($item));
            if (!isset($arrayHashes[$hash])) {
                $arrayHashes[$hash] = $hash;
                if ($preserveKeys) {
                    $arrayRewrite[$key] = $item;
                } else {
                    $arrayRewrite[] = $item;
                }
            }
        }
        return $arrayRewrite;
    }

    public function GetSuggestions(Request $request) {
        checkCache();
        $user = User::find($request->loginUserId);
        $blockedUsers = \App\Models\SpamReport::where('user_id', $request->loginUserId)->pluck('module_id')->toArray();
        $friends = $user->friends->where('is_confirmed', 1)->pluck('friend_id')->toArray();
        $fof_array = Friendship::whereIn('user_id', $friends)->where('friend_id', '!=', $request->loginUserId)->where('is_confirmed', 1)->pluck('friend_id')->toArray();
        $sent_request = $user->friendships->where('is_confirmed', 0)->pluck('friend_id')->toArray();
        array_push($friends, $request->loginUserId);
        $friends = array_unique(array_merge($friends, $sent_request, $blockedUsers));
        $suggestions = User::whereNotIn('id', $friends)
                ->addSelect(['id', 'name', 'avatar', 'slug', 'country_id', 'role_id', 'sport_id'])
                ->with('country:id,name', 'role:id,name')
                ->withCount(['friends as friends_count' => function($query) {
                        $query->where('is_confirmed', 1);
                    }])
                ->withCount(['friends as is_friend' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, 'is_confirmed' => 1]);
                    }])
                ->withCount(['friends as is_request_sent' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->withCount(['friends as is_awating_responce' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '!=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->with('profileStat:user_id,position_id,level_id,current_club_id', 'profileStat.position:id,name', 'profileStat.level:id,name', 'profileStat.currentClub:id,name')
                ->where('sport_id', $user->sport_id)
                ->whereHas('profileStat')
                ->orderByRaw("FIELD(`country_id`, {$user->country_id}) DESC");
        if (count($fof_array) > 0) {
            $fof = implode(',', $fof_array);
            $suggestions = $suggestions->orderByRaw("FIELD(`id`, $fof) DESC");
        }
        $suggestions = $suggestions->Paginate(PAGINATION_PER_PAGE);
        $suggestions = $suggestions->toArray();
        $result = $suggestions['data'];
        unset($suggestions['data']);

        $output = array(
            'users' => $result,
            'pagination' => $suggestions,
        );
        return prepareResult(200, $output, "Success");
    }

    public function FriendList(Request $request) {
        checkCache();
        $user = User::find($request->loginUserId);
        $friends = $user->friends->where('is_confirmed', 1)->pluck('friend_id');
        $result['friends'] = User::whereIn('id', $friends)
                ->addSelect(['id', 'name', 'avatar', 'slug'])
                ->with('country:id,name')
                ->withCount('friends as friends_count')
                ->orderBy('name', 'asc')
                ->get();
        return prepareResult(200, $result, "Success");
    }

    public function advancedSearch(Request $request) {
        checkCache();
        $this->validate($request, [
            'data.0.height.0' => 'numeric|nullable|min:0',
            'data.0.height.1' => 'numeric|nullable|min:0',
            'data.0.weight.0' => 'numeric|nullable|min:0',
            'data.0.weight.1' => 'numeric|nullable|min:0',
            'data.0.dob.0' => 'numeric|nullable|min:0',
            'data.0.dob.1' => 'numeric|nullable|min:0',
                ], ['data.0.height.0.min' => 'Minimun value must be greater or equal to 0',
            'data.0.height.1.min' => 'Maximum value must be greater or equal to 0',
            'data.0.weight.0.min' => 'Minimun value must be greater or equal to 0',
            'data.0.weight.1.min' => 'Maximum value must be greater or equal to 0',
            'data.0.dob.0.min' => 'Minimun value must be greater or equal to 0',
            'data.0.dob.1.min' => 'Maximum value must be greater or equal to 0']);
//        $friend_ids = User::find($request->loginUserId)->friends->where('is_confirmed', 1)->pluck('friend_id')->toArray();
//        array_push($friend_ids, $request->loginUserId);
//        $users = User::where('status', 'Active')->whereNotIn('id', $friend_ids)
        $users = User::where('status', 'Active')->whereNotIn('id', [$request->loginUserId])
                ->select(['id', 'name', 'avatar', 'slug', 'country_id', 'role_id', 'sport_id'])
//                ->where('sport_id', $request->input('data.0.sport_id'))
                ->whereHas('profileStat')
                ->with('sport:id,name', 'country:id,name', 'role:id,name')
                ->withCount(['friends as friends_count' => function($query) {
                        $query->where('is_confirmed', 1);
                    }])
                ->withCount(['friends as is_friend' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, 'is_confirmed' => 1]);
                    }])
                ->withCount(['friends as is_request_sent' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->withCount(['friends as is_awating_responce' => function($query) use($request) {
                        $query->where(['friend_id' => $request->loginUserId, ['sender_id', '!=', $request->loginUserId], 'is_confirmed' => 0]);
                    }])
                ->with('profileStat:user_id,position_id,level_id,current_club_id,country_residence_id', 'profileStat.position:id,name', 'profileStat.level:id,name', 'profileStat.currentClub:id,name', 'profileStat.countryResidence:id,name');

        if ($request->filled('data.0.sport_id')) {
            $users->where('sport_id', $request->input('data.0.sport_id'));
        }
        if ($request->filled('data.0.role_id')) {
            $users->where('role_id', $request->input('data.0.role_id'));
        }
        if ($request->filled('data.0.name')) {
            $users->where('name', 'Like', '%' . $request->input('data.0.name') . '%');
        }
        if ($request->filled('data.0.gender_id')) {
            $gender = $request->input('data.0.gender_id');
            $users->whereHas('profileStat', function ($query) use ($gender) {
                $query->where('gender_id', $gender);
            });
        }
        if ($request->filled('data.0.dob') && ($request->filled('data.0.dob.0') || $request->filled('data.0.dob.1'))) {
            $dob = $request->input('data.0.dob');
            $users->whereHas('profileStat', function ($query) use ($dob) {
                if (isset($dob[0])) {
//                    $query->where('age', '>', $dob);
//                    $query->whereRaw("dob <= date_sub(curdate(), interval {$dob[0]} year)");
//                    $query->whereRaw("YEAR(dob) <= YEAR(date_sub(curdate(), interval {$dob[0]} year))");
                    $query->whereRaw("FLOOR(DATEDIFF(CURRENT_DATE, STR_TO_DATE(dob, '%Y-%m-%d'))/365) >= {$dob[0]}");
                }
                if (isset($dob[1])) {
//                    $query->where('age', '<', $dob);
//                    $query->whereRaw("dob >= date_sub(curdate(), interval {$dob[1]} year)");
//                    $query->whereRaw("YEAR(dob) >= YEAR(date_sub(curdate(), interval {$dob[1]} year))");
                    $query->whereRaw("FLOOR(DATEDIFF(CURRENT_DATE, STR_TO_DATE(dob, '%Y-%m-%d'))/365) <= {$dob[1]}");
                }
            });
        }
        if ($request->filled('data.0.height') && ($request->filled('data.0.height.0') || $request->filled('data.0.height.1'))) {
            $height = $request->input('data.0.height');
            $height_unit = $request->input('data.0.height_unit');
            $users->whereHas('profileStat', function ($query) use ($height, $height_unit) {
                if ($height[0]) {
//                    $query->whereRaw("(height >= $height[0] && height_unit ==)");
                    $query->where('height', '>=', (int) ($height_unit == "cm" ? $height[0] : $height[0]));
                }
                if ($height[1]) {
                    $query->where('height', '<=', (int) ($height_unit == "cm" ? $height[1] : $height[1]));
                }
            });
        }
        if ($request->filled('data.0.weight') && ($request->filled('data.0.weight.0') || $request->filled('data.0.weight.1'))) {
            $weight = $request->input('data.0.weight');
            $weight_unit = $request->input('data.0.weight_unit');
            $users->whereHas('profileStat', function ($query) use ($weight, $weight_unit) {
                if ($weight[0]) {
                    $query->where('weight', '>=', (int) ($weight_unit == "lbs" ? $weight[0] : $weight[0]));
                }
                if ($weight[1]) {
                    $query->where('weight', '<=', (int) ($weight_unit == "lbs" ? $weight[1] : $weight[1]));
                }
            });
        }
        if ($request->filled('data.0.country_residence_id')) {
            $country_residence = $request->input('data.0.country_residence_id');
            $users->whereHas('profileStat', function ($query) use ($country_residence) {
                $query->where('country_residence_id', $country_residence);
            });
        }
        if ($request->filled('data.0.city_residence_id')) {
            $city_residence = $request->input('data.0.city_residence_id');
            $users->whereHas('profileStat', function ($query) use ($city_residence) {
                $query->where('city_residence_id', $city_residence);
            });
        }
        if ($request->filled('data.0.league_id')) {
            $league = $request->input('data.0.league_id');
            $users->whereHas('userCareers', function ($query) use ($league) {
                $query->where('league_id', $league);
            });
        }
        if ($request->filled('data.0.current_club_id')) {
            $current_club = $request->input('data.0.current_club_id');
            $users->whereHas('profileStat', function ($query) use ($current_club) {
                $query->where('current_club_id', $current_club);
            });
        }
        if ($request->filled('data.0.position_id')) {
            $position = $request->input('data.0.position_id');
            $users->whereHas('profileStat', function ($query) use ($position) {
                $query->where('position_id', $position);
            });
        }
        if ($request->filled('data.0.level_id')) {
            $level = $request->input('data.0.level_id');
            $users->whereHas('profileStat', function ($query) use ($level) {
                $query->where('level_id', $level);
            });
        }
        if ($request->filled('data.0.preferred_foot_id')) {
            $preferred_foot = $request->input('data.0.preferred_foot_id');
            $users->whereHas('profileStat', function ($query) use ($preferred_foot) {
                $query->where('preferred_foot_id', $preferred_foot);
            });
        }
//        if ($request->filled('data.0.city_residence_id')) {
//            $preferred_foot = $request->input('data.0.city_residence_id');
//            $users->whereHas('profileStat', function ($query) use ($preferred_foot) {
//                $query->where('preferred_foot_id', $preferred_foot);
//            });
//        }
        if ($request->filled('data.0.playing_role_id')) {
            $users->whereHas('profileStat', function ($query) use ($request) {
                $query->where('playing_role_id', $request->input('data.0.playing_role_id'));
            });
        }
        if ($request->filled('data.0.batting_style_id')) {
            $users->whereHas('profileStat', function ($query) use ($request) {
                $query->where('batting_style_id', $request->input('data.0.batting_style_id'));
            });
        }
        if ($request->filled('data.0.bowling_style_id')) {
            $users->whereHas('profileStat', function ($query) use ($request) {
                $query->where('bowling_style_id', $request->input('data.0.bowling_style_id'));
            });
        }
        $search_result = $users->orderUserNearToMeFirst()->Paginate(10)->toArray();
        $result = $search_result['data'];
        unset($search_result['data']);

        $output = array(
            'users' => $result,
            'pagination' => $search_result,
        );
        return prepareResult(200, $output, 'Success');
    }

    public function advancedSearchCLubs(Request $request) {
        checkCache();
        $this->validate($request, [
            'data.0.establish_in_a' => 'numeric|nullable|min:0',
            'data.0.establish_in_b' => 'numeric|nullable|min:0',
                ], ['data.0.establish_in_a.min' => 'Established From must be positive',
            'data.0.establish_in_b.min' => 'Established To must be positive']);
        $clubs = \App\Models\Club::where('type', $request->input('data.0.type'))
                ->addSelect(['id', 'country_id', 'city_id', 'type', 'logo', 'intro', 'slug', 'sport_id', 'title', 'user_id', 'established_in_id'])
                ->with('establishedIn:id,name', 'country:id,name', 'city:id,name')
                ->withCount(['clubFollowers as hasFollowed' => function($query) use($request) {
                        $query->where(['user_id' => $request->loginUserId]);
                    }])
                ->withCount(['clubOfficials as is_official' => function($query) use($request) {
                $query->where('user_id', $request->loginUserId)
                ->where(function($query) {
                            $query->where('status', 'Author')->orWhere('status', 'Accepted');
                        });
            }]);
        if ($request->filled('data.0.sport_id')) {
            $clubs->where('sport_id', $request->input('data.0.sport_id'));
        }
        if ($request->filled('data.0.name')) {
            $clubs->where('title', 'Like', '%' . $request->input('data.0.name') . '%');
        }
        if ($request->filled('data.0.stadium')) {
            $clubs->where('stadium', 'Like', '%' . $request->input('data.0.stadium') . '%');
        }
        if ($request->filled('data.0.country_residence_id')) {
            $clubs->where('country_id', $request->input('data.0.country_residence_id'));
        }
        if ($request->filled('data.0.city_residence_id')) {
            $clubs->where('city_id', $request->input('data.0.city_residence_id'));
        }
        if ($request->filled('data.0.establish_in_a')) {
            $clubs->whereHas('establishedIn', function ($query) use ($request) {
                $query->where('name', '>=', $request->input('data.0.establish_in_a'));
            });
        }
        if ($request->filled('data.0.establish_in_b')) {
            $clubs->whereHas('establishedIn', function ($query) use ($request) {
                $query->where('name', '<=', $request->input('data.0.establish_in_b'));
            });
        }

        $search_result = $clubs->Paginate(10)->toArray();
        $result = $search_result['data'];
        unset($search_result['data']);

        $output = array(
            'clubs' => $result,
            'pagination' => $search_result,
        );
        return prepareResult(200, $output, 'Success');
    }

    public function GetCurrentUser(Request $request) {
        checkCache();
        $UserInfo = User::where('id', $request->loginUserId)->first(['name', 'email']);
        return prepareResult(200, $UserInfo, "Success");
    }

    public function ProfileProgress(Request $request) {

        $progress = getHashCache('profile_progress', $request->loginUserId);
        if ($progress) {
            return prepareResult(200, $progress, "Success");
        }
        $UserInfo = User::where('id', $request->loginUserId)
                ->addSelect(['id', 'avatar'])
                ->withCount(['friends as friends_count' => function($query) {
                        $query->where('is_confirmed', 1);
                    }])
                ->withCount(['photos as has_cover' => function($query) {
                        $query->where('module', 'CoverPhoto');
                    }])
                ->withCount('profileStat as has_profile')
                ->withCount('user_skills as has_skills')
                ->withCount('userCareers as has_career')
                ->first();
        $progress = [
            'percentage' => 20,
            'action' => ''
        ];
        if (!$UserInfo) {
            return prepareResult(200, $progress, "User not activate yet");
        }
        if (!$UserInfo->has_profile) {
            $progress['percentage'] = 0;
            $progress['action'] = 'basic_info';
            return prepareResult(200, $progress, "Success");
        }
        if ($UserInfo->avatar) {
            $progress['percentage'] += 20;
        } elseif (!$progress['action']) {
            $progress['action'] = 'avatar';
        }
        if ($UserInfo->has_career) {
            $progress['percentage'] += 20;
        } elseif (!$progress['action']) {
            $progress['action'] = 'career';
        }
        if ($UserInfo->has_cover) {
            $progress['percentage'] += 10;
        } elseif (!$progress['action']) {
            $progress['action'] = 'cover';
        }
        if ($UserInfo->has_skills) {
            $progress['percentage'] += 20;
        } elseif (!$progress['action']) {
            $progress['action'] = 'skills';
        }
        if ($UserInfo->friends_count > 0) {
            $progress['percentage'] += 10;
        } elseif (!$progress['action']) {
            $progress['action'] = 'add_friend';
        }
        setHashCache('profile_progress', $request->loginUserId, $progress);
        return prepareResult(200, $progress, "Success");
    }

    public function GetUserInfo(Request $request) {
        $this->validate($request, [
            'slug' => 'required|exists:users,slug,status,Active', //user must be active as well
        ]);
        $data['user'] = User::where('id', $request->id)->orWhere('slug', $request->slug)
                ->select("id", "name", "slug", "avatar", "country_id", "sport_id", "role_id")
                ->whereHas('profileStat')
                ->with(['country', 'sport', 'role', 'profileStat:user_id,level_id,dob,age,intro', 'profileStat.level'])
                ->withCount(['friends as friends_count' => function($query) {
                        $query->where('is_confirmed', 1);
                    }])
                ->first();
        $cov = Photo::where('user_id', $data['user']->id)->where('module', 'CoverPhoto')->select('hash', 'extention')->orderBy('date_updated', 'desc')->first();
        $cover = '';
        if ($cov) {
            $url = config('app.img_url');
            $cover = $url . '/' . 'users' . '/' . $data['user']->id . '/CoverPhoto/' . $cov->hash . '.' . $cov->extention;
        }
        $data['user']['cover'] = $cover;
        return prepareResult(200, $data, "Success");
    }

}
