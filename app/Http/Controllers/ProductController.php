<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if (!request()->filled('category')) {

                abort(404, 'Category not found');
            }
            $category = Category::where('slug', request('category'))->pluck('id')->first();

            if (is_null($category)) {
                return prepareResult(200, ['products' => []], 'success');
            }

            $products = Product::with('images')->where('category_id', $category);

            if (request()->filled('price')) {

                $prices = explode('-', request('price'));
                $products->whereBetween('actual_price', $prices);
            }

            if (request()->filled('brand')) {
                $brand = Brand::where('slug', request('brand'))->pluck('id')->first();

                $products->where('brand_id', $brand);
            }

            $all_products = $products->get();

            return prepareResult(200, ['products' => $all_products], 'success');
        }
        catch (\Exception $error) {
            return prepareResult(500, ['exception' => 'Oops Something went wrong'], 'error');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        if (request()->filled('category') && request()->filled('brand') && request()->filled('product')) {


            $product_details = Product::with(['images','brand','category'])->whereHas('brand',function($query){
                return $query->where('slug',request('brand'));
            })->whereHas('category',function($query) {
                return $query->where('slug',request('category'));
            })->where('slug',request('product'))->first();


            return prepareResult(200, ['product_details' => $product_details], 'success');
        }
    else{

            return prepareResult(404, ['data' => 'Invalid parameters'], 'error');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }


}
