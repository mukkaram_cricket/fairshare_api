<?php

namespace App\Http\Controllers;

use App\UserInvitationHistory;
use Illuminate\Http\Request;

class UserInvitationHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserInvitationHistory  $userInvitationHistory
     * @return \Illuminate\Http\Response
     */
    public function show(UserInvitationHistory $userInvitationHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserInvitationHistory  $userInvitationHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(UserInvitationHistory $userInvitationHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserInvitationHistory  $userInvitationHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserInvitationHistory $userInvitationHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserInvitationHistory  $userInvitationHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserInvitationHistory $userInvitationHistory)
    {
        //
    }
}
