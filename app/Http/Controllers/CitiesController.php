<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \stdClass;

class CitiesController extends Controller {

    public function CitiesByCountry(Request $request) {    //readcitiesbycountries method brings all cities with specific country_id
        checkCache();
        $valid = validator($request->only('id'), [
            'id' => 'required|numeric|max:3']);
        if ($valid->fails()) {
            $jsonError = response()->json($valid->errors(), 400);
            return prepareResult(400, new stdClass, "validation error");
        } 
            $result['cities'] = array();
            $result['cities'] = City::where('country_id', '=', $request->id
                    )->get(['id', 'name']);
            return prepareResult(200, $result, "Success");
    }

}
