<?php

namespace App\Http\Repository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CommonRepository {

    public function saveUserLatestViewedId($latestId, $type = 'activity') {
        $request = request();
        if (!$request->checkingForNewFeeds) {
            $key = "USER_{$request->loginUser->id}_SPORT_{$request->loginUser->sport_id}_{$type}_MAX_ID";
            if ($latestId > Cache::get($key, 0)) {
                Cache::forever($key, $latestId);
            }
        }
    }

}
