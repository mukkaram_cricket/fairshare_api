<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest as TransformsRequest;

class ConvertArabicIntegers extends TransformsRequest {

    /**
     * The attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password',
        'email',
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value) {
        if (in_array($key, $this->except, true)) {
            return $value;
        }

        $urdu = ['۰',
            '۱',
            '۲',
            '۳',
            '۴',
            '۵',
            '۶',
            '۷',
            '۸',
            '۹'
        ];

        $arabic = ['٠',
            '١',
            '٢',
            '٣',
            '٤',
            '٥',
            '٦',
            '٧',
            '٨',
            '٩'
        ];
        $english = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        return str_replace($arabic, $english, str_replace($urdu, $english, $value));
    }

}
