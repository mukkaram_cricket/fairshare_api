<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades;

class GlobalMiddleware {

    public function handle($request, Closure $next) {
        //----------GDPR DB Encryption-------------------------
        if ($request->filled('email')) {
            $email = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? true : false;
            if ($email) {
                $request['email'] = strtolower($request->email);
                $request['hashed_email'] = encrypted($request->email);
            } else {    // having phone value
                $request['email'] = convertToPhoneFormat($request->email);
                $request['hashed_email'] = encrypted($request->email);
            }
        }
        if ($request->filled('phone')) {
            $request['phone'] = convertToPhoneFormat($request->phone);
            $request['hashed_phone'] = encrypted($request->phone);
        }
        if ($request->filled('emails') && is_array($request->emails)) {
            $request['hashed_emails'] = array_map("encrypted", array_map("strtolower", $request->emails));
        }
        if ($request->filled('phones') && is_array($request->phones)) {
            $request['hashed_phones'] = array_map("encrypted", array_map("convertToPhoneFormat", $request->phones));
        }
        //---------------Response Headers------------------------
        header('Access-Control-Allow-Origin: *'); //header('Access-Control-Allow-Origin: '.config('app.web_url'));
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: Content-Type,Accept, Access-Control-Allow-Headers, Authorization,Device-Type,Local-Lang-Code, X-Requested-With,cache-control');
        header('Access-Control-Expose-Headers: pm_version,Video-Source,Server-Lang-Code');
        //---Handling Language Syncronization   // will overide in Verify Middleware if user is logged in
        header("Server-Lang-Code: " . $request->header('Local-Lang-Code', 'en_US'), true);
        Facades\Config::set('app.locale', $request->header('Local-Lang-Code', 'en_US'));

        return $next($request);
    }

}
