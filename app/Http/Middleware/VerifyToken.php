<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Cache;
use App\Models\User;
use Illuminate\Support\Facades;
use Closure;
use \stdClass;

class VerifyToken {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $verifyTokenType) {
        //---Compulsory = only LoggedIn users;;;  Optional + Token = LoggedIn + Guest Users
        if ($verifyTokenType == "compulsory" || ($verifyTokenType == "optional" && $request->header('Authorization'))) {
            if (!$request->header('Authorization')) {
                return response(['status' => 503, 'result' => new stdClass, 'message' => __('Token not provided'),]);
            }
            $tokenString = explode(' ', $request->header('Authorization'));
            $token = $tokenString[1];
            if ($token == 'null') {
                return response(['status' => 503, 'result' => new stdClass, 'message' => __('Token not provided'),]);
            }
            $tokenParts = explode(':', base64_decode($token));
            $userId = $tokenParts[0];
            $useragent = $request->header('User-Agent');
            $userApi = \App\Models\UserApi::where(['user_id' => $userId, 'token' => $tokenParts[1], 'status' => 1, 'login_type' => $request->header('Device-Type', 'web'), 'user_agent' => $useragent])
// For Expiry of token                ->where('last_used', '>=', now('utc)->subHours(config('app.token_expiry_time_in_hours')))
                    ->first();
            if (!$userApi) {
                return response(['status' => 503, 'result' => new stdClass, 'message' => __('Your Session Has Been Expired. Please Login Again'),]);
            }
            if ($userApi->timezone) {
                Facades\Config::set('app.timezone', $userApi->timezone);
                date_default_timezone_set($userApi->timezone);
            }
// For Expiry of token            $userApi->last_used = now('utc');
// For Expiry of token           $userApi->save();
            $currentUser = User::find($userId);
            //---Handling Language Syncronization
            header("Server-Lang-Code: " . $currentUser->locale, true);
            Facades\Config::set('app.locale', $currentUser->locale);
            $request->loginUser = $currentUser;
            $request->loginUserId = $userId;
            $request->loginUserToken = $tokenParts[1];
            $request->loginUser_slug = $currentUser->slug;
            $request->loginUser_role_id = $currentUser->role_id;
            $request->loginUser_sport_id = $currentUser->sport_id;

            $request->setUserResolver(function () use ($currentUser) {
                return $currentUser;
            });
            //----Maintaining Online Status Very Smartly
            if (!Cache::has("last_active{$request->loginUserId}")) {
                Cache::put("last_active{$request->loginUserId}", true, config('app.USER_ONLINE_INTERVAL', 10) / 2);
                User::whereKey($request->loginUserId)->update(['date_updated' => now('utc')]); // for online status
            }
        } else if ($verifyTokenType == "optional" && !$request->header('Authorization')) {  // public api
            $request->loginUser = null; //empty object
        }
        return $next($request);
    }

}
