<?php

namespace App\Http\Libraries;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use File;

class Uploader {

    protected $maxFileSize, $inputName, $file, $fileType, $valid, $filePath, $uploaded, $message, $mineType;
    public $fileName;

    function __construct($inputName = '', $maxFileSize = 26214400) {
        $this->maxFileSize = $maxFileSize; // 25 MB default size
        $this->inputName = $inputName;
        $this->valid = FALSE;
        $this->uploaded = FALSE;
        $this->message = '';
        $this->mineType = '';
    }

    public function setInputName($inputName) {
        $this->inputName = $inputName;
        return $this;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function isValidFile() {
        if (Input::hasFile($this->inputName) || $this->file) {
            $this->file = (!$this->file) ? Input::file($this->inputName) : $this->file;
            $this->mimeType = $this->file->getMimeType();
            if ($this->file->isValid()) {
                if ($this->file->getSize() <= $this->maxFileSize) {
                    $fileName = explode('.', $this->file->getClientOriginalName());
                    $this->fileName = (strlen($fileName[0]) > 150) ? str_limit($fileName[0], 150, '') : $fileName[0];
                    $this->valid = TRUE;
                } else {
                    $this->message = __('File size too large');
                }
            } else {
                $this->message = $this->file->getErrorMessage();
            }
        }
        return $this->valid;
    }

    public function hasValidDimentions($minWidth, $minHeight) {
        if ($this->valid) {
            $validator = Validator::make([$this->inputName => 'image'], [$this->inputName => $this->file]);
            if ($validator->passes()) {
                $this->fileType = 'image';
                $imageData = getimagesize($this->file->getRealPath());
                if ($imageData[0] >= $minWidth && $imageData[1] >= $minHeight) {
                    return TRUE;
                } else {
                    $this->message = __('Image must be at least') . ' ' . $minWidth . 'x' . $minHeight;
                }
            } else {
                $this->message = $validator->messages()->first();
            }
        }
        return false;
    }

    public function upload($uploadPath, $fileName = 'file', $temp = false, $thumb = []) {
        if ($this->valid) {
            $fileName = $fileName . '-' . time() . '.' . $this->file->getClientOriginalExtension();
            $this->filePath = (($temp) ? 'temp' : $uploadPath) . '/';
            $directoryPath = storage_path(config('app.images_relative_path') . "/" . $this->filePath);
            if (!File::exists($directoryPath)) {
                File::makeDirectory($directoryPath, 0777, true);
            }
            $this->file->move($directoryPath, $fileName);
            if (!empty($thumb)) {
                list($width, $height) = $thumb;
            }
            $this->filePath = $this->filePath . $fileName;
            $this->file = NULL;
            $this->valid = FALSE;
            $this->uploaded = TRUE;
        } else {
            $this->uploaded = FALSE;
        }
    }

    public function getMimeType() {
        return $this->mimeType;
    }

    public function getUploadedPath() {
        return $this->filePath;
    }

    public function isUploaded() {
        return $this->uploaded;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getMaxFileSize() {
        return $this->maxFileSize;
    }

}
