<?php

namespace App\Traits;

use App\Models\User;
use App\Models\EmailNotification;
use App\Models\Notification;
use App\Jobs\ProcessEmail;
use App\Jobs\Reminder;

trait CommonTraits {

    private function saveNotification($notification_for_id, $type, $title, $description, $relative_id, $module = 'generals', $subject = '', $message = '', $extra = '') {
        $request = request();
        // for the notification of uploaded video
        if ($type == 'video_live' || $type == 'job_reminder') {
            $request->loginUserId = $notification_for_id;
        }
        if ($notification_for_id == $request->loginUserId && ($type != 'video_live' && $type != 'job_reminder')) {
            return false;
        }
        $notification_for = User::find($notification_for_id);
        if(!$notification_for){
            return false;            
        }

//---------------------In-App Notificatioin-----------------------------------------//        
        $notif = Notification::create([
                    'user_id' => $notification_for->id,
                    'sender_id' => $request->loginUserId,
                    'title' => $title,
                    'description' => $description,
                    'module' => $module,
                    'type' => $type,
                    'related_id' => $relative_id,
                    'extra' => $extra
        ]);

//---------------------FCM Notification as per user setting------------------------------//

        $profileStatExtra = \App\Models\ProfileStat::where('user_id', $notification_for->id)->first(['extra']);
        if (empty($profileStatExtra->extra)) {
            //Sending FCM Notifications
            $this->fcmNotification($notification_for, $type, $title, $description, $module, $relative_id, $extra, $notif->id);
        } else {
            $extra_fcm_setting_decode = json_decode($profileStatExtra->extra);
            if (empty($extra_fcm_setting_decode->fcm_setting)) {
                $this->fcmNotification($notification_for, $type, $title, $description, $module, $relative_id, $extra, $notif->id);
            } else {
                $fcm_setting = $extra_fcm_setting_decode->fcm_setting;
                if (empty($fcm_setting->$type)) {
                    $this->fcmNotification($notification_for, $type, $title, $description, $module, $relative_id, $extra, $notif->id);
                } else {
                    if ($type != 'video_live' && $type != 'job_reminder' && $fcm_setting->$type == 'true') {
                        //Sending FCM Notifications
                        $this->fcmNotification($notification_for, $type, $title, $description, $module, $relative_id, $extra, $notif->id);
                    }
                }
            }
        }

//-------------------------------------Sending Emails Notificatoins as well-------------------------------//
        if ($notif && $notification_for->email && $notification_for->is_email_verified) {
            if ($type == 'friend_request') {
                $Ouser = User::where('id', $relative_id)->first();
                $request_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/friends/requests';
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';
                $o_link = config('app.VUE_APP_URL') . '/users/' . $Ouser->slug;
                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $request_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $Ouser->name,
                    'name_link' => $o_link,
                    'unsub_link' => $unsub_link
                ];
                $friend_request = EmailNotification::where('user_id', $notification_for->id)->first(['friend_req_recieved']);
                if (!$friend_request || ($friend_request && $friend_request->friend_req_recieved)) {
                    ProcessEmail::dispatch('accept_friend_req', $data_var)->onQueue('low');
                    //send_email('accept_friend_req', $data_var);
                }
            } else if ($type == 'request_accepted') {
                $Ouser = User::where('id', $relative_id)->first();
                $verification_link = config('app.VUE_APP_URL') . "/users/" . $Ouser->slug;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $verification_link,
                    "subject" => $title,
                    "to" => $notification_for->email,
                    'from' => $Ouser->name,
                    'unsub_link' => $unsub_link
                ];
                $request_accepted = EmailNotification::where('user_id', $notification_for->id)->first(['friend_req_accepted']);
                if (!$request_accepted || ($request_accepted && $request_accepted->friend_req_accepted)) {
                    ProcessEmail::dispatch('friend_req_accepted', $data_var)->onQueue('low');
//                    send_email('friend_req_accepted', $data_var);
                }
            } else if ($type == 'comment_reply') {
                $Ouser = User::where('id', $request->loginUserId)->first();
//                    $feeds = Feed::where('id', $relative_id)->first();
//                    $reciever = User::where('id', $feeds->user_id);
                $verification_link = config('app.VUE_APP_URL') . "/users/" . $notification_for->slug . "/feed/" . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $verification_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $Ouser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
//                    logger('sf ', $data_var);
                $comment_reply = EmailNotification::where('user_id', $notification_for->id)->first(['comment_reply']);
                if (!$comment_reply || ($comment_reply && $comment_reply->comment_reply)) {
                    ProcessEmail::dispatch('comment_reply', $data_var)->onQueue('low');
                    //send_email('comment_reply', $data_var);
                }
            } else if ($type == 'feed_comment') {
//                    $feeds = Feed::where('id', $relative_id)->first();
//                    $reciever = User::where('id', $feeds->user_id);
                $feed_link = config('app.VUE_APP_URL') . "/feed/" . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $feed_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
//                    logger('sf ', $data_var);
                $feed_comment = EmailNotification::where('user_id', $notification_for->id)->first(['comment_reply']);
                if (!$feed_comment || ($feed_comment && $feed_comment->comment_reply)) {
                    ProcessEmail::dispatch('feed_comment', $data_var)->onQueue('low');
                    //send_email('comment_reply', $data_var);
                }
            } else if ($type == 'new_msg') {
                $Ouser = User::where('id', $request->loginUserId)->first();
                $verification_link = config('app.VUE_APP_URL') . "/users/" . $notification_for->slug . "/messages/view/" . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'sender' => $Ouser->name,
                    'link' => $verification_link,
                    "subject" => $title,
                    'msgsub' => $subject,
                    "to" => $notification_for->email,
                    "des" => $message,
                    'unsub_link' => $unsub_link
                ];
                $new_msg = EmailNotification::where('user_id', $notification_for->id)->first(['new_message']);
                if (!$new_msg || ($new_msg && $new_msg->new_message)) {
//                    ProcessEmail::dispatch('sent_msg', $data_var)->onQueue('low');
                    send_email('sent_msg', $data_var);
                }
            } else if ($type == 'msg_reply') {
                $Ouser = User::where('id', $request->loginUserId)->first();
                $verification_link = config('app.VUE_APP_URL') . "/users/" . $notification_for->slug . "/messages/view/" . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'sender' => $Ouser->name,
                    'link' => $verification_link,
                    "subject" => $title,
                    'msgsub' => $subject,
                    "to" => $notification_for->email,
                    "des" => $message,
                    'unsub_link' => $unsub_link
                ];
                $msg_reply = EmailNotification::where('user_id', $notification_for->id)->first(['message_reply']);
                if (!$msg_reply || ($msg_reply && $msg_reply->message_reply)) {
//                    ProcessEmail::dispatch('sent_msg', $data_var)->onQueue('low');
                    send_email('sent_msg', $data_var);
                }
            } else if ($type == 'feed_taged') {
                //$Ouser = User::where('id', $request->loginUserId)->first();
//                    $feeds = Feed::where('id', $relative_id)->first();
//                    $reciever = User::where('id', $feeds->user_id);
                $feed_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/feed/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $feed_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $feed_taged = EmailNotification::where('user_id', $notification_for->id)->first(['activity_mention']);
                if (!$feed_taged || ($feed_taged && $feed_taged->activity_mention)) {
                    ProcessEmail::dispatch('activity_mention', $data_var)->onQueue('low');
                }
            } else if ($type == 'comment_mention') {

                $feed_link = config('app.VUE_APP_URL') . '/feed/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $feed_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $comment_mention = EmailNotification::where('user_id', $notification_for->id)->first(['comment_mention']);
                if (!$comment_mention || ($comment_mention && $comment_mention->comment_mention)) {
                    ProcessEmail::dispatch('comment_mention', $data_var)->onQueue('low');
                }
            } else if ($type == 'match_request') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_request = EmailNotification::where('user_id', $notification_for->id)->first(['match_request']);
                if (!$match_request || ($match_request && $match_request->match_request)) {
                    ProcessEmail::dispatch('match_request', $data_var)->onQueue('medium');
//                    send_email('match_request', $data_var);
                }
            } else if ($type == 'match_joining_accepted') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_joining_accepted = EmailNotification::where('user_id', $notification_for->id)->first(['match_joining_accepted']);
                if (!$match_joining_accepted || ($match_joining_accepted && $match_joining_accepted->match_joining_accepted)) {
                    ProcessEmail::dispatch('match_joining_accepted', $data_var)->onQueue('medium');
//                    send_email('match_joining_accepted', $data_var);
                }
            } else if ($type == 'match_joining_rejected') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_joining_rejected = EmailNotification::where('user_id', $notification_for->id)->first(['match_joining_rejected']);
                if (!$match_joining_rejected || ($match_joining_rejected && $match_joining_rejected->match_joining_rejected)) {
                    ProcessEmail::dispatch('match_joining_rejected', $data_var)->onQueue('medium');
//                    send_email('match_joining_rejected', $data_var);
                }
            } else if ($type == 'player_invite_match') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];

                $invite_match = EmailNotification::where('user_id', $notification_for->id)->first(['player_invite_match']);
                if (!$invite_match || ($invite_match && $invite_match->player_invite_match)) {
                    ProcessEmail::dispatch('player_invite_match', $data_var)->onQueue('medium');
//                    send_email('player_invite_match', $data_var);
                }
            } else if ($type == 'player_invite_team') {

                $club_slug = \App\Models\Club::where('id', $relative_id)->first(['slug']);
                $slug = $club_slug->slug;
                $match_link = config('app.VUE_APP_URL') . '/clubs/' . $slug . '/teams';
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];

                $player_invite = EmailNotification::where('user_id', $notification_for->id)->first(['player_invite_team']);
                if (!$player_invite || ($player_invite && $player_invite->player_invite_team)) {
                    ProcessEmail::dispatch('player_invite_team', $data_var)->onQueue('medium');
//                    send_email('player_invite_team', $data_var);
                }
            } else if ($type == 'match_request_accepted') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_request_accepted = EmailNotification::where('user_id', $notification_for->id)->first(['match_request_accepted']);
                if (!$match_request_accepted || ($match_request_accepted && $match_request_accepted->match_request_accepted)) {
                    ProcessEmail::dispatch('match_request_accepted', $data_var)->onQueue('medium');
//                    send_email('match_request_accepted', $data_var);
                }
            } else if ($type == 'match_request_rejected') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_request_rejected = EmailNotification::where('user_id', $notification_for->id)->first(['match_request_rejected']);
                if (!$match_request_rejected || ($match_request_rejected && $match_request_rejected->match_request_rejected)) {
                    ProcessEmail::dispatch('match_request_rejected', $data_var)->onQueue('medium');
//                    send_email('match_request_rejected', $data_var);
                }
            } else if ($type == 'match_result_added') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $match_result_added = EmailNotification::where('user_id', $notification_for->id)->first(['match_result_added']);
                if (!$match_result_added || ($match_result_added && $match_result_added->match_result_added)) {
                    ProcessEmail::dispatch('match_result_added', $data_var)->onQueue('medium');
//                    send_email('match_result_added', $data_var);
                }
            } else if ($type == 'player_result_added') {

                $match_link = config('app.VUE_APP_URL') . '/match-detail/' . $relative_id;
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';

                $data_var = [
                    'name' => $notification_for->name,
                    'link' => $match_link,
                    'subject' => $title,
                    'to' => $notification_for->email,
                    'from' => $request->loginUser->name,
                    'content' => $subject,
                    'name_link' => config('app.VUE_APP_URL') . '/users/' . $request->loginUser->slug,
                    'unsub_link' => $unsub_link
                ];
                $player_result_added = EmailNotification::where('user_id', $notification_for->id)->first(['player_result_added']);
                if (!$player_result_added || ($player_result_added && $player_result_added->player_result_added)) {
                    ProcessEmail::dispatch('player_result_added', $data_var)->onQueue('medium');
//                    send_email('player_result_added', $data_var);
                }
            } else if ($type == 'official_req_rejected') {
                $Ouser = User::where('id', $request->loginUserId)->first();
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';
                $data_var = [
                    'name' => $notification_for->name,
                    'sender' => $Ouser->name,
                    'link' => '',
                    "subject" => $title,
                    "to" => $notification_for->email,
                    "des" => $description,
                    'unsub_link' => $unsub_link
                ];
                $req_rejected = EmailNotification::where('user_id', $notification_for->id)->first(['official_req_rejected']);
                if (!$req_rejected || ($req_rejected && $req_rejected->official_req_rejected)) {
                    ProcessEmail::dispatch('official_req_rejected', $data_var)->onQueue('low');
//                    send_email('official_req_rejected', $data_var);
                }
            } else if ($type == 'official_req_accepted') {
                $Ouser = User::where('id', $request->loginUserId)->first();
                $unsub_link = config('app.VUE_APP_URL') . '/users/' . $notification_for->slug . '/settings/notifications';
                $data_var = [
                    'name' => $notification_for->name,
                    'sender' => $Ouser->name,
                    'link' => '',
                    "subject" => $title,
                    "to" => $notification_for->email,
                    "des" => $description,
                    'unsub_link' => $unsub_link
                ];
                $req_accepted = EmailNotification::where('user_id', $notification_for->id)->first(['official_req_accepted']);
                if (!$req_accepted || ($req_accepted && $req_accepted->official_req_accepted)) {
                    ProcessEmail::dispatch('official_req_accepted', $data_var)->onQueue('low');
//                    send_email('official_req_accepted', $data_var);
                }
            }
        }
    }

    private function fcmNotification($user, $type, $title, $description, $module, $relative_id, $extra, $notification_id) {
        $allFcmTokens = \App\Models\UserApi::where('user_id', $user->id)->whereNotNull('fcm_token')->get();
        $allTokens['android'] = $allFcmTokens->where('login_type', 'android')->pluck('fcm_token')->toArray();
        $allTokens['ios_web'] = $allFcmTokens->where('login_type', '!=', 'android')->pluck('fcm_token')->toArray();
        foreach ($allTokens as $key => $fcmTokens) {
            $fcmTokens = array_filter(array_values(array_unique($fcmTokens)));
            if (count($fcmTokens) > 0) {
//---------------you can copy the latest one switch code from notification controllr -> getNotifications()
                switch ($type) {
                    case 'feed_like':
                    case 'feed_rating':
                    case 'feed_comment':
                    case 'feed_taged':
                    case 'comment_reply':
                    case 'request_accepted':
                    case 'friend_request':
                    case 'new_msg':
                    case 'msg_reply':
                    case 'skill_endorsment':
                    case 'recommendation':
                    case 'job_apply':
                    case 'profile_view':
                    case 'club_official_req':
                    case 'job_invitation':
                    case 'official_req_accepted':
                    case 'official_req_rejected':
                    case 'comment_mention':
                    case 'match_request':
                    case 'match_request_accepted':
                    case 'match_request_rejected':
                    case 'player_invite_match':
                    case 'player_invite_team':
                    case 'match_joining_accepted':
                    case 'match_joining_rejected':
                    case 'match_result_added':
                    case 'player_result_added':

                        $description = request()->loginUser->name . $description;
                        break;
                    default:
                        break;
                }
//-----------------------------------------------------------------------
                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = [
                    'content_available' => true,
                    'priority' => "high",
//                'to' => $user->fcm_token,
                    'registration_ids' => $fcmTokens,
//                    'notification' => [
//                        "title" => $title,
//                        "body" => $description
//                    ],
                    'data' => [
                        'user_id' => $user->id,
                        'sender_id' => request()->loginUserId,
                        'title' => $title,
                        'description' => $description,
                        'module' => $module,
                        'type' => $type,
                        'related_id' => $relative_id,
                        'extra' => $extra,
                        "badge" => 11,
                        'id' => $notification_id
                    ],
                ];
                if ($key == 'ios_web') {
                    $fields['notification'] = [
                        "title" => $title,
                        "body" => $description,
                        "badge" => 10,
                    ];
                }
                $fields = json_encode($fields);
                $headers = [
                    'Authorization: key=' . "AAAAOBIN9Dc:APA91bEs6Dr9UBcMDPBr1e9-STPPYf7tjWP9Y9TfO2038-iRHo_pbrnIDv2XXVaHa_J3dCbbdFnPvN60sG38iVWbSWbEa1ll7cwsm3OvZGySMsPxDrsCY7c84XfIWf8HS6KwLoCMpQFS",
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                $result = curl_exec($ch);
                curl_close($ch);
//            if($result) echo $result;
            }
        }
    }

    private function getUserCountryData($ip) {
        $url = 'http://ip-api.com/json/' . $ip;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function getFieldValues($tables, $function_name, $related_id, $related_column = 'user_id') {
        $allColumns = [];
        $index = 0;
        $values = [];
        $tables_count = count($tables);
        if ($tables_count > 1) {
            $values[$index] = [];
        }
        foreach ($tables as $table => $columns) {
            $allColumns = array_merge($allColumns, $columns);
            $where_column = $related_column;
            if ($table == 'User') {
                $where_column = 'id';
            }
            $className = 'App\Models\\' . $table;
            if (class_exists($className)) {
                $query = $className::where($where_column, $related_id);
                $relationships = [];
                $this->$function_name($query, $columns, $relationships);
                if ($table == 'UserCareer') {
                    $query->orderBy('order', 'asc');
                }
                $data = $query->get($columns)->toArray();
                if ($data) {
                    foreach ($relationships as $key => $relation) {
                        $object_key = key($relation);
                        for ($index1 = 0; $index1 < count($data); $index1++) {
                            $value = $data[$index1][$object_key];
                            if (!$value) {
                                $data[$index1][$key] = '';
                                unset($data[$index1][$object_key]);
                                continue;
                            }
                            if (gettype(key($value)) == 'integer') {
                                $obj_values = [];
                                foreach ($value as $obj) {
                                    $obj_values[] = $obj[$relation[$object_key]];
                                }
                                $data[$index1][$key] = implode(',', $obj_values);
                                unset($data[$index1][$object_key]);
                            } else {
                                $data[$index1][$key] = $value[$relation[$object_key]];
                                unset($data[$index1][$object_key]);
                            }
                        }
                    }
                    if ($tables_count > 1) {
                        $values[$index] = array_merge_recursive($values[$index], $data[$index]);
                    } else {
                        $values = $data;
                    }
                } else {
                    if ($tables_count > 1) {
                        foreach ($columns as $column) {
                            $data[$column] = '';
                        }
                        $values[$index] = array_merge_recursive($values[$index], $data);
                    } else {
                        $values = $data;
                    }
                }
//                $index++;
            } else {
                return false;
            }
        }
        return $values;
    }

    private function addBasicProfileRelations(&$query, &$columns, &$relationships) {
        if (in_array('other_languages', $columns)) {
            $query->with('user_languages');
            $relationships['other_languages'] = ['user_languages' => 'name'];
            $key = array_search('other_languages', $columns);
            unset($columns[$key]);
            $columns[] = 'id';
        }
        if (in_array('work_area', $columns)) {
            $query->with('work_areas');         //getting value from relation
            $relationships['work_area'] = ['work_areas' => 'name'];     //setting custom relation to detect value from fetched relation result
            $key = array_search('work_area', $columns);                 // removing column name as it was temporay
            unset($columns[$key]);
        }
        if (in_array('nationality_id', $columns)) {
            $query->with('country:id,nationality');
            $relationships['nationality_id'] = ['country' => 'nationality'];
        }
        if (in_array('city_residence_id', $columns)) {
            $query->with('cityResidence:id,name');
            $relationships['city_residence_id'] = ['cityResidence' => 'name'];
        }
        if (in_array('position_id', $columns)) {
            $query->with('position:id,name');
            $relationships['position_id'] = ['position' => 'name'];
        }
        if (in_array('level_id', $columns)) {
            $query->with('level:id,name');
            $relationships['level_id'] = ['level' => 'name'];
        }
        if (in_array('club_role_id', $columns)) {
            $query->with('clubRole:id,name');
            $relationships['club_role_id'] = ['clubRole' => 'name'];
        }
        if (in_array('preferred_foot_id', $columns)) {
            $query->with('preferredFoot:id,name');
            $relationships['preferred_foot_id'] = ['preferredFoot' => 'name'];
        }
        if (in_array('gender_id', $columns)) {
            $query->with('gender:id,name');
            $relationships['gender_id'] = ['gender' => 'name'];
        }
        if (in_array('previous_club_id', $columns)) {
            $query->with('previousClub:id,name');
            $relationships['previous_club_id'] = ['previousClub' => 'name'];
        }
        if (in_array('current_club_id', $columns)) {
            $query->with('currentClub:id,name');
            $relationships['current_club_id'] = ['currentClub' => 'name'];
        }
        if (in_array('mother_language_id', $columns)) {
            $query->with('motherLanguage:id,name');
            $relationships['mother_language_id'] = ['motherLanguage' => 'name'];
        }
        if (in_array('country_id', $columns)) {
            $query->with('country');
            $relationships['country_id'] = ['country' => 'name'];
        }
        if (in_array('country_residence_id', $columns)) {
            $query->with('countryResidence');
            $relationships['country_residence_id'] = ['countryResidence' => 'name'];
        }
        if (in_array('playing_role_id', $columns)) {
            $query->with('playingRole:id,name');
            $relationships['playing_role_id'] = ['playingRole' => 'name'];
        }
        if (in_array('batting_style_id', $columns)) {
            $query->with('battingStyle:id,name');
            $relationships['batting_style_id'] = ['battingStyle' => 'name'];
        }
        if (in_array('bowling_style_id', $columns)) {
            $query->with('bowlingStyle:id,name');
            $relationships['bowling_style_id'] = ['bowlingStyle' => 'name'];
        }
    }

    private function addUserCareerRelations(&$query, $columns, &$relationships) {
        if (in_array('all_club_id', $columns)) {
            $query->with('allClub:id,name');
            $relationships['all_club_id'] = ['allClub' => 'name'];
        }
        if (in_array('season_id', $columns)) {
            $query->with('season');
            $relationships['season_id'] = ['season' => 'name'];
        }
        if (in_array('level_id', $columns)) {
            $query->with('level:id,name');
            $relationships['level_id'] = ['level' => 'name'];
        }
        if (in_array('status_id', $columns)) {
            $query->with('status');
            $relationships['status_id'] = ['status' => 'name'];
        }
        if (in_array('league_id', $columns)) {
            $query->with('league:id,name');
            $relationships['league_id'] = ['league' => 'name'];
        }
        if (in_array('country_id', $columns)) {
            $query->with('country:id,name');
            $relationships['country_id'] = ['country' => 'name'];
        }
    }

    private function addClubRelations(&$query, $columns, &$relationships) {
        if (in_array('country_id', $columns)) {
            $query->with('country:id,name');
            $relationships['country_id'] = ['country' => 'name'];
        }
        if (in_array('city_id', $columns)) {
            $query->with('city:id,name');
            $relationships['city_id'] = ['city' => 'name'];
        }
        if (in_array('established_in_id', $columns)) {
            $query->with('establishedIn:id,name');
            $relationships['established_in_id'] = ['establishedIn' => 'name'];
        }
    }

    private function addJobRelations(&$query, &$columns, &$relationships) {
        if (in_array('club_id', $columns)) {
            $query->with('club:id,title');
            $relationships['club_id'] = ['club' => 'title'];
        }
        if (in_array('category_id', $columns)) {
            $query->with('category:id,name');
            $relationships['category_id'] = ['category' => 'name'];
        }
        if (in_array('position_id', $columns)) {
            $query->with('positions');
            $relationships['position_id'] = ['positions' => 'name'];
            $key = array_search('position_id', $columns);
            unset($columns[$key]);
            $columns[] = 'id';
        }
    }

}
