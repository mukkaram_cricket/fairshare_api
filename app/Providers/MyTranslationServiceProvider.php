<?php

namespace App\Providers;

use Illuminate\Translation\TranslationServiceProvider;

class MyTranslationServiceProvider extends TranslationServiceProvider {

    public function register() {
        $this->registerLoader();

        $this->app->singleton('translator', function ($app) {
            $loader = $app['translation.loader'];

            // When registering the translator component, we'll need to set the default
            // locale as well as the fallback locale. So, we'll grab the application
            // configuration so we can easily get both of these values from there.
            $locale = $app['config']['app.locale'];

            $trans = new CustomTranslator($loader, $locale);

            $trans->setFallback($app['config']['app.fallback_locale']);

            return $trans;
        });
    }

}

class CustomTranslator extends \Illuminate\Translation\Translator {

    public function getFromJson($key, array $replace = [], $locale = null) {
        $locale = $locale ?: $this->locale;

        //-----------For Production------------
        if ($locale == 'en_US' && config('app.env') == 'production')
            return $key;

        // For JSON translations, there is only one file per locale, so we will simply load
        // that file and then we will be ready to check the array for the key. These are
        // only one level deep so we do not need to do any fancy searching through it.
        $this->load('*', '*', $locale);

        $line = $this->loaded['*']['*'][$locale][$key] ?? null;
        //-------------For Development
        if ($locale == 'en_US' && $line == null && !is_numeric($key) && config('app.env') != 'production') {
            $this->loaded['*']['*'][$locale][$key] = $key;
            $langFile = $this->loaded['*']['*'][$locale];
            $langFile[$key] = $key;
            file_put_contents(resource_path('lang/en_US.json'), json_encode($langFile, JSON_PRETTY_PRINT));
            $line = $key;
        }
        //----------------------
        // If we can't find a translation for the JSON key, we will attempt to translate it
        // using the typical translation file. This way developers can always just use a
        // helper such as __ instead of having to pick between trans or __ with views.
        if (!isset($line)) {
            $fallback = $this->get($key, $replace, $locale);

            if ($fallback !== $key) {
                return $fallback;
            }
        }

        return $this->makeReplacements($line ?: $key, $replace);
    }

}
