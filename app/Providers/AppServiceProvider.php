<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        \Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return (bool) preg_match('/^[+][0-9]+$/', $value);
        }, "invalid phone number");


        /*
          \Validator::extend('base64', function ($attribute, $value, $parameters, $validator) {
          if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $value)) {
          return true;
          } else {
          return false;
          }
          }, "Invalid Base64 String");

          \Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
          $explode = explode(',', $value);
          $allow = ['png', 'jpg', 'jpeg', 'svg'];
          $format = str_replace(
          [
          'data:image/',
          ';',
          'base64',
          ], [
          '', '', '',
          ], $explode[0]
          );
          // check file format
          if (!in_array($format, $allow)) {
          return false;
          }
          // check base64 format
          if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
          return false;
          }
          return true;
          }, "Invalid Base64 Image");

          \Validator::extend('validate_json', function ($attribute, $value, $parameters, $validator) {
          json_decode($value);
          return (json_last_error() == JSON_ERROR_NONE) ? true : false;
          }, "Invalid JSON String");
         * */
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
