<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Twilio\Rest\Client;
use \stdClass;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
            //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception) {
        if (config('app.env') == 'production') {
            $data_var = [
                'name' => 'Developer',
                'sender' => 'Playermatch',
                'link' => '',
                "subject" => 'Exception Occured',
                "to" => 'hamidali18@ymail.com',
                "des" => '',
                "exception" => $exception->getMessage(),
                'unsub_link' => ''
            ];
            if ($this->shouldReport($exception)) {
                \App\Jobs\ProcessEmail::dispatch('exception_log', $data_var)->onQueue('low');
            }
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) {

        // findOrFail Exception handler
        if ($request->expectsJson() && $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response(['status' => 422, 'message' => 'Content does not exist anymore or you don not have permission to view', 'result' => new \stdClass()]);
        }
        // Validator validation fail Exception handling
        if ($request->expectsJson() && $exception instanceof \Illuminate\Validation\ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            $ErrorMessage = array_first($errors);
            $firstErrorMessage = $ErrorMessage[0];
//            $allErrorMessages = '';
//            foreach ($errors as $error) {
//                $allErrorMessages .= $error[0];
//            }
            return response(['status' => 422, 'message' => $firstErrorMessage, 'result' => ['failed_rule' => array_first($exception->validator->failed())]]);
        }

        if ($request->expectsJson() && ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException || $exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException)) {
            return response(['status' => 404, 'message' => 'Sorry, the page you are looking for could not be found. check your api and api type', 'result' => ['exception' => $exception->getMessage()]]);
        }

        if ($request->expectsJson() && $exception instanceof Twilio\Exceptions\RestException) {
            return response(['status' => 404, 'message' => 'Sorry, the page you are looking for could not be found. check your api and api type', 'result' => new stdClass()]);
        }
        // convert remainging all Exception into JSON formate
        if ($exception && config('app.debug') == false) {
            return response(['message' => ($exception->getMessage()) ? $exception->getMessage() : 'Invalid request! Please check your api.', 'status' => 404, 'result' => new stdClass()]);
        }


        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception) {
        if ($request->expectsJson()) {
            /** return response()->json(['error' => 'Unauthenticated.'], 401); */
            $response = ['status' => 'error', 'message' => 'You pass invalid token'];
            return response()->json($response);
        }
        // return redirect()->guest('login');
    }

}
