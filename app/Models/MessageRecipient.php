<?php

namespace App\Models;

class MessageRecipient extends \App\Models\Base\MessageRecipient
{
	protected $fillable = [
		'is_view'
	];
}
