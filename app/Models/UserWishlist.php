<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Models\Base\UserInvitationHistory;
class UserWishlist extends \App\Models\Base\UserWishlist
{
	protected $fillable = [
		'user_id',
		'name',
		'date',
		'status',
		'created_at',
		'updated_at'
	];


	public function  activity(){
	    return $this->hasMany(UserInvitationHistory::class,'user_wishlist_id');
    }

    public function product()
    {
        return $this->hasManyThrough(Product::class,UserWishlistItem::class,'user_wishlist_id','id'
        );
    }

}
