<?php

namespace App\Models;

class Message extends \App\Models\Base\Message
{
	protected $fillable = [
		'thread_id',
		'sender_id',
		'message'
	];
}
