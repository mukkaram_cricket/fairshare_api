<?php

namespace App\Models;

class MessageThread extends \App\Models\Base\MessageThread
{
	protected $fillable = [
		'type',
		'user_id',
		'subject'
	];
}
