<?php

namespace App\Models;

class UserWishlistItem extends \App\Models\Base\UserWishlistItem
{
	protected $fillable = [
		'user_wishlist_id',
		'product_id',
		'created_at',
		'updated_at'
	];
}
