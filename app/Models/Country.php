<?php

namespace App\Models;

class Country extends \App\Models\Base\Country {

    protected $appends = array('flag');

    public function getFlagAttribute() {
        if (isset($this->attributes['name'])) {
            return str_slug($this->attributes['name']);
        }
        if (isset($this->attributes['label'])) {
            return str_slug($this->attributes['label']);
        }
    }

    public function getNameAttribute($value) {
        return __($value, [], 'static_' . config('app.locale'));
    }

    public function getLabelAttribute($value) {
        return __($value, [], 'static_' . config('app.locale'));
    }

    public function getNationalityAttribute($value) {
        return __($value, [], 'static_' . config('app.locale'));
    }

}
