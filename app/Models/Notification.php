<?php

namespace App\Models;

class Notification extends \App\Models\Base\Notification {

    public function sender() {
        return $this->belongsTo(\App\Models\User::class, 'sender_id', 'id');
    }

    public function getExtraAttribute($value) {
        return $value ? json_decode($value) : new \stdClass();
    }

    public function getDescriptionAttribute($value) {
        return __($value);
    }

}
