<?php

namespace App\Models;

class Favorite extends \App\Models\Base\Favorite
{
	protected $fillable = [
		'user_id',
		'product_id',
		'created_at',
		'updated_at'
	];
}
