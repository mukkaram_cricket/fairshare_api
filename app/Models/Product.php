<?php

namespace App\Models;

class Product extends \App\Models\Base\Product
{
	protected $fillable = [
		'brand_id',
		'category_id',
		'product_image_id',
		'name',
		'description',
		'purchase_price',
		'discount',
		'actual_price',
		'slug',
		'status',
		'created_at',
		'updated_at'
	];

	public function images(){
	    return $this->hasMany(ProductImage::class);
    }
}
