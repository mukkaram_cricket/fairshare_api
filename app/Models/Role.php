<?php

namespace App\Models;

class Role extends \App\Models\Base\Role
{
	protected $fillable = [
		'name',
		'created_at',
		'updated_at'
	];
}
