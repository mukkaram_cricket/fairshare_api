<?php

namespace App\Models;

class Category extends \App\Models\Base\Category
{
	protected $fillable = [
		'name',
		'slug',
		'avatar',
		'parent_id',
		'status',
		'created_at',
		'updated_at'
	];

	public function childs(){
	    return $this->hasMany(self::class,'parent_id');
    }
    public function parent(){
        return $this->belongsTo(self::class,'parent_id');
    }

}
