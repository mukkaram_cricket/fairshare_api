<?php

namespace App\Models;

class Autocomplete extends \App\Models\Base\Autocomplete {

    protected $hidden = [
        'type',
    ];

    public function getImageAttribute($value) {
        return is_null($value) ? $value : imagesPath($value);
    }

}
