<?php

namespace App\Models;

class UserWishlistItemDetail extends \App\Models\Base\UserWishlistItemDetail
{
	protected $fillable = [
		'user_wishlist_item_id',
		'user_id',
		'share_member',
		'status',
		'created_at',
		'updated_at'
	];
}
