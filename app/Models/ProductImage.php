<?php

namespace App\Models;

class ProductImage extends \App\Models\Base\ProductImage
{
	protected $fillable = [
		'name',
		'product_id',
		'created_at',
		'updated_at'
	];

	public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
