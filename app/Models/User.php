<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class User extends \App\Models\Base\User {

    protected $hidden = [
        'password', 'pivot',
    ];
    protected $myCasts = [
        'is_friend' => 'bool',
        'is_request_sent' => 'bool',
        'is_awating_responce' => 'bool',
        'has_profile' => 'bool',
        'has_skills' => 'bool',
        'has_career' => 'bool',
    ];



    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAvatarAttribute($value) {
        $path = config('app.img_url');
        return (is_null($value)) ? $path . "/default/default_avatar.png" : $path . "/users/" . "{$this->attributes['id']}/ProfilePicture/{$this->attributes['avatar']}";
    }

    public function getEmailAttribute($value) {
        return decrypted($value);
    }

    public function getPhoneAttribute($value) {
        return decrypted($value);
    }

    public function endorsed_skills() {
        return $this->belongsToMany(\App\Models\UserSkill::class, 'enndorsements', 'user_id', 'user_skill_id');
    }

    public function add_skills() {
        return $this->belongsToMany(\App\Models\Skill::class, 'user_skills');
    }

    public function following() {
        return $this->belongsToMany(\App\Models\Club::class, 'club_followers', 'user_id', 'club_id');
    }

    public function user_languages() {
        return $this->belongsToMany(\App\Models\Language::class, 'other_languages', 'user_id', 'language_id');
    }

    public function work_areas() {
        return $this->belongsToMany(\App\Models\Country::class, 'multiselect', 'entity_id', 'source_id')->where('type', 'work_area');
    }

    public function user_skills() {
        return $this->belongsToMany(\App\Models\Skill::class, 'user_skills');
    }

    public function skills() {
        return $this->hasMany(\App\Models\UserSkill::class);
    }

    public function enndorsements() {
        return $this->hasMany(\App\Models\Enndorsement::class);
    }

    public function friends() {
        return $this->hasMany(\App\Models\Friendship::class);
    }

    public function spamUsers() {
        return $this->hasMany(\App\Models\SpamReport::class)->where('module', 'User');
    }

    public function spamClubs() {
        return $this->hasMany(\App\Models\SpamReport::class)->where('module', 'Club');
    }

    public function spamFeeds() {
        return $this->hasMany(\App\Models\SpamReport::class)->where('module', 'Feed');
    }

}
