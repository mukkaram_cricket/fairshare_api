<?php

namespace App\Models;

use Illuminate\Support\Carbon;

trait CommonModelFunctions {

    use \Awobaz\Compoships\Compoships;  // for multi-columns relationship in eloquent models // included in all Model classes : used in Matches Module

    protected $commonModelCast = [
        'last_active' => 'datetime',
        'date_created' => 'datetime',
        'date_updated' => 'datetime',
    ];

    public function scopeActive($query, $active = 'active') {
        return $query->where(['status' => $active]);
    }

    public function scopeOrderUserNearToMeFirst($query) {
        $loginUser = request()->loginUser;
        return $query
                        ->orderByRaw('FIELD(`country_id`, ?) DESC', [$loginUser->country_id])
                        ->orderByRaw('FIELD(`sport_id`, ?) DESC', [$loginUser->sport_id])
                        ->orderByRaw('FIELD(`role_id`, ?) DESC', [$loginUser->role_id])
        ;
    }

    public function scopeOrderByFrndAndFoF($query, $field = 'id', $only = 'both') {
        $loginUser = request()->loginUser;
        $friends = $loginUser->friends->where('is_confirmed', 1)->pluck('friend_id')->toArray();
        $fof_array = Friendship::whereIn('user_id', $friends)->where('friend_id', '!=', $loginUser->id)->where('is_confirmed', 1)->pluck('friend_id')->toArray();
        if (count($friends) > 0 && ($only == 'F' || $only == 'both')) {
            array_push($friends, $loginUser->id);
            $friends_csv = implode(',', $friends);
            $query->orderByRaw("FIELD($field , $friends_csv) DESC");
        }
        if (count($fof_array) > 0 && ($only == 'FOF' || $only == 'both')) {
            array_push($fof_array, $loginUser->id);
            $fof = implode(',', $fof_array);
            $query->orderByRaw("FIELD($field , $fof) DESC");
        }
        return $query;
    }

    public function scopeOrderByBestMatch($query, $criteria, $column) {
        return $query->orderByRaw("LOCATE('{$criteria}', $column)");
    }

    public function scopeBasedOnCriteria($query, $column = 'name', $requestParam = 'criteria') {
        $criteria = request()->{$requestParam};
        if ($criteria) {
            $query->where($column, 'Like', "%$criteria%")->orderByBestMatch($criteria, $column);
        }
        return $query;
    }

    public function scopeMyScopedFeeds($feeds) {    // Feeds that i should see
        $request = request();
        $friends = Friendship::where(['user_id' => $request->loginUserId, 'is_confirmed' => 1])->pluck('friend_id')->toArray();
        $followedClubs = ClubFollower::where('user_id', $request->loginUserId)->pluck('club_id')->toArray();
        return $feeds->where(function($feeds) use ($friends, $followedClubs, $request) {
                    $feeds->where(function($query)use ($friends, $followedClubs, $request) {
                        $query->where(function($query)use ($friends, $followedClubs, $request) {
                            $query->where(function($query)use ($friends, $followedClubs, $request) {
                                $query->where(function($query)use ($friends, $followedClubs, $request) {
                                            $query->whereIn('user_id', $friends)->whereNull('club_id')->whereIn('privacy', ['friends', 'public']);
                                        })
                                        ->orWhere(function($query)use ($friends, $followedClubs, $request) {
                                            $query->whereIn('club_id', $followedClubs)->whereIn('privacy', ['public']);
                                        })
                                        ->orWhere(function($query)use ($request) {
                                            $query->whereHas('specific_connections', function($query) use ($request) {
                                                $query->where('source_id', $request->loginUser->id);
                                            })->whereIn('privacy', ['specific']);
                                        })
                                        ->orWhere(function($query)use ($request) {
                                            $isClubOfficial = ClubOfficial::whereUserId($request->loginUser->id)->whereRaw("(status = 'Accepted' OR status = 'Author')")->exists();
                                            return $isClubOfficial ? $query->where('privacy', 'clubs') : $query;
                                        });
                            });
                        })->orWhere(function($query)use ($request) {
                            $query->whereUserId($request->loginUser->id);
                        });
                    });
                });
    }

    public function scopeMyViewAbleFeeds($feeds) {    // Feeds that i can see
        return $feeds->where(function($feeds) {
                    $feeds->where(function($feeds) {
                        $feeds = $feeds->myScopedFeeds();
                    })->orWhere(function($feeds) {
                        $feeds = $feeds->wherePrivacy('public');
                    });
                });
    }

    public function scopeOnline($query) {
        return $query->where('date_updated', '>=', now(null, 'utc')->subMinute(config('app.USER_ONLINE_INTERVAL', 10)));
    }

// overiding method... Laravel Bug Fixed
    public static function cacheMutatedAttributes($class) {
        static::$mutatorCache[$class] = collect(static::getMutatorMethods($class))->map(function ($match) {
//                    return lcfirst(static::$snakeAttributes ? \Illuminate\Support\Str::snake($match) : $match); //Orignal Line
                    return lcfirst(snake_case($match)); // here i am saying that our database has only snak_case columns name .DOT
                })->all();
    }

//------------------Adding My Custom Casting by Merging BaseModel ($casts) and Model ($myCasts) Attributes ------//
    public function __construct(array $attributes = array()) {
        if ($this->myCasts) {
            $this->casts = $this->myCasts + $this->casts;
        }
        $this->casts = $this->casts + $this->commonModelCast;

        $this->guarded += [
            $this->primaryKey, //$this->CREATED_AT,$this->UPDATED_AT,$this->DELETED_AT
            'deleted_at',
            'date_created',
            'date_updated'
        ];
        $this->hidden += ['deleted_at'];
        parent::__construct($attributes);
    }

// overiding method... handling Null to empty values in response
    protected function castAttribute($key, $value) {
//        if (is_null($value)) {
//            return $value;
//        }
        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int) $value;
            case 'real':
            case 'float':
            case 'double':
                return (float) $value;
            case 'string':
                return (string) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                return new BaseCollection($this->fromJson($value));
            case 'date':
                return $this->asDate($value);
            case 'datetime':
                return $value ? Carbon::parse($this->asDateTime($value), 'UTC')->setTimezone(config('app.timezone')) : $value;
            case 'timestamp':
                return $this->asTimestamp($value);
            default:
                return (is_null($value)) ? '' : $value;
        }
    }

//overriding: updated_at and created_at timestamps will be saved in UTC timezone only, only for updated_at and created_at
    public function freshTimestamp() {
        return new \Carbon\Carbon(null, 'UTC');
    }

}
