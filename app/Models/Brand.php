<?php

namespace App\Models;

class Brand extends \App\Models\Base\Brand
{
	protected $fillable = [
		'name',
		'slug',
		'avatar',
		'status',
		'created_at',
		'updated_at'
	];
}
