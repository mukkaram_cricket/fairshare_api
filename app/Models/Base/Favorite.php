<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Favorite
 * 
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Product $product
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class Favorite extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'product_id' => 'int'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
