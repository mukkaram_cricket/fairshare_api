<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Brand
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $avatar
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models\Base
 */
class Brand extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'status' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class);
	}
}
