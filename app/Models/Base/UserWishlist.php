<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserWishlist
 * 
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property \Carbon\Carbon $date
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $userInvitationHistories
 * @property \Illuminate\Database\Eloquent\Collection $userWishlistItems
 *
 * @package App\Models\Base
 */
class UserWishlist extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'status' => 'bool'
	];

	protected $dates = [
		'date',
		'created_at',
		'updated_at'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function userInvitationHistories()
	{
		return $this->hasMany(\App\Models\UserInvitationHistory::class);
	}

	public function userWishlistItems()
	{
		return $this->hasMany(\App\Models\UserWishlistItem::class);
	}
}
