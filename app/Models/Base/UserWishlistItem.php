<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserWishlistItem
 * 
 * @property int $id
 * @property int $user_wishlist_id
 * @property int $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Product $product
 * @property \App\Models\UserWishlist $userWishlist
 * @property \Illuminate\Database\Eloquent\Collection $userWishlistItemDetails
 *
 * @package App\Models\Base
 */
class UserWishlistItem extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_wishlist_id' => 'int',
		'product_id' => 'int'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}

	public function userWishlist()
	{
		return $this->belongsTo(\App\Models\UserWishlist::class);
	}

	public function userWishlistItemDetails()
	{
		return $this->hasMany(\App\Models\UserWishlistItemDetail::class);
	}
}
