<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $slug
 * @property string $avatar
 * @property string $password
 * @property string $activation_key
 * @property bool $role_id
 * @property int $country_id
 * @property bool $is_email_verified
 * @property bool $is_phone_verified
 * @property string $locale
 * @property \Carbon\Carbon $register_date
 * @property string $status
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_updated
 * @property string $user_token
 * @property string $address
 * @property string $zip_code
 * 
 * @property \App\Models\Country $country
 * @property \App\Models\EmailNotification $emailNotification
 * @property \Illuminate\Database\Eloquent\Collection $favorites
 * @property \Illuminate\Database\Eloquent\Collection $messageRecipients
 * @property \Illuminate\Database\Eloquent\Collection $messageThreads
 * @property \Illuminate\Database\Eloquent\Collection $messages
 * @property \Illuminate\Database\Eloquent\Collection $notifications
 * @property \Illuminate\Database\Eloquent\Collection $userApis
 * @property \Illuminate\Database\Eloquent\Collection $userInvitationHistories
 * @property \Illuminate\Database\Eloquent\Collection $userWishlistItemDetails
 * @property \Illuminate\Database\Eloquent\Collection $userWishlists
 *
 * @package App\Models\Base
 */
class User extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	public static $snakeAttributes = false;

	protected $casts = [
		'role_id' => 'bool',
		'country_id' => 'int',
		'is_email_verified' => 'bool',
		'is_phone_verified' => 'bool'
	];

	protected $dates = [
		'register_date'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function emailNotification()
	{
		return $this->hasOne(\App\Models\EmailNotification::class);
	}

	public function favorites()
	{
		return $this->hasMany(\App\Models\Favorite::class);
	}

	public function messageRecipients()
	{
		return $this->hasMany(\App\Models\MessageRecipient::class);
	}

	public function messageThreads()
	{
		return $this->hasMany(\App\Models\MessageThread::class);
	}

	public function messages()
	{
		return $this->hasMany(\App\Models\Message::class, 'sender_id');
	}

	public function notifications()
	{
		return $this->hasMany(\App\Models\Notification::class);
	}

	public function userApis()
	{
		return $this->hasMany(\App\Models\UserApi::class);
	}

	public function userInvitationHistories()
	{
		return $this->hasMany(\App\Models\UserInvitationHistory::class);
	}

	public function userWishlistItemDetails()
	{
		return $this->hasMany(\App\Models\UserWishlistItemDetail::class);
	}

	public function userWishlists()
	{
		return $this->hasMany(\App\Models\UserWishlist::class);
	}
}
