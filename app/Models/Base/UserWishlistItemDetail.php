<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserWishlistItemDetail
 * 
 * @property int $id
 * @property int $user_wishlist_item_id
 * @property int $user_id
 * @property int $share_member
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserWishlistItem $userWishlistItem
 *
 * @package App\Models\Base
 */
class UserWishlistItemDetail extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_wishlist_item_id' => 'int',
		'user_id' => 'int',
		'share_member' => 'int',
		'status' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function userWishlistItem()
	{
		return $this->belongsTo(\App\Models\UserWishlistItem::class);
	}
}
