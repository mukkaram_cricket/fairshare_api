<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property string $name
 * @property int $country_id
 * @property string $name_pt_PT
 * @property string $name_da_DK
 * @property string $name_du_NL
 * @property string $name_fr_FR
 * @property string $name_it_IT
 * @property string $name_ro_RO
 * @property string $name_tr_TR
 * @property string $name_ar_SA
 * @property string $name_zh_CN
 * @property string $name_ur_PK
 * 
 * @property \App\Models\Country $country
 *
 * @package App\Models\Base
 */
class City extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'country_id' => 'int'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}
}
