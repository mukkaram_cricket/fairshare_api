<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserApi
 * 
 * @property int $id
 * @property int $user_id
 * @property string $login_type
 * @property string $token
 * @property string $fcm_token
 * @property string $user_agent
 * @property string $timezone
 * @property bool $status
 * @property \Carbon\Carbon $last_used
 * @property \Carbon\Carbon $date_created
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class UserApi extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'status' => 'bool'
	];

	protected $dates = [
		'last_used'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
