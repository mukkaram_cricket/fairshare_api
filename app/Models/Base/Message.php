<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Message
 * 
 * @property int $id
 * @property int $thread_id
 * @property int $sender_id
 * @property string $message
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_updated
 * 
 * @property \App\Models\MessageThread $messageThread
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class Message extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	public static $snakeAttributes = false;

	protected $casts = [
		'thread_id' => 'int',
		'sender_id' => 'int'
	];

	public function messageThread()
	{
		return $this->belongsTo(\App\Models\MessageThread::class, 'thread_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'sender_id');
	}
}
