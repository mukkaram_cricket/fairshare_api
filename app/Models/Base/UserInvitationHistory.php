<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserInvitationHistory
 * 
 * @property int $id
 * @property int $user_id
 * @property int $reference_id
 * @property int $user_wishlist_id
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\UserWishlist $userWishlist
 *
 * @package App\Models\Base
 */
class UserInvitationHistory extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'reference_id' => 'int',
		'user_wishlist_id' => 'int'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function userWishlist()
	{
		return $this->belongsTo(\App\Models\UserWishlist::class);
	}
}
