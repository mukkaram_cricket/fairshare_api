<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MessageRecipient
 * 
 * @property int $thread_id
 * @property int $user_id
 * @property bool $is_view
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_updated
 * 
 * @property \App\Models\MessageThread $messageThread
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class MessageRecipient extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	public $incrementing = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'thread_id' => 'int',
		'user_id' => 'int',
		'is_view' => 'bool'
	];

	public function messageThread()
	{
		return $this->belongsTo(\App\Models\MessageThread::class, 'thread_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
