<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $id
 * @property int $brand_id
 * @property int $category_id
 * @property int $product_image_id
 * @property string $name
 * @property string $description
 * @property float $purchase_price
 * @property float $discount
 * @property float $actual_price
 * @property string $slug
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Brand $brand
 * @property \App\Models\Category $category
 * @property \App\Models\ProductImage $productImage
 * @property \Illuminate\Database\Eloquent\Collection $favorites
 * @property \Illuminate\Database\Eloquent\Collection $productImages
 * @property \Illuminate\Database\Eloquent\Collection $userWishlistItems
 *
 * @package App\Models\Base
 */
class Product extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'brand_id' => 'int',
		'category_id' => 'int',
		'product_image_id' => 'int',
		'purchase_price' => 'float',
		'discount' => 'float',
		'actual_price' => 'float',
		'status' => 'bool'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function brand()
	{
		return $this->belongsTo(\App\Models\Brand::class);
	}

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function productImage()
	{
		return $this->belongsTo(\App\Models\ProductImage::class);
	}

	public function favorites()
	{
		return $this->hasMany(\App\Models\Favorite::class);
	}

	public function productImages()
	{
		return $this->hasMany(\App\Models\ProductImage::class);
	}

	public function userWishlistItems()
	{
		return $this->hasMany(\App\Models\UserWishlistItem::class);
	}
}
