<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Multiselect
 * 
 * @property int $entity_id
 * @property int $source_id
 * @property string $type
 *
 * @package App\Models\Base
 */
class Multiselect extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	protected $table = 'multiselect';
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'entity_id' => 'int',
		'source_id' => 'int'
	];
}
