<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MessageThread
 * 
 * @property int $id
 * @property string $type
 * @property int $user_id
 * @property string $subject
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_updated
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $messageRecipients
 * @property \Illuminate\Database\Eloquent\Collection $messages
 *
 * @package App\Models\Base
 */
class MessageThread extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function messageRecipients()
	{
		return $this->hasMany(\App\Models\MessageRecipient::class, 'thread_id');
	}

	public function messages()
	{
		return $this->hasMany(\App\Models\Message::class, 'thread_id');
	}
}
