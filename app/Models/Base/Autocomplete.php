<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Autocomplete
 * 
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $type
 *
 * @package App\Models\Base
 */
class Autocomplete extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	protected $table = 'autocomplete';
	public $timestamps = false;
	public static $snakeAttributes = false;
}
