<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmailNotification
 * 
 * @property int $user_id
 * @property bool $activity_mention
 * @property bool $feed_comment
 * @property bool $comment_mention
 * @property bool $comment_reply
 * @property bool $new_message
 * @property bool $message_reply
 * @property bool $friend_req_recieved
 * @property bool $friend_req_accepted
 * @property bool $job_invitation
 * @property bool $match_request
 * @property bool $match_request_accepted
 * @property bool $match_request_rejected
 * @property bool $player_invite_match
 * @property bool $match_joining_accepted
 * @property bool $match_joining_rejected
 * @property bool $match_result_added
 * @property bool $player_result_added
 * @property bool $official_req_rejected
 * @property bool $official_req_accepted
 * @property bool $player_invite_team
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class EmailNotification extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	public $incrementing = false;
	public $timestamps = false;
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'activity_mention' => 'bool',
		'feed_comment' => 'bool',
		'comment_mention' => 'bool',
		'comment_reply' => 'bool',
		'new_message' => 'bool',
		'message_reply' => 'bool',
		'friend_req_recieved' => 'bool',
		'friend_req_accepted' => 'bool',
		'job_invitation' => 'bool',
		'match_request' => 'bool',
		'match_request_accepted' => 'bool',
		'match_request_rejected' => 'bool',
		'player_invite_match' => 'bool',
		'match_joining_accepted' => 'bool',
		'match_joining_rejected' => 'bool',
		'match_result_added' => 'bool',
		'player_result_added' => 'bool',
		'official_req_rejected' => 'bool',
		'official_req_accepted' => 'bool',
		'player_invite_team' => 'bool'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
