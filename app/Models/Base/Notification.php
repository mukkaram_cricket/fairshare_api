<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 09:58:01 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Notification
 * 
 * @property int $id
 * @property int $user_id
 * @property int $sender_id
 * @property string $title
 * @property string $description
 * @property string $module
 * @property string $type
 * @property int $related_id
 * @property string $extra
 * @property bool $is_viewed
 * @property bool $is_read
 * @property \Carbon\Carbon $date_created
 * @property \Carbon\Carbon $date_updated
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models\Base
 */
class Notification extends Eloquent
{
	use \App\Models\CommonModelFunctions;
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';
	public static $snakeAttributes = false;

	protected $casts = [
		'user_id' => 'int',
		'sender_id' => 'int',
		'related_id' => 'int',
		'is_viewed' => 'bool',
		'is_read' => 'bool'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
