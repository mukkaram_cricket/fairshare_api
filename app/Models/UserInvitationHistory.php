<?php

namespace App\Models;

class UserInvitationHistory extends \App\Models\Base\UserInvitationHistory
{
	protected $fillable = [
		'user_id',
		'reference_id',
		'user_wishlist_id',
		'type',
		'created_at',
		'updated_at'
	];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class,'user_id');
    }

    public function references()
    {
        return $this->belongsTo(\App\Models\User::class,'reference_id');
    }

    public function userWishlist()
    {
        return $this->belongsTo(\App\Models\UserWishlist::class);
    }

}
