<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class City extends \App\Models\Base\City {

    protected static function boot() {
        parent::boot();

        static::addGlobalScope('cityTranslation', function (Builder $builder) {
            $locale = config('app.locale');
            if ($locale != 'en_US') {
                $builder->addSelect("id", "country_id", "name_{$locale} as label", "name_{$locale} as name");
            }
//            logger($builder->getQuery()->columns);
        });
    }

}
