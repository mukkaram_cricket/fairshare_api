<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

     'accepted' => 'De :attribute moet worden geaccepteerd.',
    'active_url' => ':attribute is geen geldig URL.',
    'after' => 'De :attribute moet een datum erna zijn :date.',
    'after_or_equal' => 'De :attribute moet een datum na of gelijk aan zijn :date.',
    'alpha' => 'De :attribute mag alleen letters bevatten.',
    'alpha_dash' => 'De :attribute mag alleen letters, cijfers en streepjes bevatten.',
    'alpha_num' => 'De :attribute mag alleen letters en cijfers bevatten.',
    'array' => 'De :attribute moet een reeks zijn.',
    'before' => 'De :attribute moet een datum voor :date zijn.',
    'before_or_equal' => 'De :attribute moet een datum vóór of gelijk aan :date zijn .',
    'between' => [
        'numeric' => 'De :attribute moet tussen :min en :max zijn.',
        'file' => 'De :attribute moet tussen :min en :max kilobytes zijn.',
        'string' => 'De :attribute moet tussen :min en :max tekens bevatten.',
        'array' => 'De :attribute moet hebben tussen :min en :max items bevatten.',
    ],
    'boolean' => 'Het :attribute veld moet waar zijn voor onjuist.',
    'confirmed' => 'De :attribute bevestiging komt niet overeen.',
    'date' => 'De :attribute is geen geldige datum.',
    'date_format' => 'De :attribute komt niet overeen met het formaat :format.',
    'different' => 'De :attribute en :other moet anders zijn.',
    'digits' => 'De :attribute moet zijn :digits digits.',
    'digits_between' => 'De :attribute moet tussen :min and :max cijfers zijn.',
    'dimensions' => 'De :attribute heeft ongeldige afbeeldingsafmetingen.',
    'distinct' => 'Het :attribute veld heeft een dubbele waarde.',
    'email' => 'De :attribute moet een geldig e-mail adres zijn.',
    'exists' => 'De gekozen :attribute is ongeldig.',
    'file' => 'De :attribute moet een bestand zijn.',
    'filled' => 'Het :attribute veld moet een waarde hebben.',
    'image' => 'De :attribute moet een afbeelding zijn.',
    'in' => 'De gekozen :attribute is ongeldig.',
    'in_array' => 'Het :attribute veld bestaat niet in :other.',
    'integer' => ':attribute moet een geheel getal zijn.',
    'ip' => ':attribute moet een geldig IP-adres zijn.',
    'ipv4' => ':attribute moet een geldig IPV4-adres zijn.',
    'ipv6' => 'attribute moet een geldig IPV6-adres zijn.',
    'json' => 'attribute moet een geldige JSON-reeks zijn.',
    'max' => [
        'numeric' => ':attribute mag niet groter zijn dan :max.',
        'file' => ':attribute mag niet groter zijn dan :max kilobytes.',
        'string' => ':attribute mag niet groter zijn dan :max tekens.',
        'array' => ':attribute mag niet meer hebben dan :max items.',
    ],
    'mimes' => 'De :attribute moet een bestand van het type zijn: :values.',
    'mimetypes' => 'De :attribute moet een bestand van het type zijn: :values.',
    'min' => [
        'numeric' => 'De :attribute moet minstens zijn :min.',
        'file' => 'De :attribute moet minstens zijn :min kilobytes.',
        'string' => 'De :attribute moet minstens zijn :min tekens.',
        'array' => 'De :attribute moet op zijn minst hebben :min items.',
    ],
    'not_in' => 'De gekozen :attribute is ongeldig.',
    'numeric' => 'De :attribute moet een nummer zijn.',
    'present' => 'Het :attribute veld moet aanwezig zijn.',
    'regex' => 'De :attribute formaat is ongeldig.',
    'required' => 'Het :attribute veld is verplicht.',
    'required_if' => 'Het :attribute veld is vereist wanneer :other is :value.',
    'required_unless' => 'Het :attribute veld is verplicht tenzij :other is in :values.',
    'required_with' => 'Het :attribute veld is vereist wanneer :values aanwezig is.',
    'required_with_all' => 'Het :attribute veld is vereist wanneer :values aanwezig is.',
    'required_without' => 'Het :attribute veld is vereist wanneer :values is er niet.',
    'required_without_all' => 'Het :attribute veld is verplicht wanneer geen van :values zijn aanwezig.',
    'same' => 'De :attribute en :other moet overeenkomen.',
    'size' => [
        'numeric' => ':attribute moet :size zijn.',
        'file' => ':attribute moet :size kilobytes zijn.',
        'string' => ':attribute moet :size tekens zijn.',
        'array' => ':attribute moet :size items bevatten .',
    ],
    'string' => ':attribute moet een tekenreeks zijn.',
    'timezone' => ':attribute moet een geldige tijdszone zijn.',
    'unique' => ':attribute is al bezet.',
    'uploaded' => 'De :attribute kan niet worden geupload.',
    'url' => 'Het :attribute formaat is ongeldig.',
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => 'Het wachtwoord moet alfanumeriek zijn.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'land',
        'club_id' => 'Club',
        'feed_id' => 'Feed',
        'city_id' => 'stad',
        'password' => 'Wachtwoord',
    ],
];
