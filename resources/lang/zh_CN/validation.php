<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => " 此 :attribute 必须被接纳。",
    'active_url' => " 此 :attribute不是有效的URL。",
    'after' => " 此 :attribute 必须是 :date 之后的一个日期。",
    'after_or_equal' => " 此 :attribute必须是 :date 之后或者同一天的日期。",
    'alpha' => " 此 :attribute 必须只含有多个字母。",
    'alpha_dash' => " 此 :attribute必须只含有多个字母、 多个号码和多个连接号。",
    'alpha_num' => " 此 :attribute必须只含有多个字母和多个号码。",
    'array' => " 此 :attribute必须是个排列矩阵。",
    'before' => " 此 :attribute 必须是 :date 之前的一个日期。",
    'before_or_equal' => " 此 :attribute必须是 :date 之前或者同一天的日期。",
    'between' => [
        'numeric' => " 此 :attribute必须是在 :min 和 :max 之间。",
        'file' => " 此 :attribute必须有从 :min 千字节到 :max 千字节之间。",
        'string' => " 此 :attribute必须有 :min 个字符到 :max 个字符之间。",
        'array' => " 此 :attribute必须有从 :min 个事项到 :max 个事项之间。",
    ],
    'boolean' => " 此 :attribute的字段必须为对或者错。",
    'confirmed' => " 此 :attribute的确认不匹配。",
    'date' => " 此 :attribute不是一个有效的日期。",
    'date_format' => " 此 :attribute与 :format 的格式不匹配。",
    'different' => " 此 :attribute与 :other 必须不一样。",
    'digits' => " 此 :attribute必须有 :digits 个号码。",
    'digits_between' => " 此 :attribute必须有从 :min 个号码到 :max 个号码之间。",
    'dimensions' => " 此 :attribute具有无效的图像尺寸。",
    'distinct' => " 此 :attribute字段有一个重复值。",
    'email' => " 此 :attribute必须是一个有效的电邮址。",
    'exists' => " 此所选 :attribute 无效。",
    'file' => " 此 :attribute 必须是个档案。",
    'filled' => " 此 :attribute 的字段必须含有一个值。",
    'image' => " 此 :attribute 必须是一个图片。",
    'in' => " 此所选 :attribute无效。",
    'in_array' => " 此 :attribute的字段在 :other 的里面是不存在的。",
    'integer' => " 此 :attribute 必须是一个整数。",
    'ip' => " 此 :attribute 必须是一个有效的IP 地址。",
    'ipv4' => " 此 :attribute 必须是一个有效的IPv4 地址。",
    'ipv6' => " 此 :attribute 必须是一个有效的IPv6 地址。",
    'json' => " 此 :attribute 必须是一个有效的JSON 字符串。",
    'max' => [
        'numeric' => " 此 :attribute 不可比 :max 更大。",
        'file' => " 此 :attribute 不可比 :max 千字节更大。",
        'string' => " 此 :attribute 不可比 :max 个字符更大。",
        'array' => " 此 :attribute 不可比 :max 个事项更大。",
    ],
    'mimes' => " 此 :attribute 必须是一个 : :values 的档案格式。",
    'mimetypes' => " 此 :attribute 必须是一个 : :values 的档案格式。",
    'min' => [
        'numeric' => " 此 :attribute 必须至少是 :min 或更大。",
        'file' => " 此 :attribute 必须至少是 :min 千字节或更多。",
        'string' => " 此 :attribute 必须至少是 :min 个字符或更多。",
        'array' => " 此 :attribute 必须至少是 :min 个事项或更多。",
    ],
    'not_in' => " 此所选 :attribute 无效。",
    'numeric' => " 此 :attribute 必须是一个号码。",
    'present' => " 此 :attribute 的字段必须存在。",
    'regex' => " 此 :attribute 的格式无效。",
    'required' => " 此 :attribute 的字段是必需的。",
    'required_if' => " 当 :other是 :value 时； 此 :attribute 的字段是必需的。",
    'required_unless' => " 除非当 :other是在 :values 的里面； 此 :attribute 的字段就是必需的。",
    'required_with' => " 当 :values 存在时； 此 :attribute 的字段是必需的。",
    'required_with_all' => " 当 :values 存在时； 此 :attribute 的字段是必需的。",
    'required_without' => " 当 :values 不存在时； 此 :attribute 的字段是必需的。",
    'required_without_all' => " 当连一个 :values 都不存在时； 此 :attribute 的字段是必需的。",
    'same' => " 此 :attribute 与 :other 必须匹配。",
    'size' => [
        'numeric' => " 此 :attribute 必须是 :size。",
        'file' => " 此 :attribute 必须是 :size 千字节。",
        'string' => " 此 :attribute 必须是 :size 个字符。",
        'array' => " 此 :attribute 必须含有 :size 个事项。",
    ],
    'string' => " 此 :attribute 必须是一个字符串。",
    'timezone' => " 此 :attribute 必须是一个有效的区域。",
    'unique' => " 此 :attribute 已被采用。",
    'uploaded' => " 此 :attribute 无法上传。",
    'url' => " 此 :attribute 的格式无效。",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => " 此密码必须是字母数字。",
        ],
        'new_password' => [
            'regex' => " 此密码必须是字母数字。",
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => '国家',
        'club_id' => '社团',
        'feed_id' => '哺育',
        'city_id' => '城市',
        'password' => '密码',
        'hashed_email' => '电邮',
        'hashed_phone' => '电话',
    ],
];
