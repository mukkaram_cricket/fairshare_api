<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => "يجب قبول :attribute",
    'active_url' => ":attribute رابط غير صالح. ",
    'after' => ":attribute يجب أن يكون تاريخاً بعد :date. ",
    'after_or_equal' => ":attribute يجب أن يكون تاريخاً بعد أو متساوٍ لـ :date. ",
    'alpha' => "قد يحتوي :attribute على حروف فقط. ",
    'alpha_dash' => "قد يحتوي :attribute على حروف، وأرقام، وشرطات. ",
    'alpha_num' => "قد يحتوي :attribute على حروف وأرقام فقط. ",
    'array' => "يجب أن يكون :attribute تسلسل منظم. ",
    'before' => "يجب أن يكون :attribute تاريخ قبل :date. ",
    'before_or_equal' => "يجب أن يكون :attribute تاريخ قبل أو مساوٍ لـ :date. ",
    'between' => [
        'numeric' => "يجب أن يكون :attribute بين :min و :max. ",
        'file' => "يجب أن يكون :attribute بين :min و :max كيلوبايت. ",
        'string' => "يجب أن يكون :attribute بين :min و :max رمز. ",
        'array' => "يجب أن يكون :attribute بين :min و :max عنصر. ",
    ],
    'boolean' => "يجب أن يكون حقل :attribute صحيحاً أو خاطئاً. ",
    'confirmed' => "تأكيد :attribute لا يتطابق. ",
    'date' => ":attribute تاريخ غير صحيح. ",
    'date_format' => ":attribute لا يطابق الصيغة :format. ",
    'different' => "يجب أن يكون :attribute و :other مختلفان. ",
    'digits' => "يجب أن يكون :attribute :digits رقم. ",
    'digits_between' => "يجب أن يكون :attribute بين :min و :max رقم. ",
    'dimensions' => "يحتوي :attribute على أبعاد صورة غير صحيحة. ",
    'distinct' => "يحتوي حقل :attribute على قيمة مكررة. ",
    'email' => "يجب أن يكون :attribute عنوان بريد الكتروني صحيح. ",
    'exists' => ":attribute المحدد غير صحيح. ",
    'file' => "يجب أن يكون :attribute ملف. ",
    'filled' => "يجب أن يكون حقل :attribute قيمة. ",
    'image' => "يجب أن يكون :attribute صورة. ",
    'in' => ":attribute المحدد غير صحيح. ",
    'in_array' => "حقل :attribute غير موجود في :other. ",
    'integer' => "يجب أن يكون :attribute عدد صحيح. ",
    'ip' => "يجب أن يكون :attribute عنوان أي بي صحيح. ",
    'ipv4' => "يجب أن يكون :attribute عنوان IPv4 صحيح. ",
    'ipv6' => "يجب أن يكون :attribute عنوان IPv6 صحيح. ",
    'json' => "يجب أن يكون :attribute سلسة JSON صحيحة. ",
    'max' => [
        'numeric' => "لا يجب أن تكون :attribute أكبر من :max. ",
        'file' => "لا يجب أن تكون :attribute أكبر من :max كيلوبايت. ",
        'string' => "لا يجب أن تكون :attribute أكبر من :max رمز. ",
        'array' => "لا يجب أن يحتوي :attribute على أكثر من :max عنصر. ",
    ],
    'mimes' => "يجب أن تكون :attribute ملف من نوع type: :values. ",
    'mimetypes' => "يجب أن تكون :attribute ملف من نوع type: :values.",
    'min' => [
        'numeric' => "يجب أن تكون :attribute :min على الأقل. ",
        'file' => "يجب أن تكون :attribute :min كيلوبايت على الأقل. ",
        'string' => "يجب أن تكون :attribute :min رمز على الأقل. ",
        'array' => "يجب أن تحتوي :attribute على :min عنصر على الأقل. ",
    ],
    'not_in' => ":attribute المحدد غير صحيح. ",
    'numeric' => "يجب أن تكون :attribute رقم. ",
    'present' => "يجب أن يكون حقل :attribute حالي. ",
    'regex' => "صيغة :attribute غير صحيحة. ",
    'required' => "حقل :attribute مطلوب. ",
    'required_if' => "حقل :attribute مطلوب عندما :other تكون :value. ",
    'required_unless' => "حقل :attribute مطلوب إلا إذا :other كان في :values. ",
    'required_with' => "حقل :attribute مطلوب عندما :values تكون حالية. ",
    'required_with_all' => "حقل :attribute مطلوب عندما :values تكون حالية. ",
    'required_without' => "حقل :attribute مطلوب عندما :values تكون غير حالية. ",
    'required_without_all' => "حقل :attribute مطلوب عندما لا تكون أي من :values حالية. ",
    'same' => "يجب أن يتطابق كلا من :attribute و :other. ",
    'size' => [
        'numeric' => "يجب أن تكون :attribute :size. ",
        'file' => "يجب أن تكون :attribute :size كيلوبايت. ",
        'string' => "يجب أن تكون :attribute :size رمز. ",
        'array' => "يجب أن تحتوي :attribute على :size عنصر.",
    ],
    'string' => "يجب أن تكون :attribute سلسلة. ",
    'timezone' => "يجب أن تكون :attribute منطقة صحيحة. ",
    'unique' => "لقد تم استخدام :attribute من قبل. ",
    'uploaded' => "فشلت عملية رفع :attribute. ",
    'url' => "صيغة :attribute غير صالحة. ",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "يجب أن تكون كلمة المرور أبجدية.",
        ],
        'new_password' => [
            'regex' => "يجب أن تكون كلمة المرور أبجدية.",
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'الدولة',
        'club_id' => 'النادي',
        'feed_id' => 'الأخبار',
        'city_id' => 'المدينة',
        'password' => 'كلمة المرور',
        'hashed_email' => 'البريد الالكتروني',
        'hashed_phone' => 'الهاتف',
    ],
];
