<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => "O :attribute tem de ser aceite.",
    'active_url' => "O :attribute não é um URL válido.",
    'after' => "O :attribute tem de ser uma data depois de :date.",
    'after_or_equal' => "O :attribute deverá ser uma data igual ou maior que :date.",
    'alpha' => "O :attribute só pode conter letras.",
    'alpha_dash' => "O :attribute só pode conter letras, números e traços.",
    'alpha_num' => "O :attribute só pode conter letras e números.",
    'array' => "O :attribute deverá ser uma matriz.",
    'before' => "O :attribute tem de ser uma data antes de :date.",
    'before_or_equal' => "O :attribute deverá ser uma data anterior ou igual a :date.",
    'between' => [
        'numeric' => "O :attribute deevrá ser entre :min e :max.",
        'file' => "O :attribute tem de ser entre :min e :max kilobytes.",
        'string' => "O :attribute tem de ser entre :min e :max caracteres.",
        'array' => "O :attribute tem de ter entre :min e :max items.",
    ],
    'boolean' => "O campo do :attribute tem de ser verdadeiro ou falso.",
    'confirmed' => "A confirmação do :attribute não coincide.",
    'date' => "O :attribute não é uma data válida.",
    'date_format' => "O :attribute não corresponde ao formato :format.",
    'different' => "O :attribute e :other têm de ser diferentes.",
    'digits' => "O :attribute deverá ser :digits dígitos.",
    'digits_between' => "O :attribute tem de conter entre :min e :max dígitos.",
    'dimensions' => "O :attribute tem dimensões de imagem inválidas.",
    'distinct' => "O campo do :attribute tem um valor duplicado.",
    'email' => "O :attribute tem de ser um endereço de email válido.",
    'exists' => "O :attribute selecionado é inválido.",
    'file' => "O :attribute tem de ser um ficheiro.",
    'filled' => "O campo do :attribute tem de ser um valor.",
    'image' => "O :attribute deverá ser uma imagem.",
    'in' => "O :attribute selecionado é inválido.",
    'in_array' => "O campo do :attribute não existe em :other.",
    'integer' => "O :attribute tem de ser um número inteiro.",
    'ip' => "O :attribute tem de ser um endereço de IP válido.",
    'ipv4' => "O :attribute tem de ser um endereço de IPv4 válido.",
    'ipv6' => "O :attribute tem de ser um endereço de IPv6 válido.",
    'json' => "O :attribute tem de ser uma JSON string válida.",
    'max' => [
        'numeric' => "O :attribute não pode ser maior que :max.",
        'file' => "O :attribute não deve ser maior que :max kilobytes.",
        'string' => "O :attribute não deve ter mais que :max caracteres.",
        'array' => "O :attribute não deve ter mais que :max items.",
    ],
    'mimes' => "O :attribute deverá ser um ficheiro do tipo: :values.",
    'mimetypes' => "O :attribute deverá ser um ficheiro do tipo: :values.",
    'min' => [
        'numeric' => "O :attribute tem de ser no mínimo :min.",
        'file' => "O :attribute deve ter no mínimo :min kilobytes.",
        'string' => "O :attribute deve ter no mínimo :min caracteres.",
        'array' => "O :attribute deve ter no mínimo :min items.",
    ],
    'not_in' => "O :attribute selecionado é inválido.",
    'numeric' => "O :attribute deve ser um número.",
    'present' => "O campo do :attribute tem de estar presente.",
    'regex' => "O formato do :attribute é inválido.",
    'required' => "O campo do :attribute é necessário.",
    'required_if' => "O campo do :attribute é necessáro quando :other é :value.",
    'required_unless' => "O campo do :attribute é necessário a menos que :other seja :values.",
    'required_with' => "O campo do :attribute é necessário quando :values está presente.",
    'required_with_all' => "O campo do :attribute é necessário quando :values está presente.",
    'required_without' => "O campo do :attribute é neessário quando :values não está presente.",
    'required_without_all' => "O campo do :attribute é necessário quando nenhum dos :values está presente.",
    'same' => "O :attribute e :other devem coincidir.",
    'size' => [
        'same' => "O :attribute e :other devem coincidir.",
        'numeric' => "O :attribute deverá ser :size.",
        'file' => "O :attribute deverá ter :size kilobytes.",
        'string' => "O :attribute deverá ter :size caracteres.",
        'array' => "O :attribute deve conter :size items.",
    ],
    'string' => "O :attribute deve ser uma string.",
    'timezone' => "O :attribute deve ser uma zona válida.",
    'unique' => "O :attribute já foi escolhido.",
    'uploaded' => "O :attribute falhou ao carregar.",
    'url' => "O formato do :attribute é inválido.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "A Palavra-passe deve ser alfanumérica.",
        ],
        'new_password' => [
            'regex' => 'A Palavra-passe deve ser alfanumérica.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'País',
        'club_id' => 'Clube',
        'feed_id' => 'Feed',
        'city_id' => 'Cidade',
        'password' => 'Palavra-Passe',
    ],
];
