<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => " :attribute trebuie acceptat.",
    'active_url' => " :attribute nu este un URL valid.",
    'after' => " :attribute trebuie sã fie dupã data de :date.",
    'after_or_equal' => " :attribute trebuie sã fie o datã dupã sau egalã cu :date.",
    'alpha' => " :attribute poate con?ine doar litere.",
    'alpha_dash' => " :attribute poate con?ine litere, numere ?i cratime.",
    'alpha_num' => " :attribute poate con?ine doar litere ?i numere.",
    'array' => " :attribute trebuie sã fie o mul?ime.",
    'before' => " :attribute trebuie sã fie înainte de :date.",
    'before_or_equal' => " :attribute trebuie sã fie o datã înainte sau egalã cu :date.",
    'between' => [
        'numeric' => " :attribute trebuie sã fie între :min ?i :max.",
        'file' => " :attribute trebuie sã fie între :min ?i :max kilobytes.",
        'string' => ":attribute trebuie sã fie între :min ?i :max de caractere.",
        'array' => " :attribute trebuie sã aibã între :min ?i :max de obiecte.",
    ],
    'boolean' => "Câmpul :attribute trebuie sã fie adevãrat sau fals.",
    'confirmed' => "Confirmarea :attribute nu se potrive?te.",
    'date' => " :attribute nu este o datã validã.",
    'date_format' => " :attribute nu se potrive?te cu formatul :format.",
    'different' => " :attribute ?i :other trebuie sã fie diferite.",
    'digits' => " :attribute trebuie sã fie :digits cifre.",
    'digits_between' => " :attribute trebuie sã fie între :min ?i :max cifre.",
    'dimensions' => " :attribute are dimensiunile fotografiei invalide.",
    'distinct' => "Câmpul :attribute are o valoare dublã.",
    'email' => " :attribute trebuie sã fie o adresã de e-mail validã.",
    'exists' => " :attribute selectat este invalid.",
    'file' => " :attribute trebuie sã fie un fi?ier.",
    'filled' => " :attribute trebuie sã aibã o valoare.",
    'image' => " :attribute trebuie sã fie o imagine.",
    'in' => " :attribute selectat este invalid.",
    'in_array' => "Câmpul :attribute nu existã în :other.",
    'integer' => " :attribute trebuie sã fie un numãr întreg.",
    'ip' => " :attribute trebuie sã fie o adresã IP validã.",
    'ipv4' => " :attribute trebuie sã fie o adresã IPv4 validã.",
    'ipv6' => " :attribute trebuie sã fie o adresã IPv6 vlidã.",
    'json' => " :attribute trebuie sã fie un string JSON valid.",
    'max' => [
        'numeric' => " :attribute nu poate fi mai mare decât :max.",
        'file' => " :attribute nu poate fi mai mare decât :max kilobytes.",
        'string' => " :attribute nu poate fi mai mare decât :max caractere.",
        'array' => " :attribute nu poate avea mai multe decât :max caractere.",
    ],
    'mimes' => " :attribute trebuie sã fie un fi?ier de tipul :values.",
    'mimetypes' => " :attribute trebuie sã fie un fi?ier de tipul :values.",
    'min' => [
        'numeric' => " :attribute trebuie sã fie mãcar :min.",
        'file' => " :attribute trebuie sã fie mãcar de :min kilobytes.",
        'string' => " :attribute trebuie sã fie mãcar de :min caractere.",
        'array' => " :attribute trebuie sã fie mãcar de :min obiecte.",
    ],
    'not_in' => " :attribute selectat este invalid.",
    'numeric' => " :attribute trebuie sã fie un numãr.",
    'present' => "Câmpul :attribute trebuie sã fie prezent.",
    'regex' => "Formatul :attribute este invalid.",
    'required' => "Câmpul :attribute este necesar.",
    'required_if' => "Câmpul :attribute este necesar când :other este :value.",
    'required_unless' => "Câmpul :attribute este necesar doar dacã :other este în :values.",
    'required_with' => "Câmpul :attribute este necesar când :values este prezent.",
    'required_with_all' => "Câmpul :attribute este necesar când :values este prezent.",
    'required_without' => "Câmpul :attribute este necesar când :values nu este prezent.",
    'required_without_all' => "Câmpul :attribute este necesar când nicio :values nu este prezentã.",
    'same' => " :attribute ?i :other trebuie sã se potriveascã.",
    'size' => [
        'numeric' => " :attribute trebuie sã fie :size.",
        'file' => " :attribute trebuie sã fie :size kilobytes.",
        'string' => " :attribute trebuie sã fie :size caractere.",
        'array' => " :attribute trebuie sã con?inã :size obiecte.",
    ],
    'string' => " :attribute trebuie sã fie un string.",
    'timezone' => " :attribute trebuie sã fie o zonã validã.",
    'unique' => " :attribute a fost deja luat.",
    'uploaded' => " :attribute nu s-a încãrcat.",
    'url' => "Formatul :attribute este invalidã.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "Parola trebuie sã fie alfanumericã.",
        ],
        'new_password' => [
            'regex' => "Parola trebuie sã fie alfanumericã.",
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Țară',
        'club_id' => 'Club',
        'feed_id' => 'Feed',
        'city_id' => 'Oraș',
        'password' => 'Parolã',
        'hashed_email' => 'e-mail',
        'hashed_phone' => 'telefon',
    ],
];
