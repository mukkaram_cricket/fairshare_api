<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => "L' :attribute doit être accepté.",
    'active_url' => "L' :attribute n'est pas une URL valide.",
    'after' => "L' :attribute doit être une date aprés :date.",
    'after_or_equal' => "L' :attribute doit être une date postérieure ou égale à :date.",
    'alpha' => " L' :attribute ne peut contenir que des lettres.",
    'alpha_dash' => "L' :attribute ne peut contenir que des lettres, des chiffres et des tirets.",
    'alpha_num' => " L' :attribute ne peut contenir que des lettres et des chiffres.",
    'array' => "L' :attribute doit être un tableau.",
    'before' => "The :attribute must be a date before :date.",
    'before_or_equal' => " L' :attribute doit être une date antérieure ou égale à :date.",
    'between' => [
        'numeric' => "L' :attribute doit être entre :min et :max.",
        'file' => "L' :attribut doit être entre :min et :max Kilooctets.",
        'string' => "L' :attribute doit être entre :min et :max Caractères.",
        'array' => "L' :attribute doit avoir entre :min et :max articles",
    ],
    'boolean' => "Le champ :attribute doit être vrai ou faux.",
    'confirmed' => "La confirmation :attribute ne correspond pas",
    'date' => " L' :attribute n'est pas une date valide.",
    'date_format' => "L' :attribute ne correspond pas au format :format.",
    'different' => "L' :attribute et :other autres doivent être différents. ",
    'digits' => "L' :attribute doit être :digits chiffres.",
    'digits_between' => "L' :attribut doit être entre :min et :max Chiffres",
    'dimensions' => "L' :attribute a des dimensions de l'image non valides.",
    'distinct' => "Le champ d'attribute a une valeur en double. ",
    'email' => "L' :attribute doit être une adresse courriel valide.",
    'exists' => "L' :attribute sélectionné est invalide",
    'file' => "L' :attribute doit être un fichier.",
    'filled' => "Le champ :attribute doit avoir une valeur.",
    'image' => "L' :attribute doit être une image.",
    'in' => "L' :attribute sélectionné est invalide",
    'in_array' => "Le :champ d'attribute n'existe pas en  :other.",
    'integer' => "L' :attribut doit être un entier.",
    'ip' => "L' :attribute doit être une adresse IP valide.",
    'ipv4' => "L' :attribute doit être une adresse IPv4 valide.",
    'ipv6' => "L' :attribute doit être une adresse IPv6 valide.",
    'json' => "L' :attribute doit être une chaîne JSON valide.",
    'max' => [
        'numeric' => " L' :attribute ne peut être supérieure à :max.",
        'file' => "L' :attribute ne peut être supérieur à :max Kilooctets ",
        'string' => "L' :attribute ne peut pas être supérieure à :max Caractères.",
        'array' => " L' :attribute ne peut pas avoir plus. Éléments :max.",
    ],
    'mimes' => "L' :attribute doit être un fichier de type: :values.",
    'mimetypes' => "L' :attribute doit être un fichier de type: :values.",
    'min' => [
        'numeric' => "L' :attribute doit être au moins :min.",
        'file' => "L' :attribute doit être au moins :min kilooctets.",
        'string' => "L' :attribute doit être au moins :min caractères.",
        'array' => "L' :attribute doit avoir au moins :min articles.",
    ],
    'not_in' => "L' :attribute sélectionné est invalide",
    'numeric' => "L' :attribute doit être un nombre.",
    'present' => " Le champ :attribute doit être présent.",
    'regex' => "Le  :format d' attribute est invalide.",
    'required' => "Le champ :attribute est nécessaire.",
    'required_if' => "Le champ :attribute est requis lorsque :other est :values.",
    'required_unless' => "Le champ :attribute est nécessaire sauf si :other est en :values.",
    'required_with' => "Le champ :attribute est requis lorsque :values est présent.",
    'required_with_all' => "Le champ :attribute est requis lorsque :values est présent.",
    'required_without' => "Le champ :attribute est requis lorsque :values n'est pas présent.",
    'required_without_all' => "Le champ :attribute est nécessaire lorsque aucune :values sont présentes ",
    'same' => "L' :attribute et :other doit correspondre.",
    'size' => [
        'numeric' => "L' :attribute doit être :size taille.",
        'file' => "L' :attribute doit être :size kilo-octets.",
        'string' => "L' :attribute doit être :size caractères.",
        'array' => "L' :attribute doit contenir :size éléments. ",
    ],
    'string' => "L' :attribute doit être une chaîne.",
    'timezone' => "L' :attribute doit être une zone valide.",
    'unique' => "L' :attribute a déjà été prise.",
    'uploaded' => "L' :attribute n'a pas réussi à télécharger.",
    'url' => "Le :format d'attribute est invalide.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => " Le mot de passe doit être alphanumérique.",
        ],
        'new_password' => [
            'regex' => ' Le mot de passe doit être alphanumérique.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Pays',
        'club_id' => 'Club',
        'feed_id' => 'Flux',
        'city_id' => 'Ville',
        'password' => 'Mot de passe',
        'hashed_email' => 'courriel',
        'hashed_phone' => 'Téléphone',
    ],
];
