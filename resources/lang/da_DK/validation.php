<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => "The :attribute skal accepteres.",
    'active_url' => "The :attribute er ikke en gyldig URL.",
    'after' => "The :attribute must be a date after :date.",
    'after_or_equal' => "The :attribute skal være en dato efter eller lig med :date.",
    'alpha' => "The :attribute må kun indeholde bogstaver.",
    'alpha_dash' => "The :attribute må kun indeholde bogstaver, tal, og tegn.",
    'alpha_num' => "The :attribute må kun indeholde bogstaver og tal.",
    'array' => "The :attribute skal være en array.",
    'before' => "The :attribute skal være en dato før :date.",
    'before_or_equal' => "The :attribute skal være en dato før eller lig med :date.",
    'between' => [
        'numeric' => "The :attribute skal være mellem :min og :max.",
        'file' => "The :attribute skal være mellem :min og :max kilobytes.",
        'string' => "The :attribute skal være mellem :min og :max karakterer.",
        'array' => "The :attribute skal være mellem :min og :max elementer.",
    ],
    'boolean' => "The :attribute feltet skal være sandt eller falsk.",
    'confirmed' => "The :attribute bekræftelsen stemmer ikke overens",
    'date' => "The :attribute er ikke en gyldig dato.",
    'date_format' => "The :attribute masher ikke formatet :format.",
    'different' => "The :attribute og :other skal være anderledes.",
    'digits' => "The :attribute skal være :digits cifre.",
    'digits_between' => "The :attribute skal være mellem :min og :max cifre.",
    'dimensions' => "The :attribute har ugyldige billeddimensioner.",
    'distinct' => "The :attribute feltet har en duplikatværdi.",
    'email' => "The :attribute skal være en gyldig e-mail adresse.",
    'exists' => "The valgte :attribute er ugyldig.",
    'file' => "The :attribute skal være en fil.",
    'filled' => "The :attribute feltet skal have en værdi.",
    'image' => "The :attribute skal være et billede.",
    'in' => "The valgte :attribute er ikke gyldig.",
    'in_array' => "The :attribute feltet findes ikke I :other.",
    'integer' => "The :attribute skal være et helt tal.",
    'ip' => "The :attribute skal være en gyldig IP adresse.",
    'ipv4' => "The :attribute skal være en gyldig IPv4 adresse.",
    'ipv6' => "The :attribute skal være en gyldig IPv6 adresse.",
    'json' => "The :attribute skal være en gyldig JSON string.",
    'max' => [
        'numeric' => "The :attribute må ikke være større end :max.",
        'file' => "The :attribute må ikke være større end :max kilobytes.",
        'string' => "The :attribute må ikke være større end :max karakterer.",
        'array' => "The :attribute må ikke have mere end :max items.",
    ],
    'mimes' => "The :attribute skal være en fil af typen: :values.",
    'mimetypes' => "The :attribute skal være en fil af typen: :values.",
    'min' => [
        'numeric' => "The :attribute skal være mindst :min.",
        'file' => "The :attribute skal være mindst :min kilobytes.",
        'string' => "The :attribute skal være mindst :min karakterer.",
        'array' => "The :attribute skal mindst have :min emner.",
    ],
    'not_in' => "The valgte :attribute er ugyldig.",
    'numeric' => "The :attribute skal være et tal.",
    'present' => "The :attribute feltet skal være tilstede.",
    'regex' => "The :attribute format er ugyldig.",
    'required' => "The :attribute feltet er påkrævet.",
    'required_if' => "The :attribute feltet er påkrævet når :other er :value.",
    'required_unless' => "The :attribute felt er påkrævet medmindre :other er i :values.",
    'required_with' => "The :attribute felt er påkrævet når :values er til stede.",
    'required_with_all' => "The :attribute felt er påkrævet når :values er til stede.",
    'required_without' => "The :attribute felt er påkrævet når :values ikke er til stede.",
    'required_without_all' => "The :attribute felt er påkrævet når ingen af :values er til stede.",
    'same' => "The :attribute og :other skat matche.",
    'size' => [
        'numeric' => "The :attribute skal være :size.",
        'file' => "The :attribute skal være :size kilobytes.",
        'string' => "The :attribute skal være :size karakterer.",
        'array' => "The :attribute skal indeholde :size emner.",
    ],
    'string' => "The :attribute skal være en string.",
    'timezone' => "The :attribute skal være en gyldig zone.",
    'unique' => "The :attribute er allerede taget.",
    'uploaded' => "The :attribute kunne ikke uploade.",
    'url' => "The :attribute formatet er ugyldig",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "The Adgangskoden skal være alfanumerisk.",
        ],
        'new_password' => [
            'regex' => 'The Adgangskoden skal være alfanumerisk.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Land',
        'club_id' => 'Klub',
        'feed_id' => 'Feed',
        'city_id' => 'BY',
        'password' => 'Adgangskode',
        'hashed_email' => 'e-mail',
    ],
];
