<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => "L' :attribute deve essere accettato.",
    'active_url' => "L' :attribute non e un URL valido.",
    'after' => "The :attribute must be a date after :date.", "L' :attribute deve essere una data dopo :date.",
    'after_or_equal' => "L' :attribute deve essere una data dopo o uguale alla :date.",
    'alpha' => "L' :attribute puo contere solo lettere.",
    'alpha_dash' => "L' :attribute puo contenere solo lettere, numeri e trattini.",
    'alpha_num' => "L' :attribute puo contenere solo lettere e numeri.",
    'array' => "L' :attribute deve essere un ordine.",
    'before' => "L' :attribute deve essere una data prima :date.",
    'before_or_equal' => "L' :attribute deve essere una data prima o dopo :date.",
    'between' => [
        'numeric' => "L' :attribute deve essere fra :min e :max.",
        'file' => "L' :attribute deve essere fra :min e :max kilobytes.",
        'string' => "L' :attribute deve essere fra :min e :max caratteri.",
        'array' => "L' :attribute deve avere fra :min e :max oggetti.",
    ],
    'boolean' => "Il campo :attribute deve essere vero o falso.",
    'confirmed' => "La conferma :attribute non corrisponde.",
    'date' => "L' :attribute non ha una data valida.",
    'date_format' => "L'attributo non corrisponde al formato :format.",
    'different' => " L' :attributo e :other devono essere diversi.",
    'digits' => " L' :attribute deve essere :digits numeri.",
    'digits_between' => "L' :attribute deve essere fra :min e :max numeri.",
    'dimensions' => "Le dimensioni dell'immagine dell' :attribute non sono valide.",
    'distinct' => "Il campo :field ha un valore doppio.",
    'email' => "L' :attribute deve essere un e-mail valido.",
    'exists' => "L' :attribute selezionto non e valido.",
    'file' => "L' :attribute deve essere un file.",
    'filled' => "Il campo :attribute deve avere un valore.",
    'image' => "L' :attribute deve essere un'immagine.",
    'in' => "L' :attribute selezionato non e valido.",
    'in_array' => "Il campo :attribute non esiste in :other.",
    'integer' => "L' :attribute deve essere un numero intero.",
    'ip' => "L' :attribute deve essere un indirizzo IP valido.",
    'ipv4' => "L' :attribute deve essere un indirizzo IPv4 valido.",
    'ipv6' => "L' :attribute deve essere un indirizzo IPv6 valido.",
    'json' => "L' :attribute deve essere un indirizzo JSON valido.",
    'max' => [
        'numeric' => "L' :attribute non puo essere piu grande di :max.",
        'file' => "L' :attribute non puo essere piu grande di :max kilobytes.",
        'string' => "L' :attribute non puo essere piu grande di :max caratteri.",
        'array' => "L' :attribute non puo essere piu grande di :max oggetti.",
    ],
    'mimes' => "L' :attribute deve essere un file del tipo :values.",
    'mimetypes' => "L' :attribute deve essere un file del tipo :values.",
    'min' => [
        'numeric' => "L' :attribute deve essere almeno :min.",
        'file' => "L' :attribute deve essere almeno :min kilobytes.",
        'string' => "L' :attribute deve essere almeno :min caratteri.",
        'array' => "L' :attribute deve essere almeno :min oggetti.",
    ],
    'not_in' => "L' :attribute selezionato non e valido.",
    'numeric' => "L' :attribute deve essere un numero.",
    'present' => "Il campo :attribute deve essere presente.",
    'regex' => "Il formato :attribute non e valido.",
    'required' => "Il campo :attribute e richiesto.",
    'required_if' => "Il campo :attribute e richiesto quando :other e :value.",
    'required_unless' => "Il campo :attribute e richiesto solo se :values e presente.",
    'required_with' => "Il campo :attribute e richiesto quando :values e presente.",
    'required_with_all' => "Il campo :attribute e richiesto quando :values e presente.",
    'required_without' => "Il campo :attribute e richiesto quando :values non e presente.",
    'required_without_all' => "Il campo :attribute e richiesto quando nessuno dei :values non e presente.",
    'same' => "L' :attribute e :others devono corrispondere.",
    'size' => [
        'numeric' => "L' :attribute deve essere :size.",
        'file' => "L' :attribute deve essere :size kilobytes.",
        'string' => "L' :attribute deve essere :size caratteri.",
        'array' => "L' :attribute deve essere :size oggetti.",
    ],
    'string' => "L' :attribute deve essere una sequenza.",
    'timezone' => "L' :attribute deve essere una zona valida.",
    'unique' => "L' :attribute e gia stato preso.",
    'uploaded' => "L' :attribute non e riuscito a caricare.",
    'url' => "Il formato :attribute non e valido.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "La Password deve essere alpha-numerica.",
        ],
        'new_password' => [
            'regex' => "La Password deve essere alpha-numerica.",
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Paese',
        'club_id' => 'Club',
        'feed_id' => 'Profilo',
        'city_id' => 'Citta',
        'password' => '"Password",',
        'hashed_email' => 'E-mail',
        'hashed_phone' => 'Telefono',
    ],
];
