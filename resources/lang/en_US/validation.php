<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => 'The Password must be alphanumeric.',
        ],
        'new_password' => [
            'regex' => 'The Password must be alphanumeric.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Country',
        'club_id' => 'Club',
        'feed_id' => 'Feed',
        'city_id' => 'City',
        'password' => 'Password',
        'hashed_email' => 'email',
        'hashed_phone' => 'phone no',
        //FormFields column name validation Mapping
        'data.0.height' => 'Height',
        'data.0.weight' => 'Weight',
        'data.0.gender_id' => 'Gender',
        'data.0.country_residence_id' => 'Country Residence',
        'data.0.city_residence_id' => 'City Residence',
        'data.0.nationality_id' => 'Nationality',
        'data.0.mother_language_id' => 'Mother Language',
        'data.0.other_languages' => 'Other Languages',
        'data.0.previous_club_id' => 'Previous Club',
        'data.0.preferred_foot_id' => 'Preferred Foot',
        'data.0.license' => 'License',
        'data.0.club_role_id' => 'Club Role',
        'data.0.season_id' => 'Season',
        'data.0.league_id' => 'League',
        'data.0.all_club_id' => 'All Club',
        'data.0.level_id' => 'Level',
        'data.0.status_id' => 'Status',
        'data.0.matches_played' => 'Matches Played',
        'data.0.goals' => 'Goals',
        'data.0.assists' => 'Assists',
        'data.0.mom' => 'Man of the Match',
        'data.0.prizes' => 'Prizes',
        'data.0.winnings' => 'Winnings',
        'data.0.draws' => 'Draws',
        'data.0.game_lost' => 'Game Lost',
        'data.0.total_games' => 'Total Games',
        'data.0.win_percentage' => 'Win Percentage',
        'data.0.club_chairman' => 'Club Chairman',
        'data.0.website' => 'Website',
        'data.0.email' => 'Email',
        'data.0.role_id' => 'Role',
        'data.0.intro' => 'Intro',
        'data.0.logo' => 'Logo',
        'data.0.country_id' => 'Country',
        'data.0.city_id' => 'City',
        'data.0.established_in_id' => 'Established In',
        'data.0.active_players' => 'Active Players',
        'data.0.total_teams' => 'Total Teams',
        'data.0.stadium' => 'Stadium',
        'data.0.club_id' => 'Club',
        'data.0.category_id' => 'Category',
        'data.0.title' => 'Title',
        'data.0.position_id' => 'Position',
        'data.0.salary' => 'Salary',
        'data.0.is_salary_hidden' => 'Salary Hidden',
        'data.0.description' => 'Description',
        'data.0.start_date' => 'Start Date',
        'data.0.end_date' => 'End Date',
        'data.0.id' => 'id',
        'data.0.sport_id' => 'Sport',
        'data.0.dob' => 'Date of Birth',
        'data.0.current_club_id' => 'Current Club',
        'data.0.has_license' => 'Has License',
        'data.0.rated_by' => 'Rated By',
        'data.0.playing_role_id' => 'Playing Role',
        'data.0.batting_style_id' => 'Batting Style',
        'data.0.bowling_style_id' => 'Bowling Style',
        'data.0.runs' => 'Runs',
        'data.0.fifties' => 'Fifties',
        'data.0.centuries' => 'Centuries',
        'data.0.wickets' => 'Wickets',
        'data.0.highest' => 'Highest',
        'data.0.best_bowling' => 'Best Bowling',
        'data.0.name' => 'Name',
        'data.0.company' => 'Company',
    ],
];
