<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => ":attribute kabul edilmelidir.",
    'active_url' => ":attribute geçerli bir URL deðil.",
    'after' => ":attribute sonra bir tarih olmalýdýr :date.",
    'after_or_equal' => ":attribute sonra veya eþdeðer bir tarih olmalýdýr :date.",
    'alpha' => ":attribute sadece harflerden oluþmalýdýr.",
    'alpha_dash' => ":attribute sadece harf,rakam ve çizgilerden oluþmalýdýr.",
    'alpha_num' => ":attribute sedece harf ve rakamlardan oluþmalýdýr.",
    'array' => ":attribute bir dizi olmalýdýr.",
    'before' => ":attribute önce bir tarih olmalýdýr :date.",
    'before_or_equal' => ":önce veya eþdeðer bir tarih olmalýdýr :date.",
    'between' => [
        'numeric' => ":attribute arasýnda olmalýdýr :min ve :max.",
        'file' => ":attribute arasýnda olmalýdýr :min ve :max kilobayt.",
        'string' => ":attribute arasýnda olmalýdýr :min ve :max karakter.",
        'array' => ":attribute arasýnda olmalýdýr :min ve :max öðe.",
    ],
    'boolean' => ":attribute alaný doðru veya yanlýþ olmalýdýr.",
    'confirmed' => ":attribute onayý eþleþmiyor.",
    'date' => ":attribute geçerli bir tarih deðil.",
    'date_format' => ":attribute formatla eþleþmiyor :format.",
    'different' => ":attribute ve :other farklý olmalýdýr.",
    'digits' => " :attribute olmalýdýr :digits hane.",
    'digits_between' => " :attribute arasýnda olmalýdýr :min ve :max hane.",
    'dimensions' => ":attribute geçersiz resim boyutlarýna sahip.",
    'distinct' => ":attribute alaný tekrarlayan bir deðere sahip.",
    'email' => ":attribute geçerli bir e-posta adresi olmalýdýr.",
    'exists' => "Seçilen :attribute geçersiz.",
    'file' => ":attribute bir dosya olmalýdýr.",
    'filled' => ":attribute alaný bir deðere sahip olmalýdýr.",
    'image' => ":attribute bir resim olmalýdýr.",
    'in' => "Seçilen :attribute geçersiz.",
    'in_array' => ":attribute dosyasý mevcut deðil :other'in içinde.",
    'integer' => ":attribute bir tam sayý olmalýdýr.",
    'ip' => "attribute geçerli bir IP adresi olmalýdýr.",
    'ipv4' => ":attribute geçerli bir IPv4 adresi olmalýdýr.",
    'ipv6' => ":attribute geçerli bir IPv6 adresi olmalýdýr.",
    'json' => ":attribute geçerli bir JSON string olmalýdýr.",
    'max' => [
        'numeric' => ":attribute büyük olmayabilir :max dan.",
        'file' => ":attribute büyük olmayabilir :max kilobayt'tan.",
        'string' => ":attribute büyük olmayabilir:max karakter'den.",
        'array' => ":attribute fazla olmayabilir :max öðeden.",
    ],
    'mimes' => ":attribute türünde bir dosya olmalýdýr: :deðerler.",
    'mimetypes' => ":attribute türünde bir dosya olmalýdýr: :deðerler.",
    'min' => [
        'numeric' => ":attribute en az olmalýdýr :min.",
        'file' => ":attribute en az olmalýdýr:min kilobayt.",
        'string' => " :attribute en az olmalýdýr :min karakter.",
        'array' => ":attribute en az sahip olmalýdýr :min öðe.",
    ],
    'not_in' => "Seçilen:attribute geçersiz.",
    'numeric' => ":attribute bir rakam olmalýdýr.",
    'present' => "attribute alaný var olmalýdýr.",
    'regex' => " :attribute formatý geçersiz.",
    'required' => ":attribute alaný gerekiyor.",
    'required_if' => " :attribute alaný dýðýnda gereklidir :diðer :deðer.",
    'required_unless' => ":attribute alaný madýkça gereklidir:diðeri içinde :deðerler.",
    'required_with' => ":attribute alaný dýðýnda gereklidir :deðerler mevcut.",
    'required_with_all' => ":attribute alaný dýðýnda gereklidir :deðerler mevcut.",
    'required_without' => ":attribute alaný dýðýnda gereklidir :deðerler mevcut deðil.",
    'required_without_all' => ":attribute alaný hiçbiri madýðýnda gereklidir :deðerler mevcut.",
    'same' => ":attribute ve :diðeri eþleþmelidir.",
    'size' => [
        'numeric' => ":attribute olmalýdýr :boyut.",
        'file' => "attribute olmalýdýr : kilobayt boyutu.",
        'string' => ":attribute olmalýdýr :karakter boyutu.",
        'array' => ":attribute içermelidir :öðe boyutu.",
    ],
    'string' => "attribute bir string olmalýdýr.",
    'timezone' => " :attribute geçerli bir alan olmalýdýr.",
    'unique' => ":attribute çoktan alýndý.",
    'uploaded' => ":attribute yüklemesi baþarýsýz oldu.",
    'url' => ":attribute formatý geçersiz.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'password' => [
            'regex' => "Parola alfanümerik olmalýdýr.",
        ],
        'new_password' => [
            'regex' => "Parola alfanümerik olmalýdýr.",
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'country_id' => 'Ülke',
        'club_id' => 'Kulüp',
        'feed_id' => 'Yayýn',
        'city_id' => 'Þehir',
        'password' => 'Parola',
        'hashed_email' => 'e-posta',
        'hashed_phone' => 'telefon',
    ],
];
