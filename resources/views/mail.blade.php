<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php
    $settings = [
        'email_bg' => '#e8e5e7',
        'header_bg' => '#232740',
        'header_text_size' => '26',
        'header_text_color' => '#fc6715',
        'body_bg' => '#ffffff',
        'body_text_size' => '13',
        'body_text_color' => '#000000',
        'footer_text' => '',
        'footer_bg' => '#ffffff',
        'footer_text_size' => '11',
        'highlight_color' => '#D84800',
        'footer_text_color' => '#525252'
    ];
    ?>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width" /> <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Use the latest (edge) version of IE rendering engine -->
        <!-- CSS Reset -->
        <style type="text/css">
            /* What it does: Remove spaces around the email design added by some email clients. */
            /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
            html,body {
                margin: 0 !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }
            /* What it does: Stops email clients resizing small text. */
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            /* What it does: Forces Outlook.com to display emails full width. */
            .ExternalClass {
                width: 100%;
            }
            /* What is does: Centers email on Android 4.4 */
            div[style*="margin: 16px 0"] {
                margin: 0 !important;
            }
            /* What it does: Stops Outlook from adding extra spacing to tables. */
            table,td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }
            /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                Margin: 0 auto !important;
            }
            table table table {
                table-layout: auto;
            }
            /* What it does: Uses a better rendering method when resizing images in IE. */
            /* & manages img max widths to ensure content body images don't exceed template width. */
            img {
                -ms-interpolation-mode:bicubic;
                height: auto;
                max-width: 100%;
            }
            /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
            .yshortcuts a {
                border-bottom: none !important;
            }
            /* What it does: A work-around for iOS meddling in triggered links. */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: underline !important;
            }
        </style>
    </head>
    <body class="email_bg" width="100%" height="100%" bgcolor="<?php echo $settings['email_bg']; ?>" style="margin: 0;">
        <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="<?php echo $settings['email_bg']; ?>" style="border-collapse:collapse;" class="email_bg">
            <tr>
                <td valign="top">
                    <center style="width:100%">
                        <div style="max-width:600px">
                            <!--[if (gte mso 9)|(IE)]>
                            <table cellspacing="0" cellpadding="0" border="0" width="600" align="center">
                            <tr>
                            <td>
                            <![endif]-->

                            <!-- Email Header : BEGIN -->

                            <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px; border-top: 7px solid <?php echo $settings['highlight_color']; ?>" bgcolor="<?php echo $settings['header_bg']; ?>" class="header_bg">
                                <tr>
                                    <td style="text-align: center; padding: 10px 0px 5px; font-family: sans-serif; mso-height-rule: exactly; font-weight: bold; color: <?php echo $settings['header_text_color']; ?>; font-size: <?php echo $settings['header_text_size'] . 'px'; ?>" class="header_text_color header_text_size">
                                        <a href="https://www.playermatch.com">
                                            <img width="177" src="https://www.playermatch.com/static/images/logo.png" alt="Player Match" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <!-- Email Header : END -->

                            <!-- Email Body : BEGIN -->
                            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="<?php echo $settings['body_bg']; ?>" width="100%" style="max-width: 600px; border-radius: 5px;" class="body_bg">

                                <!-- 1 Column Text : BEGIN -->
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 20px; font-family: sans-serif; mso-height-rule: exactly; line-height: <?php echo floor($settings['body_text_size'] * 1.618) . 'px' ?>; color: <?php echo $settings['body_text_color']; ?>; font-size: <?php echo $settings['body_text_size'] . 'px'; ?>" class="body_text_color body_text_size">
                                                    <span style="font-weight: bold; font-size: <?php echo floor($settings['body_text_size'] * 1.35) . 'px'; ?>" class="welcome"><?php echo __('Hi') ?> {{$name}},</span>
                                                    <hr color="<?php echo $settings['email_bg']; ?>" />
                                                    <br />
                                                    <!-- Dynamic Content : BEGIN -->
                                                    <?php echo __('A Big THANK YOU for making an account on PlayerMatch. You are all ready to achieve your Goals!') ?><br />
                                                    <br />
                                                    <blockquote style="background-color: #f0f0f0; padding:15px;margin:0;"><?php echo __('Here is your Verification Code') ?>: <b>{{$link}}</b></blockquote><br />
                                                    <?php echo __('PlayerMatch provides an exhilarating platform to the players and the game professionals to stay in touch and promote yourself to grasp the best opportunities to grow! We bring supple and insight to our gaming users through our friendly format that lets you interact with professionals from all around the world and stay updated with most happening stuff in the field. And you can be connecting through messages, posting news through links and sharing pictures, videos and much more.') ?><br />
                                                    <br />
                                                    <?php echo __('We believe in the perfect match … a PlayerMatch!!') ?><br />
                                                    <br />
                                                    <?php echo __('Thanks') ?>,<br />
                                                    <?php echo __('Team PlayerMatch!') ?>
                                                    <!-- Dynamic Content : END -->
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- 1 Column Text : END -->

                            </table>
                            <!-- Email Body : END -->

                            <!-- Email Footer : BEGIN -->
                            <table cellspacing="0" cellpadding="0" border="0" align="left" width="100%" style="max-width: 600px; border-radius: 5px;" bgcolor="<?php echo $settings['footer_bg']; ?>" class="footer_bg">
                                <tr>
                                    <td style="padding: 20px; width: 100%; font-size: <?php echo $settings['footer_text_size'] . 'px'; ?>; font-family: sans-serif; mso-height-rule: exactly; line-height: <?php echo floor($settings['footer_text_size'] * 1.618) . 'px' ?>; text-align: left; color: <?php echo $settings['footer_text_color']; ?>;" class="footer_text_color footer_text_size">
                                        <table style="padding: 0; border-top: 1px solid #555555; margin: 0 auto; border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td>
                                                    <p><?php echo __('You received an email from PlayerMatch. PlayerMatch will be using your email address to make suggestions to our users in features like People You May Know. If you do not wish to receive such emails in the future, please') ?> <a href="{{$unsub_link}}" style="text-decoration: underline; color: <?php echo $settings['highlight_color']; ?>"><?php echo __('unsubscribe') ?></a></p>
                                                    <p><?php echo __('If you need any assistance or have questions, please contact') ?> <a style="text-decoration: underline; color: <?php echo $settings['highlight_color']; ?>" href="https://www.playermatch.com/contact" target="_blank" ><?php echo __('PlayerMatch Support Service.') ?></a></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- Email Footer : END -->

                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>