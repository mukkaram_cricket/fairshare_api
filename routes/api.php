<?php

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */



Route::post('/login', 'AuthController@login');
Route::post('/signup', 'AuthController@signup');
Route::post('/email_verification', 'AuthController@email_verification');
Route::post('/forgot_password', 'AuthController@forget_password');
Route::post('/reset_password_api', 'AuthController@reset_password');
Route::post('/app_reset_password', 'AuthController@app_reset_password');
//Route::post('/mobile_verification', 'AuthController@mobile_verification');
Route::post('/resend_verification_code', 'AuthController@resend_sms_verification_code');
Route::get('/download_userprofile', 'UserProfileController@DownloadUserProfile');
Route::get('/resend_email_verification', 'AuthController@resend_email_verification');
Route::post('/send_email/', 'MailController@BasicEmail');
Route::post('upload-video', 'UploadController@videoUpload');

Route::get('/show_top_clubs', 'ClubController@ShowTopClubs');

Route::get('/get-user-info', 'UsersController@getUserInfo');




/**** Category start  ***/
/*
 * Get category and sub category for menu
 */
Route::get('/category', 'CategoryController@index');

/**** Category end  ***/

/* * ***********For LoggedIn Users API's********* */
Route::group(['middleware' => ['verifytoken:compulsory']], function() {



    /*** wishlist start **/
    Route::group(['prefix' => 'wishlist'], function () {
        Route::get('/', 'UserWishlistController@index');
        Route::post('store', 'UserWishlistController@store');
        Route::get('/name', 'UserWishlistController@get_user_wishlist');
    });
    /*** wishlist end **/


    /*** Add or remove favourite item start ***/
    Route::group(['prefix' => 'favorite'], function (){
        Route::post('/store', 'FavoriteController@store');
        Route::post('/delete', 'FavoriteController@destroy');
        Route::get('/show', 'FavoriteController@show');
    });

    /*** Add or remove favourite item start ***/


    /*** Items in wishlist start **/
    Route::group(['prefix' => 'wishlist/item'], function () {
        Route::get('/', 'UserWishlistItemController@index');
        Route::post('store', 'UserWishlistItemController@store');
        Route::get('/show/{id}', 'UserWishlistItemController@show');
    });
    /*** Items in wishlist end **/

    /*** wishlist start **/
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index');
        Route::post('store', 'ProductController@store');
        Route::get('/show', 'ProductController@show');

    });
    /*** wishlist end **/


    Route::post('/logout', 'AuthController@logoutApi');
    Route::post('/update-user', 'AuthController@UpdateUser');
    Route::get('/activate_user', 'AuthController@ActivateUser');
    Route::post('imageUpload', 'UploadController@imageUpload');
    Route::post('/get_fields', 'FormFieldsController@get_fields');
    Route::post('/get_data', 'FormFieldsController@get_values');
    Route::get('get_positions/{id}', 'FormFieldsController@getPositions');

    /*     * ******countries********* */
    Route::get('/countries/', 'CountriesController@GetCountries');
    Route::get('/get-cities', 'CountriesController@getCities');
    Route::get('/search-cities', 'CountriesController@searchCities');

    /*     * ******static data by country********* */
    Route::get('/get_data_by_country/', 'CountriesController@GetData');
    Route::get('/site-data', 'FeedsController@siteData');

    /*     * ******Settings********* */
    Route::post('/change_password/', 'SettingsController@ResetPassword');
    Route::put('/change_role/', 'SettingsController@ChangeRole');
    Route::post('/disable_account/', 'SettingsController@DisableAccount');
    Route::put('/update_default_privacy', 'SettingsController@UpdateDefaulPrivacy');
    Route::get('/get_profile_privacy/', 'SettingsController@GetProfilePrivacy');
    Route::get('/get_privacy/', 'SettingsController@GetPrivacy');
    Route::put('/update_profile_visibility/', 'SettingsController@UpdateProfileVisbility');
    Route::post('/update_default_language', 'SettingsController@UpdateDefaultLanguage');
    Route::post('/change_email', 'SettingsController@ChangeEmail');
    Route::post('/change_email_verification', 'SettingsController@checkEmailVerificationCode');
    Route::post('/delete_account', 'SettingsController@DeleteAccount');
    Route::post('/delete_account_verification', 'SettingsController@DeleteAccountVerification');
    Route::post('/change_email_notifications', 'SettingsController@ChangeEmailNotifications');
    Route::post('/change_fcm_notifications', 'SettingsController@ChangeFCMNotifications');
    Route::get('/get_email_notification_setting', 'SettingsController@GetEmailNotificationSetting');
    Route::get('/get_fcm_notification_setting', 'SettingsController@GetFcmNotificationSetting');
    Route::post('/download_user_data', 'SettingsController@DownloadUserData');
    Route::post('/suspend-user-account', 'SettingsController@suspendUserAccount');

    /*     * ******UserCareer********* */
    Route::post('/add_usercareer', 'UserCareerController@AddUserCareer');
    Route::post('/delete_usercareer', 'UserCareerController@Delete');
    Route::post('/career_stats', 'UserCareerController@get_career_stats');

    /*     * ******Cities********* */
//    Route::get('/cities_by_country/', 'CitiesController@CitiesByCountry');
    /*     * ******Language********* */

    //Route::get('/languages/', 'LanguagesController@index');

    /*     * ******UserProfile********* */

    Route::post('/add_userprofile/', 'UserProfileController@AddUserProfile');
//    Route::get('/get_userprofile/{id}', 'UserProfileController@GetUserProfile');
    Route::get('/profile_header', 'UserProfileController@get_profile_header');
    Route::get('/profile_visitors', 'UserProfileController@GetProfileVisitors');
    Route::get('/user_country', 'UserProfileController@getUserCountry');
//    Route::get('/download_userprofile', 'UserProfileController@DownloadUserProfile');

    /*     * ******Feeds********* */
    Route::get('/user-feeds', 'FeedsController@getFeeds');
    Route::get('/feed-photos', 'FeedsController@getFeedPhotos');
    Route::get('/feed-videos', 'FeedsController@getFeedVideos');
    Route::post('/like-feed', 'FeedsController@likeFeed');
    Route::post('/un-like-feed', 'FeedsController@unLikeFeed');
    Route::post('/delete-feed', 'FeedsController@deleteFeed');
    Route::post('/save-feed', 'FeedsController@saveFeed');
    Route::post('/delete-feed-comment', 'FeedsController@deleteComment');
    Route::post('/update-feed', 'FeedsController@updateFeed');
    Route::post('/save-feed-count', 'FeedsController@saveFeedCount');
    Route::post('/comment-feed', 'FeedsController@commentFeed');
    Route::get('/get-feed-comments', 'FeedsController@getFeedComments');
    Route::post('/rate-feed', 'FeedsController@rateFeed');
    Route::post('/un-pin-feed', 'FeedsController@unPinFeed');
    Route::get('/free-style-winner', 'FeedsController@freeStyleWinner');
    Route::post('/vote-feed', 'FeedsController@voteFeed');
    Route::get('/get-feeling-activities', 'FeedsController@getFeelingActivities');

    /*     * ******Skills********* */
//    Route::get('/skills/', 'SkillsController@index');


    /*     * ******SkillGroups********* */
//    Route::get('/skillgroups/', 'SkillsgroupController@index');
    Route::get('/get_skills_group/', 'SkillsgroupController@GetSkillsGroup');


//    Route::post('/skillgroups/', 'SkillsgroupController@store');

    /*     * ******UserSkills********* */
    Route::post('/add_user_skills/', 'UserSkillsController@AddUserSkills');
    Route::get('/user_skills/', 'UserSkillsController@GetUserSkills');
    Route::get('/user_top_skills/', 'UserSkillsController@GetTopSkills');
    Route::get('/skills_for_home/', 'UserSkillsController@SkillsForHome');
    Route::get('/fetch_user_skills/', 'UserSkillsController@FetchUserSkills');
    /*     * ******Endorsements********* */
    /* Route::get('/Enndorsements/GetEndorsements/{GetEndorsements}', 'EnndorsementsController@GetEndorsements'); */
    Route::post('/add_enndorsements/', 'EnndorsementsController@AddEnndorsements');

    /*     * ******Recommendation********* */
    Route::get('/get_recommendations/', 'RecommendationsController@GetRecommendations');
    Route::post('/add_recommendations/', 'RecommendationsController@AddRecommendations');

    /*     * ******Messages********* */
    Route::post('/compose_message/', 'MessagesController@ComposeMessage');
    Route::get('/get_conversations/', 'MessagesController@GetConversations');
    Route::get('/get_messages', 'MessagesController@GetConversationMessages');
    Route::post('/delete_conversation/', 'MessagesController@DeleteConversation');
    Route::post('/message_reply/', 'MessagesController@ReplyMessage');
    Route::post('/read_message/', 'MessagesController@ReadMessage');
    Route::get('/unread_messages/', 'MessagesController@UnreadMessages');
    Route::post('/action_apply/{action}', 'MessagesController@ActionApply');
    Route::get('/get-thread-id', 'MessagesController@getThreadId');

    /*     * ******Btags********* */
    Route::get('/get_btags/', 'BtagsController@GetBtags');

    /*     * ******Blogs********* */

    Route::post('/add_blog/', 'BlogsController@AddBlog');
    Route::post('/add_comment/', 'BlogsController@AddComment');
    Route::post('/add_reply/', 'BlogsController@AddReply');
    Route::delete('/delete_blog/', 'BlogsController@DeleteBlog');
    Route::delete('/delete_comment/', 'BlogsController@DeleteComment');
    Route::delete('/delete_reply/', 'BlogsController@DeleteReply');
    Route::post('/update_blog/', 'BlogsController@UpdateBlog');
    /*     * ******Users********* */
    Route::get('/get-friends-players', 'UsersController@searchFriendsOrPlayers');
    Route::get('/get_users/', 'UsersController@searchUserFriend');
    Route::get('/search_users/', 'UsersController@SearchUsers');
    Route::get('/friend_list/', 'UsersController@FriendList');
//    Route::get('/all_suggestion', 'UsersController@AllSuggestions');
//    Route::get('/get_current_user', 'UsersController@GetCurrentUser');
    Route::get('/get_profile_progress', 'UsersController@ProfileProgress');
//    Route::get('/search-all-users', 'UsersController@SearchAllUser');
    Route::get('/search-all-players', 'UsersController@SearchAllPlayers');
    Route::post('/send-email-invitations', 'UsersController@sendEmailInvitations');
    Route::post('/filter-non-register-users', 'UsersController@nonRegisteredEmails');
    Route::post('/filter-non-register-numbers', 'UsersController@nonRegisteredNumbers');
    Route::get('/online_friends', 'UsersController@OnlineFriends');
    Route::post('/jobs_invitation', 'UsersController@JobsInvitation');
    Route::get('/get_all_user', 'UsersController@GetAllUser');
    Route::get('/get_user_emails', 'UsersController@GetUserEmails');
    Route::get('/user_under_sixteen', 'UsersController@userUnderSixteen');
    Route::post('/upload_parent_consent_file', 'UsersController@uploadParentConsentFile');
    Route::post('/upload_agent_license_file', 'UsersController@uploadAgentLicenseFile');
    Route::get('/users_with_consent_file', 'UsersController@UsersWithConsentFile');
    Route::post('/upload-general-file', 'UsersController@uploadGeneralFile');

    /*     * ******Friendship********* */
    Route::get('/friend-requests', 'FriendshipController@friendRequests');
    Route::get('/friends', 'FriendshipController@getUserFriends');
    Route::put('/send_request', 'FriendshipController@send_request');
    Route::put('/accept_request', 'FriendshipController@accept_request');
    Route::put('/cancel_request', 'FriendshipController@cancel_request');
    Route::put('/reject_request', 'FriendshipController@reject_request');
    Route::put('/cancel_friendship', 'FriendshipController@unfriend_user');

    /*     * ******Notification********* */
    Route::get('/get-un-view-count', 'NotificationsController@getUnViewCount');
    Route::get('/notifications', 'NotificationsController@getNotifications');
    Route::post('/mark-read', 'NotificationsController@markAsRead');
    Route::post('/mark-all-viewed', 'NotificationsController@markAllAsViewed');


    /*     * ******Clubs********* */
    Route::post('/create_club/', 'ClubController@CreateClub');
    Route::get('/is_my_club', 'ClubController@CheckClubOfficial');
    Route::get('/show_clubs', 'ClubController@ShowAllClubs');
    Route::get('/club_detail', 'ClubController@GetClubDetail');
    Route::get('/get_officials', 'ClubController@GetClubOfficials');
    Route::post('/send_official_request', 'ClubController@SendOfficialRequest');
    Route::post('/accept_official_request', 'ClubController@AcceptOfficialRequest');
    Route::post('/cancel_official_request/', 'ClubController@CancelOfficialRequest');
    Route::post('/reject_official_request', 'ClubController@RejectOfficialRequest');
    Route::post('/remove_official_request', 'ClubController@RemoveOfficial');
    Route::post('/get_followers', 'ClubController@GetClubFollowers');
    Route::post('/follow_club', 'ClubController@FollowClub');
    Route::post('/unfollow_club', 'ClubController@UnFollowClub');
    Route::get('/pending-officials', 'ClubController@GetPendingOfficials');
    Route::get('/my-clubs', 'ClubController@getMyClubs');
    Route::get('/get-country-cities', 'ClubController@getCitiesHavingClubs');

    /*     * ******Jobs********* */
    Route::post('/create_job/', 'JobController@CreateJob');
    Route::get('/job_detail', 'JobController@GetJobDetail');
    Route::post('/delete-job', 'JobController@deleteJob');
    Route::post('/send-job-invitations', 'JobController@SendJobInvitations');
    Route::get('/job-applications', 'JobController@getJobApplications');
    Route::post('/job-reminder', 'JobController@RemindJob');

    Route::get('/search_jobs', 'JobController@SearchJob');
//    Route::get('/my_jobs/', 'JobController@MyJobs');
//    Route::post('/get_jobs', 'JobController@get_jobs');

    /*     * ******Applications********* */
    Route::get('/get-applicant-detail', 'ApplicationController@getApplicantDetail');
    Route::get('/application-detail', 'ApplicationController@getApplicationDetail');
    Route::post('/apply_application', 'ApplicationController@ApplyApplication');
//    Route::post('/application_listing/', 'ApplicationController@ApplicationListing');
    Route::get('/get-my-applications', 'ApplicationController@MyApplications');
    Route::get('/get-my-applicants', 'ApplicationController@MyApplicants');

    /*     * ******Advance Search********* */
    Route::post('/search/', 'UsersController@advancedSearch');
    Route::post('/search-clubs/', 'UsersController@advancedSearchCLubs');
    /*     * ******Club team and matches********* */
    Route::post('/create_team/', 'ClubTeamsController@CreateClubTeam');
    Route::post('/create_matches/', 'ClubTeamsController@CreateClubMatches');
    Route::post('/create_teamplayers/', 'ClubTeamsController@CreateTeamPlayers');
    Route::post('/create_matchlineup/', 'ClubTeamsController@CreateMatchLineup');
    Route::post('/rate_player/', 'ClubTeamsController@RatePlayer');
    Route::post('/match_result/', 'ClubTeamsController@MatchResult');

    /*     * ******SpamReport********* */
    Route::post('/add_spam', 'SpamReportController@AddToSpam');
    /** General API's * */
//    Route::post('/i-am-online', 'AuthController@iAmOnline');
    Route::get('/get_suggestion', 'UsersController@GetSuggestions');
    Route::get('/job_listing', 'JobController@GetJobListing');
    /*     * * Matches APIS* */
    Route::post('/create-match', 'MatchesController@createMatch');
    Route::post('/update-match', 'MatchesController@updateMatch');
    Route::post('/update-match-line-up', 'MatchesController@updateMatchLineUp');
    Route::get('/get-matches', 'MatchesController@getMatches');
    Route::get('/get-match-detail', 'MatchesController@getMatcheDetail');
    Route::get('/get-create-match-data', 'MatchesController@getCreateMatchData');
    Route::get('/fetch-autocomplete', 'MatchesController@fetchAutocomplete');
    Route::post('/accept-reject-match', 'MatchesController@acceptRejectMatch');
    Route::post('/accept-reject-match-joining', 'MatchesController@acceptRejectMatchJoining');
    Route::post('/delete-match', 'MatchesController@deleteMatch');
    Route::post('/invite-again-player', 'MatchesController@inviteAgainPlayer');
    Route::post('/save-match-result', 'MatchesController@saveMatchResult');
    Route::post('/save-player-result', 'MatchesController@savePlayerResult');
    Route::post('/save-match-tactics', 'MatchesController@saveMatchTactics');
    Route::get('/get-teams', 'MatchesController@getTeams');
    Route::get('/get-teams-with-players', 'MatchesController@getTeamsWithPlayers');
    Route::get('/get-team-players', 'MatchesController@getTeamPlayers');
    Route::get('/get-away-teams', 'MatchesController@getAwayTeams');
    Route::post('/save-team', 'MatchesController@saveTeam');
    Route::post('/accept-reject-team-player-request', 'MatchesController@acceptRejectTeamPlayerRequest');
    Route::get('/get-team-detail', 'MatchesController@getTeamDetail');
    Route::get('/delete-team', 'MatchesController@deleteTeam');
    Route::post('/show-interest', 'MatchesController@showInterest');
    Route::post('/save-toss-detail', 'MatchesController@saveTossDetail');
    Route::get('/get-scorebook-detail', 'MatchesController@getScoreBookDetail');
    Route::get('/get-scorebook-summary', 'MatchesController@getScoreBookSummary');
    Route::post('/change-bowler', 'MatchesController@changeBowler');
    Route::post('/change-batsman', 'MatchesController@changeBatsman');
    Route::post('/update-score', 'MatchesController@updateScore');
    Route::get('/get-over-bowls', 'MatchesController@getOverBowls');
    Route::post('/edit-bowled-ball', 'MatchesController@editBowledBall');
    Route::post('/delete-last-ball', 'MatchesController@deleteLastBall');
    Route::post('/update-onstriker', 'MatchesController@updateOnStrike');
    Route::post('/change-inning', 'MatchesController@changeInning');
    Route::post('/mark-match-finished', 'MatchesController@markCricketMatchFinished');
});

/* * ***For LoggedIn & Guest Users API's ******* */
Route::group(['middleware' => ['verifytoken:optional']], function() {
    Route::get('/get-pre-data', 'FeedsController@preData');
});
