<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


//$factory->define(App\Models\Product::class, function (Faker $faker) {
//
//    return [
//        'name' => $faker->sentence,
//        'brand_id' => 1,
//        'category_id' => 7,
//        'actual_price' => $faker->randomNumber(2),
//        'status' => 1,
//        'slug' => 'collusion-x001-skinny-jeans-in-black',
//        'type' => 1
//    ];
//});
