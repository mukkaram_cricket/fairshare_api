<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageRecipientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_recipients', function(Blueprint $table)
		{
			$table->bigInteger('thread_id')->unsigned()->index('fk_thread_id_msgrecipient');
			$table->bigInteger('user_id')->unsigned()->index('fk_user_id_msgrecipient');
			$table->boolean('is_view')->nullable()->default(0);
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
			$table->primary(['thread_id','user_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_recipients');
	}

}
