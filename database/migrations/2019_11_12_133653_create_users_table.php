<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('email')->nullable()->unique('user_email');
			$table->string('phone')->nullable();
			$table->string('name');
			$table->string('slug', 200)->unique('user_slug');
			$table->string('avatar', 100)->nullable();
			$table->string('password', 60)->nullable();
			$table->char('activation_key', 32)->nullable();
			$table->boolean('role_id')->nullable()->default(2)->index('fk_role_id_users');
			$table->smallInteger('country_id')->unsigned()->nullable()->index('fk_country_users');
			$table->boolean('is_email_verified')->default(0);
			$table->boolean('is_phone_verified')->default(0);
			$table->char('locale', 5)->default('en_US');
			$table->dateTime('register_date')->nullable()->comment('when user verified him');
			$table->enum('status', array('InActive','Active','Spam','Deactivated'))->default('InActive');
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
