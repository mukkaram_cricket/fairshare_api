<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_notifications', function(Blueprint $table)
		{
			$table->bigInteger('user_id')->unsigned()->index('fk_user_id_email_notif');
			$table->boolean('activity_mention')->nullable()->default(1);
			$table->boolean('feed_comment')->nullable()->default(1);
			$table->boolean('comment_mention')->nullable()->default(1);
			$table->boolean('comment_reply')->nullable()->default(1);
			$table->boolean('new_message')->nullable()->default(1);
			$table->boolean('message_reply')->nullable()->default(1);
			$table->boolean('friend_req_recieved')->nullable()->default(1);
			$table->boolean('friend_req_accepted')->nullable()->default(1);
			$table->boolean('job_invitation')->nullable()->default(1);
			$table->boolean('match_request')->nullable()->default(1);
			$table->boolean('match_request_accepted')->nullable()->default(1);
			$table->boolean('match_request_rejected')->nullable()->default(1);
			$table->boolean('player_invite_match')->nullable()->default(1);
			$table->boolean('match_joining_accepted')->nullable()->default(1);
			$table->boolean('match_joining_rejected')->nullable()->default(1);
			$table->boolean('match_result_added')->nullable()->default(1);
			$table->boolean('player_result_added')->nullable()->default(1);
			$table->boolean('official_req_rejected')->nullable()->default(1);
			$table->boolean('official_req_accepted')->nullable()->default(1);
			$table->boolean('player_invite_team')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_notifications');
	}

}
