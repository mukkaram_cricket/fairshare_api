<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserApisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_apis', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('user_id')->unsigned()->index('user_id');
			$table->string('login_type', 100)->nullable();
			$table->string('token', 100)->nullable();
			$table->string('fcm_token', 512)->nullable();
			$table->string('user_agent', 500)->nullable();
			$table->string('timezone', 100)->nullable()->default('utc');
			$table->boolean('status')->nullable();
			$table->dateTime('last_used');
			$table->dateTime('date_created')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_apis');
	}

}
