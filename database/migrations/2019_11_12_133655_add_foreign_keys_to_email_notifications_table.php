<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmailNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('email_notifications', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_user_id_email_notif')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('email_notifications', function(Blueprint $table)
		{
			$table->dropForeign('fk_user_id_email_notif');
		});
	}

}
