<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMultiselectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('multiselect', function(Blueprint $table)
		{
			$table->bigInteger('entity_id')->unsigned()->comment('Main Auther e.g User Job Club');
			$table->bigInteger('source_id')->unsigned()->index('fk_user_otherlanguages')->comment('Ids of Selected Options e.g Countries job_positions');
			$table->enum('type', array('work_area','specific_connections','job_reminder','views_count'))->nullable();
			$table->primary(['entity_id','source_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('multiselect');
	}

}
