<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLaravelFailedJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('laravel_failed_jobs', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->text('connection', 65535);
			$table->text('queue', 65535);
			$table->text('payload', 65535);
			$table->text('exception', 65535);
			$table->timestamp('failed_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('laravel_failed_jobs');
	}

}
