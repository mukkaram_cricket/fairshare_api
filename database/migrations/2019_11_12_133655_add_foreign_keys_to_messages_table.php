<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('messages', function(Blueprint $table)
		{
			$table->foreign('thread_id', 'fk_thread_messages')->references('id')->on('message_threads')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('sender_id', 'fk_user_messages')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('messages', function(Blueprint $table)
		{
			$table->dropForeign('fk_thread_messages');
			$table->dropForeign('fk_user_messages');
		});
	}

}
