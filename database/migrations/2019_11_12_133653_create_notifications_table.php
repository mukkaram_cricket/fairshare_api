<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->bigInteger('user_id')->unsigned()->index('fk_user_id_notifications');
			$table->bigInteger('sender_id')->unsigned()->index('fk_sender_notifications');
			$table->string('title', 100);
			$table->string('description', 300);
			$table->enum('module', array('generals','messages','friends'))->default('generals');
			$table->string('type', 100);
			$table->bigInteger('related_id')->unsigned()->index('fk_related_id_notifications');
			$table->text('extra', 65535)->nullable();
			$table->boolean('is_viewed')->default(0);
			$table->boolean('is_read')->default(0);
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
