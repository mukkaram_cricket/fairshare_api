<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 60);
			$table->smallInteger('country_id')->unsigned()->index('fk_country_id_cities');
			$table->string('name_pt_PT', 60);
			$table->string('name_da_DK', 60);
			$table->string('name_du_NL', 60);
			$table->string('name_fr_FR', 60);
			$table->string('name_it_IT', 60);
			$table->string('name_ro_RO', 60);
			$table->string('name_tr_TR', 60);
			$table->string('name_ar_SA', 60);
			$table->string('name_zh_CN', 60);
			$table->string('name_ur_PK', 60);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
