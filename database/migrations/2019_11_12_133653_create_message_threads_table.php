<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageThreadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_threads', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->enum('type', array('conversation','group'));
			$table->bigInteger('user_id')->unsigned()->index('fk_user_id_msgthread');
			$table->string('subject')->nullable();
			$table->dateTime('date_created')->nullable();
			$table->dateTime('date_updated')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_threads');
	}

}
