<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessageRecipientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('message_recipients', function(Blueprint $table)
		{
			$table->foreign('thread_id', 'fk_thread_recipients')->references('id')->on('message_threads')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'fk_user_recipients')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('message_recipients', function(Blueprint $table)
		{
			$table->dropForeign('fk_thread_recipients');
			$table->dropForeign('fk_user_recipients');
		});
	}

}
