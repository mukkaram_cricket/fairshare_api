<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afghanistan',
                'code' => 'AF',
                'nationality' => 'Afghan',
                'language_id' => 88,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Albania',
                'code' => 'AL',
                'nationality' => 'Albanian',
                'language_id' => 106,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Algeria',
                'code' => 'DZ',
                'nationality' => 'Algerian',
                'language_id' => 6,
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'Andorra',
                'code' => 'AD',
                'nationality' => 'Andorran',
                'language_id' => 18,
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'Angola',
                'code' => 'AO',
                'nationality' => 'Angolan',
                'language_id' => 89,
            ),
            5 => 
            array (
                'id' => 7,
                'name' => 'Anguilla',
                'code' => 'AI',
                'nationality' => 'Anguillian',
                'language_id' => 30,
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'Antarctica',
                'code' => 'AQ',
                'nationality' => 'Antarctican',
                'language_id' => 30,
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'Antigua and Barbuda',
                'code' => 'AG',
                'nationality' => 'Antiguan / Barbudan',
                'language_id' => 30,
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'Argentina',
                'code' => 'AR',
                'nationality' => 'Argentine',
                'language_id' => 27,
            ),
            9 => 
            array (
                'id' => 11,
                'name' => 'Armenia',
                'code' => 'AM',
                'nationality' => 'Armenian',
                'language_id' => 45,
            ),
            10 => 
            array (
                'id' => 12,
                'name' => 'Aruba',
                'code' => 'AW',
                'nationality' => 'Aruban',
                'language_id' => 30,
            ),
            11 => 
            array (
                'id' => 13,
                'name' => 'Australia',
                'code' => 'AU',
                'nationality' => 'Australian',
                'language_id' => 30,
            ),
            12 => 
            array (
                'id' => 14,
                'name' => 'Austria',
                'code' => 'AT',
                'nationality' => 'Austrian',
                'language_id' => 23,
            ),
            13 => 
            array (
                'id' => 15,
                'name' => 'Azerbaijan',
                'code' => 'AZ',
                'nationality' => 'Azerbaijani / Azeri',
                'language_id' => 9,
            ),
            14 => 
            array (
                'id' => 16,
                'name' => 'Bahamas',
                'code' => 'BS',
                'nationality' => 'Bahamian',
                'language_id' => 30,
            ),
            15 => 
            array (
                'id' => 17,
                'name' => 'Bahrain',
                'code' => 'BH',
                'nationality' => 'Bahraini',
                'language_id' => 6,
            ),
            16 => 
            array (
                'id' => 18,
                'name' => 'Bangladesh',
                'code' => 'BD',
                'nationality' => 'Bangladeshi',
                'language_id' => 15,
            ),
            17 => 
            array (
                'id' => 19,
                'name' => 'Barbados',
                'code' => 'BB',
                'nationality' => 'Barbadian / Bajan',
                'language_id' => 30,
            ),
            18 => 
            array (
                'id' => 20,
                'name' => 'Belarus',
                'code' => 'BY',
                'nationality' => 'Belarusian',
                'language_id' => 94,
            ),
            19 => 
            array (
                'id' => 21,
                'name' => 'Belgium',
                'code' => 'BE',
                'nationality' => 'Belgian ',
                'language_id' => 82,
            ),
            20 => 
            array (
                'id' => 22,
                'name' => 'Belize',
                'code' => 'BZ',
                'nationality' => 'Belizean',
                'language_id' => 30,
            ),
            21 => 
            array (
                'id' => 23,
                'name' => 'Benin',
                'code' => 'BJ',
                'nationality' => 'Beninese',
                'language_id' => 34,
            ),
            22 => 
            array (
                'id' => 24,
                'name' => 'Bermuda',
                'code' => 'BM',
                'nationality' => 'Bermudian',
                'language_id' => 30,
            ),
            23 => 
            array (
                'id' => 25,
                'name' => 'Bhutan',
                'code' => 'BT',
                'nationality' => 'Bhutanese',
                'language_id' => 24,
            ),
            24 => 
            array (
                'id' => 26,
                'name' => 'Bolivia',
                'code' => 'BO',
                'nationality' => 'Bolivian',
                'language_id' => 8,
            ),
            25 => 
            array (
                'id' => 27,
                'name' => 'Bosnia and Herzegovina',
                'code' => 'BA',
                'nationality' => 'Bosniak/ Bosnian Croat/ Bosnian Serb',
                'language_id' => 43,
            ),
            26 => 
            array (
                'id' => 28,
                'name' => 'Botswana',
                'code' => 'BW',
                'nationality' => 'Motswana / Batswana',
                'language_id' => 30,
            ),
            27 => 
            array (
                'id' => 30,
                'name' => 'Brazil',
                'code' => 'BR',
                'nationality' => 'Brazilian',
                'language_id' => 89,
            ),
            28 => 
            array (
                'id' => 32,
                'name' => 'Brunei Darussalam',
                'code' => 'BN',
                'nationality' => 'Bruneian',
                'language_id' => 30,
            ),
            29 => 
            array (
                'id' => 33,
                'name' => 'Bulgaria',
                'code' => 'BG',
                'nationality' => 'Bulgarian',
                'language_id' => 12,
            ),
            30 => 
            array (
                'id' => 34,
                'name' => 'Burkina Faso',
                'code' => 'BF',
                'nationality' => 'Burkinabe',
                'language_id' => 34,
            ),
            31 => 
            array (
                'id' => 35,
                'name' => 'Burundi',
                'code' => 'BI',
                'nationality' => 'Burundian',
                'language_id' => 92,
            ),
            32 => 
            array (
                'id' => 36,
                'name' => 'Cambodia',
                'code' => 'KH',
                'nationality' => 'Cambodian',
                'language_id' => 30,
            ),
            33 => 
            array (
                'id' => 37,
                'name' => 'Cameroon',
                'code' => 'CM',
                'nationality' => 'Cameroonian',
                'language_id' => 30,
            ),
            34 => 
            array (
                'id' => 38,
                'name' => 'Canada',
                'code' => 'CA',
                'nationality' => 'Canadian',
                'language_id' => 30,
            ),
            35 => 
            array (
                'id' => 39,
                'name' => 'Cape Verde',
                'code' => 'CV',
                'nationality' => 'Cape Verdean',
                'language_id' => 89,
            ),
            36 => 
            array (
                'id' => 40,
                'name' => 'Cayman Islands',
                'code' => 'KY',
                'nationality' => 'Caymanian',
                'language_id' => 30,
            ),
            37 => 
            array (
                'id' => 41,
                'name' => 'Central African Republic',
                'code' => 'CF',
                'nationality' => 'Central African',
                'language_id' => 34,
            ),
            38 => 
            array (
                'id' => 42,
                'name' => 'Chad',
                'code' => 'TD',
                'nationality' => 'Chadian',
                'language_id' => 6,
            ),
            39 => 
            array (
                'id' => 43,
                'name' => 'Chile',
                'code' => 'CL',
                'nationality' => 'Chilean',
                'language_id' => 27,
            ),
            40 => 
            array (
                'id' => 44,
                'name' => 'China',
                'code' => 'CN',
                'nationality' => 'Chinese',
                'language_id' => 134,
            ),
            41 => 
            array (
                'id' => 45,
                'name' => 'Christmas Island',
                'code' => 'CX',
                'nationality' => 'Christmas Islander',
                'language_id' => 30,
            ),
            42 => 
            array (
                'id' => 46,
            'name' => 'Cocos (Keeling) Islands',
                'code' => 'CC',
                'nationality' => 'Cocos Islander',
                'language_id' => 30,
            ),
            43 => 
            array (
                'id' => 47,
                'name' => 'Colombia',
                'code' => 'CO',
                'nationality' => 'Colombian',
                'language_id' => 27,
            ),
            44 => 
            array (
                'id' => 48,
                'name' => 'Comoros',
                'code' => 'KM',
                'nationality' => 'Comorian',
                'language_id' => 6,
            ),
            45 => 
            array (
                'id' => 49,
                'name' => 'Congo',
                'code' => 'CG',
                'nationality' => 'Congolese',
                'language_id' => 66,
            ),
            46 => 
            array (
                'id' => 50,
                'name' => 'Cook Islands',
                'code' => 'CK',
                'nationality' => 'Cook Islander',
                'language_id' => 30,
            ),
            47 => 
            array (
                'id' => 51,
                'name' => 'Costa Rica',
                'code' => 'CR',
                'nationality' => 'Costa Rican',
                'language_id' => 27,
            ),
            48 => 
            array (
                'id' => 52,
            'name' => 'Croatia (Hrvatska)',
                'code' => 'HR',
                'nationality' => 'Croatian',
                'language_id' => 43,
            ),
            49 => 
            array (
                'id' => 53,
                'name' => 'Cuba',
                'code' => 'CU',
                'nationality' => 'Cuban',
                'language_id' => 27,
            ),
            50 => 
            array (
                'id' => 54,
                'name' => 'Cyprus',
                'code' => 'CY',
                'nationality' => 'Cypriot',
                'language_id' => 25,
            ),
            51 => 
            array (
                'id' => 55,
                'name' => 'Czech Republic',
                'code' => 'CZ',
                'nationality' => 'Czech',
                'language_id' => 20,
            ),
            52 => 
            array (
                'id' => 56,
                'name' => 'Denmark',
                'code' => 'DK',
                'nationality' => 'Danish/Dane',
                'language_id' => 22,
            ),
            53 => 
            array (
                'id' => 58,
                'name' => 'Dominica',
                'code' => 'DM',
                'nationality' => 'Dominican',
                'language_id' => 30,
            ),
            54 => 
            array (
                'id' => 59,
                'name' => 'Dominican Republic',
                'code' => 'DO',
                'nationality' => 'Dominican',
                'language_id' => 27,
            ),
            55 => 
            array (
                'id' => 61,
                'name' => 'Ecuador',
                'code' => 'EC',
                'nationality' => 'Ecuadorian',
                'language_id' => 27,
            ),
            56 => 
            array (
                'id' => 62,
                'name' => 'Egypt',
                'code' => 'EG',
                'nationality' => 'Egyptian',
                'language_id' => 6,
            ),
            57 => 
            array (
                'id' => 63,
                'name' => 'El Salvador',
                'code' => 'SV',
                'nationality' => 'Salvadoran',
                'language_id' => 27,
            ),
            58 => 
            array (
                'id' => 64,
                'name' => 'Equatorial Guinea',
                'code' => 'GQ',
                'nationality' => 'Equatorial Guinean/Equatoguinean',
                'language_id' => 27,
            ),
            59 => 
            array (
                'id' => 65,
                'name' => 'Eritrea',
                'code' => 'ER',
                'nationality' => 'Eritrean',
                'language_id' => 117,
            ),
            60 => 
            array (
                'id' => 66,
                'name' => 'Estonia',
                'code' => 'EE',
                'nationality' => 'Estonian',
                'language_id' => 28,
            ),
            61 => 
            array (
                'id' => 67,
                'name' => 'Ethiopia',
                'code' => 'ET',
                'nationality' => 'Ethiopian',
                'language_id' => 5,
            ),
            62 => 
            array (
                'id' => 68,
            'name' => 'Falkland Islands (Malvinas)',
                'code' => 'FK',
                'nationality' => 'British National of Falkland Islands',
                'language_id' => 30,
            ),
            63 => 
            array (
                'id' => 69,
                'name' => 'Faroe Islands',
                'code' => 'FO',
                'nationality' => 'Faroese',
                'language_id' => 30,
            ),
            64 => 
            array (
                'id' => 70,
                'name' => 'Fiji',
                'code' => 'FJ',
                'nationality' => 'Fiji Islander / Fijian',
                'language_id' => 32,
            ),
            65 => 
            array (
                'id' => 71,
                'name' => 'Finland',
                'code' => 'FI',
                'nationality' => 'Finnish',
                'language_id' => 31,
            ),
            66 => 
            array (
                'id' => 72,
                'name' => 'France',
                'code' => 'FR',
                'nationality' => 'French',
                'language_id' => 34,
            ),
            67 => 
            array (
                'id' => 74,
                'name' => 'French Guiana',
                'code' => 'GF',
                'nationality' => 'French Guianan / Guianan',
                'language_id' => 30,
            ),
            68 => 
            array (
                'id' => 75,
                'name' => 'French Polynesia',
                'code' => 'PF',
                'nationality' => 'French Polynesian',
                'language_id' => 30,
            ),
            69 => 
            array (
                'id' => 76,
                'name' => 'French Southern Territories',
                'code' => 'TF',
                'nationality' => 'French Southern Territories',
                'language_id' => 30,
            ),
            70 => 
            array (
                'id' => 77,
                'name' => 'Gabon',
                'code' => 'GA',
                'nationality' => 'Gabonese',
                'language_id' => 30,
            ),
            71 => 
            array (
                'id' => 78,
                'name' => 'Gambia',
                'code' => 'GM',
                'nationality' => 'Gambian',
                'language_id' => 30,
            ),
            72 => 
            array (
                'id' => 79,
                'name' => 'Georgia',
                'code' => 'GE',
                'nationality' => 'Georgian',
                'language_id' => 56,
            ),
            73 => 
            array (
                'id' => 80,
                'name' => 'Germany',
                'code' => 'DE',
                'nationality' => 'German',
                'language_id' => 23,
            ),
            74 => 
            array (
                'id' => 81,
                'name' => 'Ghana',
                'code' => 'GH',
                'nationality' => 'Ghanaian',
                'language_id' => 30,
            ),
            75 => 
            array (
                'id' => 82,
                'name' => 'Gibraltar',
                'code' => 'GI',
                'nationality' => ' Gibraltarian',
                'language_id' => 30,
            ),
            76 => 
            array (
                'id' => 84,
                'name' => 'Greece',
                'code' => 'GR',
                'nationality' => 'Greek',
                'language_id' => 25,
            ),
            77 => 
            array (
                'id' => 85,
                'name' => 'Greenland',
                'code' => 'GL',
                'nationality' => 'Greenlander / Greenlandic',
                'language_id' => 30,
            ),
            78 => 
            array (
                'id' => 86,
                'name' => 'Grenada',
                'code' => 'GD',
                'nationality' => 'Grenadian',
                'language_id' => 30,
            ),
            79 => 
            array (
                'id' => 87,
                'name' => 'Guadeloupe',
                'code' => 'GP',
                'nationality' => 'Guadeloupean',
                'language_id' => 30,
            ),
            80 => 
            array (
                'id' => 88,
                'name' => 'Guam',
                'code' => 'GU',
                'nationality' => 'Guamanian',
                'language_id' => 30,
            ),
            81 => 
            array (
                'id' => 89,
                'name' => 'Guatemala',
                'code' => 'GT',
                'nationality' => 'Guatemalan',
                'language_id' => 27,
            ),
            82 => 
            array (
                'id' => 90,
                'name' => 'Guinea',
                'code' => 'GN',
                'nationality' => 'Guinean',
                'language_id' => 34,
            ),
            83 => 
            array (
                'id' => 91,
                'name' => 'Guinea Bissau',
                'code' => 'GW',
                'nationality' => 'Guinean',
                'language_id' => 89,
            ),
            84 => 
            array (
                'id' => 92,
                'name' => 'Guyana',
                'code' => 'GY',
                'nationality' => 'Guyanese',
                'language_id' => 30,
            ),
            85 => 
            array (
                'id' => 93,
                'name' => 'Haiti',
                'code' => 'HT',
                'nationality' => 'Haitian',
                'language_id' => 34,
            ),
            86 => 
            array (
                'id' => 95,
                'name' => 'Honduras',
                'code' => 'HN',
                'nationality' => 'Honduran',
                'language_id' => 27,
            ),
            87 => 
            array (
                'id' => 96,
                'name' => 'Hong Kong',
                'code' => 'HK',
                'nationality' => 'Hongkonger/ Chinese',
                'language_id' => 134,
            ),
            88 => 
            array (
                'id' => 97,
                'name' => 'Hungary',
                'code' => 'HU',
                'nationality' => 'Hungarian',
                'language_id' => 44,
            ),
            89 => 
            array (
                'id' => 98,
                'name' => 'Iceland',
                'code' => 'IS',
                'nationality' => 'Icelander',
                'language_id' => 30,
            ),
            90 => 
            array (
                'id' => 99,
                'name' => 'India',
                'code' => 'IN',
                'nationality' => 'Indian',
                'language_id' => 42,
            ),
            91 => 
            array (
                'id' => 100,
                'name' => 'Isle of Man',
                'code' => 'IM',
                'nationality' => 'Manx',
                'language_id' => 30,
            ),
            92 => 
            array (
                'id' => 101,
                'name' => 'Indonesia',
                'code' => 'ID',
                'nationality' => 'Indonesian',
                'language_id' => 49,
            ),
            93 => 
            array (
                'id' => 102,
            'name' => 'Iran (Islamic Republic of)',
                'code' => 'IR',
                'nationality' => 'Iranian',
                'language_id' => 30,
            ),
            94 => 
            array (
                'id' => 103,
                'name' => 'Iraq',
                'code' => 'IQ',
                'nationality' => 'Iraqi',
                'language_id' => 6,
            ),
            95 => 
            array (
                'id' => 104,
                'name' => 'Ireland, Republic of',
                'code' => 'IE',
                'nationality' => 'Irishman / Irishwoman / Irish',
                'language_id' => 36,
            ),
            96 => 
            array (
                'id' => 105,
                'name' => 'Israel',
                'code' => 'IL',
                'nationality' => 'Israeli',
                'language_id' => 52,
            ),
            97 => 
            array (
                'id' => 106,
                'name' => 'Italy',
                'code' => 'IT',
                'nationality' => 'Italian',
                'language_id' => 51,
            ),
            98 => 
            array (
                'id' => 107,
                'name' => 'Ivory Coast',
                'code' => 'CI',
                'nationality' => 'Ivoirian',
                'language_id' => 34,
            ),
            99 => 
            array (
                'id' => 108,
                'name' => 'Jersey',
                'code' => 'JE',
                'nationality' => 'Channel Islander',
                'language_id' => 30,
            ),
            100 => 
            array (
                'id' => 109,
                'name' => 'Jamaica',
                'code' => 'JM',
                'nationality' => 'Jamaican',
                'language_id' => 30,
            ),
            101 => 
            array (
                'id' => 110,
                'name' => 'Japan',
                'code' => 'JP',
                'nationality' => 'Japanese',
                'language_id' => 53,
            ),
            102 => 
            array (
                'id' => 111,
                'name' => 'Jordan',
                'code' => 'JO',
                'nationality' => 'Jordanian',
                'language_id' => 6,
            ),
            103 => 
            array (
                'id' => 112,
                'name' => 'Kazakhstan',
                'code' => 'KZ',
                'nationality' => 'Kazakhstani',
                'language_id' => 57,
            ),
            104 => 
            array (
                'id' => 113,
                'name' => 'Kenya',
                'code' => 'KE',
                'nationality' => 'Kenyan',
                'language_id' => 112,
            ),
            105 => 
            array (
                'id' => 114,
                'name' => 'Kiribati',
                'code' => 'KI',
                'nationality' => 'Kiribati',
                'language_id' => 30,
            ),
            106 => 
            array (
                'id' => 115,
                'name' => 'Korea, Democratic People\'s Republic of',
                'code' => 'KP',
                'nationality' => 'North Korean / Korean',
                'language_id' => 61,
            ),
            107 => 
            array (
                'id' => 116,
                'name' => 'Korea, Republic of',
                'code' => 'KR',
                'nationality' => 'South Korean / Korean',
                'language_id' => 61,
            ),
            108 => 
            array (
                'id' => 117,
                'name' => 'Kosovo',
                'code' => 'XK',
                'nationality' => 'Kosovar / Kosovan',
                'language_id' => 106,
            ),
            109 => 
            array (
                'id' => 118,
                'name' => 'Kuwait',
                'code' => 'KW',
                'nationality' => 'Kuwaiti',
                'language_id' => 6,
            ),
            110 => 
            array (
                'id' => 119,
                'name' => 'Kyrgyzstan',
                'code' => 'KG',
                'nationality' => 'Kyrgyzstani',
                'language_id' => 94,
            ),
            111 => 
            array (
                'id' => 120,
                'name' => 'Laos',
                'code' => 'LA',
                'nationality' => 'Laotian',
                'language_id' => 30,
            ),
            112 => 
            array (
                'id' => 121,
                'name' => 'Latvia',
                'code' => 'LV',
                'nationality' => 'Latvian',
                'language_id' => 69,
            ),
            113 => 
            array (
                'id' => 122,
                'name' => 'Lebanon',
                'code' => 'LB',
                'nationality' => 'Lebanese',
                'language_id' => 6,
            ),
            114 => 
            array (
                'id' => 123,
                'name' => 'Lesotho',
                'code' => 'LS',
                'nationality' => 'Mosotho',
                'language_id' => 30,
            ),
            115 => 
            array (
                'id' => 124,
                'name' => 'Liberia',
                'code' => 'LR',
                'nationality' => 'Liberian',
                'language_id' => 30,
            ),
            116 => 
            array (
                'id' => 125,
                'name' => 'Libya',
                'code' => 'LY',
                'nationality' => 'Libyan',
                'language_id' => 6,
            ),
            117 => 
            array (
                'id' => 126,
                'name' => 'Liechtenstein',
                'code' => 'LI',
                'nationality' => 'Liechtensteiner',
                'language_id' => 23,
            ),
            118 => 
            array (
                'id' => 127,
                'name' => 'Lithuania',
                'code' => 'LT',
                'nationality' => 'Lithuanian',
                'language_id' => 68,
            ),
            119 => 
            array (
                'id' => 128,
                'name' => 'Luxembourg',
                'code' => 'LU',
                'nationality' => 'Luxembourger',
                'language_id' => 34,
            ),
            120 => 
            array (
                'id' => 129,
                'name' => 'Macau',
                'code' => 'MO',
                'nationality' => 'Macanese',
                'language_id' => 89,
            ),
            121 => 
            array (
                'id' => 130,
                'name' => 'Macedonia',
                'code' => 'MK',
                'nationality' => 'Macedonian',
                'language_id' => 72,
            ),
            122 => 
            array (
                'id' => 131,
                'name' => 'Madagascar',
                'code' => 'MG',
                'nationality' => 'Malagasy',
                'language_id' => 70,
            ),
            123 => 
            array (
                'id' => 132,
                'name' => 'Malawi',
                'code' => 'MW',
                'nationality' => 'Malawian',
                'language_id' => 30,
            ),
            124 => 
            array (
                'id' => 133,
                'name' => 'Malaysia',
                'code' => 'MY',
                'nationality' => 'Malaysian',
                'language_id' => 30,
            ),
            125 => 
            array (
                'id' => 134,
                'name' => 'Maldives',
                'code' => 'MV',
                'nationality' => 'Maldivian',
                'language_id' => 30,
            ),
            126 => 
            array (
                'id' => 135,
                'name' => 'Mali',
                'code' => 'ML',
                'nationality' => 'Malian',
                'language_id' => 34,
            ),
            127 => 
            array (
                'id' => 136,
                'name' => 'Malta',
                'code' => 'MT',
                'nationality' => 'Maltese',
                'language_id' => 78,
            ),
            128 => 
            array (
                'id' => 138,
                'name' => 'Martinique',
                'code' => 'MQ',
            'nationality' => 'Martinic(qu)an',
                'language_id' => 30,
            ),
            129 => 
            array (
                'id' => 139,
                'name' => 'Mauritania',
                'code' => 'MR',
                'nationality' => 'Mauritanian',
                'language_id' => 6,
            ),
            130 => 
            array (
                'id' => 140,
                'name' => 'Mauritius',
                'code' => 'MU',
                'nationality' => 'Mauritian',
                'language_id' => 30,
            ),
            131 => 
            array (
                'id' => 142,
                'name' => 'Mexico',
                'code' => 'MX',
                'nationality' => 'Mexican',
                'language_id' => 27,
            ),
            132 => 
            array (
                'id' => 144,
                'name' => 'Moldova, Republic of',
                'code' => 'MD',
                'nationality' => 'Moldovan',
                'language_id' => 93,
            ),
            133 => 
            array (
                'id' => 145,
                'name' => 'Monaco',
                'code' => 'MC',
                'nationality' => 'Monegasque',
                'language_id' => 34,
            ),
            134 => 
            array (
                'id' => 146,
                'name' => 'Mongolia',
                'code' => 'MN',
                'nationality' => 'Mongolian',
                'language_id' => 74,
            ),
            135 => 
            array (
                'id' => 147,
                'name' => 'Montenegro',
                'code' => 'ME',
                'nationality' => 'Montenegrin',
                'language_id' => 30,
            ),
            136 => 
            array (
                'id' => 148,
                'name' => 'Montserrat',
                'code' => 'MS',
                'nationality' => 'Montserratian',
                'language_id' => 30,
            ),
            137 => 
            array (
                'id' => 149,
                'name' => 'Morocco',
                'code' => 'MA',
                'nationality' => 'Moroccan',
                'language_id' => 6,
            ),
            138 => 
            array (
                'id' => 150,
                'name' => 'Mozambique',
                'code' => 'MZ',
                'nationality' => 'Mozambican',
                'language_id' => 89,
            ),
            139 => 
            array (
                'id' => 151,
                'name' => 'Myanmar',
                'code' => 'MM',
                'nationality' => 'Burmese / Myanmarese',
                'language_id' => 79,
            ),
            140 => 
            array (
                'id' => 152,
                'name' => 'Namibia',
                'code' => 'NA',
                'nationality' => 'Namibian',
                'language_id' => 4,
            ),
            141 => 
            array (
                'id' => 153,
                'name' => 'Nauru',
                'code' => 'NR',
                'nationality' => 'Nauruan',
                'language_id' => 30,
            ),
            142 => 
            array (
                'id' => 154,
                'name' => 'Nepal',
                'code' => 'NP',
                'nationality' => 'Nepali ',
                'language_id' => 81,
            ),
            143 => 
            array (
                'id' => 155,
                'name' => 'Netherlands',
                'code' => 'NL',
                'nationality' => 'Dutch',
                'language_id' => 82,
            ),
            144 => 
            array (
                'id' => 157,
                'name' => 'New Caledonia',
                'code' => 'NC',
                'nationality' => 'New Caledonian',
                'language_id' => 30,
            ),
            145 => 
            array (
                'id' => 158,
                'name' => 'New Zealand',
                'code' => 'NZ',
                'nationality' => 'New Zealander',
                'language_id' => 30,
            ),
            146 => 
            array (
                'id' => 159,
                'name' => 'Nicaragua',
                'code' => 'NI',
                'nationality' => 'Nicaraguan',
                'language_id' => 27,
            ),
            147 => 
            array (
                'id' => 160,
                'name' => 'Niger',
                'code' => 'NE',
                'nationality' => 'Nigerien',
                'language_id' => 34,
            ),
            148 => 
            array (
                'id' => 161,
                'name' => 'Nigeria',
                'code' => 'NG',
                'nationality' => 'Nigerian',
                'language_id' => 30,
            ),
            149 => 
            array (
                'id' => 162,
                'name' => 'Niue',
                'code' => 'NU',
                'nationality' => 'Niuean',
                'language_id' => 30,
            ),
            150 => 
            array (
                'id' => 163,
                'name' => 'Norfolk Island',
                'code' => 'NF',
                'nationality' => 'Norfolk Islander',
                'language_id' => 30,
            ),
            151 => 
            array (
                'id' => 164,
                'name' => 'Northern Mariana Islands',
                'code' => 'MP',
                'nationality' => 'Northern Mariana Islander',
                'language_id' => 30,
            ),
            152 => 
            array (
                'id' => 165,
                'name' => 'Norway',
                'code' => 'NO',
                'nationality' => 'Norwegian',
                'language_id' => 83,
            ),
            153 => 
            array (
                'id' => 166,
                'name' => 'Oman',
                'code' => 'OM',
                'nationality' => 'Omani',
                'language_id' => 30,
            ),
            154 => 
            array (
                'id' => 167,
                'name' => 'Pakistan',
                'code' => 'PK',
                'nationality' => 'Pakistani',
                'language_id' => 127,
            ),
            155 => 
            array (
                'id' => 169,
                'name' => 'Palestine',
                'code' => 'PS',
                'nationality' => 'Palestinian',
                'language_id' => 6,
            ),
            156 => 
            array (
                'id' => 170,
                'name' => 'Panama',
                'code' => 'PA',
                'nationality' => 'Panamanian',
                'language_id' => 27,
            ),
            157 => 
            array (
                'id' => 171,
                'name' => 'Papua New Guinea',
                'code' => 'PG',
                'nationality' => 'Papua New Guinean',
                'language_id' => 30,
            ),
            158 => 
            array (
                'id' => 172,
                'name' => 'Paraguay',
                'code' => 'PY',
                'nationality' => 'Paraguayan',
                'language_id' => 27,
            ),
            159 => 
            array (
                'id' => 173,
                'name' => 'Peru',
                'code' => 'PE',
                'nationality' => 'Peruvian',
                'language_id' => 27,
            ),
            160 => 
            array (
                'id' => 174,
                'name' => 'Philippines',
                'code' => 'PH',
                'nationality' => 'Filipino',
                'language_id' => 30,
            ),
            161 => 
            array (
                'id' => 175,
                'name' => 'Pitcairn, Islands',
                'code' => 'PN',
                'nationality' => 'Pitcairn Islander',
                'language_id' => 30,
            ),
            162 => 
            array (
                'id' => 176,
                'name' => 'Poland',
                'code' => 'PL',
                'nationality' => 'Pole / Polish',
                'language_id' => 87,
            ),
            163 => 
            array (
                'id' => 177,
                'name' => 'Portugal',
                'code' => 'PT',
                'nationality' => 'Portuguese',
                'language_id' => 89,
            ),
            164 => 
            array (
                'id' => 178,
                'name' => 'Puerto Rico',
                'code' => 'PR',
                'nationality' => 'Puerto Rican',
                'language_id' => 30,
            ),
            165 => 
            array (
                'id' => 179,
                'name' => 'Qatar',
                'code' => 'QA',
                'nationality' => 'Qatari',
                'language_id' => 6,
            ),
            166 => 
            array (
                'id' => 180,
                'name' => 'Reunion',
                'code' => 'RE',
                'nationality' => 'Réunionese',
                'language_id' => 30,
            ),
            167 => 
            array (
                'id' => 181,
                'name' => 'Romania',
                'code' => 'RO',
                'nationality' => 'Romanian',
                'language_id' => 93,
            ),
            168 => 
            array (
                'id' => 182,
                'name' => 'Russia',
                'code' => 'RU',
                'nationality' => 'Russian',
                'language_id' => 94,
            ),
            169 => 
            array (
                'id' => 183,
                'name' => 'Rwanda',
                'code' => 'RW',
                'nationality' => 'Rwandan',
                'language_id' => 30,
            ),
            170 => 
            array (
                'id' => 184,
                'name' => 'Saint Kitts and Nevis',
                'code' => 'KN',
                'nationality' => 'Kittitian / Nevisian',
                'language_id' => 30,
            ),
            171 => 
            array (
                'id' => 185,
                'name' => 'Saint Lucia',
                'code' => 'LC',
                'nationality' => 'St. Lucian',
                'language_id' => 30,
            ),
            172 => 
            array (
                'id' => 186,
                'name' => 'Saint Vincent and the Grenadines',
                'code' => 'VC',
                'nationality' => 'Vincentian',
                'language_id' => 30,
            ),
            173 => 
            array (
                'id' => 187,
                'name' => 'Samoa',
                'code' => 'WS',
                'nationality' => 'Samoan',
                'language_id' => 103,
            ),
            174 => 
            array (
                'id' => 188,
                'name' => 'San Marino',
                'code' => 'SM',
                'nationality' => 'Sammarinese',
                'language_id' => 51,
            ),
            175 => 
            array (
                'id' => 189,
                'name' => 'Sao Tome and Principe',
                'code' => 'ST',
                'nationality' => 'Sao Tomean',
                'language_id' => 89,
            ),
            176 => 
            array (
                'id' => 190,
                'name' => 'Saudi Arabia',
                'code' => 'SA',
                'nationality' => 'Saudi',
                'language_id' => 6,
            ),
            177 => 
            array (
                'id' => 191,
                'name' => 'Senegal',
                'code' => 'SN',
                'nationality' => 'Senegalese',
                'language_id' => 34,
            ),
            178 => 
            array (
                'id' => 192,
                'name' => 'Serbia',
                'code' => 'RS',
                'nationality' => 'Montenegrin / Serbian',
                'language_id' => 107,
            ),
            179 => 
            array (
                'id' => 193,
                'name' => 'Seychelles',
                'code' => 'SC',
                'nationality' => 'Seychellois',
                'language_id' => 30,
            ),
            180 => 
            array (
                'id' => 194,
                'name' => 'Sierra Leone',
                'code' => 'SL',
                'nationality' => 'Sierra Leonean',
                'language_id' => 30,
            ),
            181 => 
            array (
                'id' => 195,
                'name' => 'Singapore',
                'code' => 'SG',
                'nationality' => 'Singaporean',
                'language_id' => 77,
            ),
            182 => 
            array (
                'id' => 196,
                'name' => 'Slovakia',
                'code' => 'SK',
                'nationality' => 'Slovak',
                'language_id' => 101,
            ),
            183 => 
            array (
                'id' => 197,
                'name' => 'Slovenia',
                'code' => 'SI',
                'nationality' => 'Slovenian',
                'language_id' => 102,
            ),
            184 => 
            array (
                'id' => 198,
                'name' => 'Solomon Islands',
                'code' => 'SB',
                'nationality' => 'Solomon Islander',
                'language_id' => 30,
            ),
            185 => 
            array (
                'id' => 199,
                'name' => 'Somalia',
                'code' => 'SO',
                'nationality' => 'Somali',
                'language_id' => 105,
            ),
            186 => 
            array (
                'id' => 200,
                'name' => 'South Africa',
                'code' => 'ZA',
                'nationality' => 'South African',
                'language_id' => 4,
            ),
            187 => 
            array (
                'id' => 201,
                'name' => 'South Georgia South Sandwich Islands',
                'code' => 'GS',
                'nationality' => 'South Georgian
South Sandwich Islander',
                'language_id' => 30,
            ),
            188 => 
            array (
                'id' => 202,
                'name' => 'Spain',
                'code' => 'ES',
                'nationality' => 'Spaniard / Spanish',
                'language_id' => 27,
            ),
            189 => 
            array (
                'id' => 203,
                'name' => 'Sri Lanka',
                'code' => 'LK',
                'nationality' => 'Sri Lankan',
                'language_id' => 113,
            ),
            190 => 
            array (
                'id' => 205,
                'name' => 'Saint Pierre and Miquelon',
                'code' => 'PM',
                'nationality' => 'Saint-Pierrais /Miquelonnais/French',
                'language_id' => 30,
            ),
            191 => 
            array (
                'id' => 206,
                'name' => 'Sudan',
                'code' => 'SD',
                'nationality' => 'Sudanese',
                'language_id' => 6,
            ),
            192 => 
            array (
                'id' => 207,
                'name' => 'Suriname',
                'code' => 'SR',
                'nationality' => 'Surinamer',
                'language_id' => 82,
            ),
            193 => 
            array (
                'id' => 208,
                'name' => 'Svalbard and Jan Mayen Islands',
                'code' => 'SJ',
                'nationality' => 'Svalbard and Jan Mayen Islands',
                'language_id' => 30,
            ),
            194 => 
            array (
                'id' => 209,
                'name' => 'Swaziland',
                'code' => 'SZ',
                'nationality' => 'Swazi',
                'language_id' => 30,
            ),
            195 => 
            array (
                'id' => 210,
                'name' => 'Sweden',
                'code' => 'SE',
                'nationality' => 'Swedish',
                'language_id' => 111,
            ),
            196 => 
            array (
                'id' => 211,
                'name' => 'Switzerland',
                'code' => 'CH',
                'nationality' => 'Swiss',
                'language_id' => 34,
            ),
            197 => 
            array (
                'id' => 212,
                'name' => 'Syria',
                'code' => 'SY',
                'nationality' => 'Syrian',
                'language_id' => 6,
            ),
            198 => 
            array (
                'id' => 213,
                'name' => 'Taiwan',
                'code' => 'TW',
                'nationality' => 'Taiwanese',
                'language_id' => 134,
            ),
            199 => 
            array (
                'id' => 214,
                'name' => 'Tajikistan',
                'code' => 'TJ',
                'nationality' => 'Tajikistani',
                'language_id' => 115,
            ),
            200 => 
            array (
                'id' => 215,
                'name' => 'Tanzania, United Republic of',
                'code' => 'TZ',
                'nationality' => 'Tanzanian/Zanzibari',
                'language_id' => 112,
            ),
            201 => 
            array (
                'id' => 216,
                'name' => 'Thailand',
                'code' => 'TH',
                'nationality' => 'Thai',
                'language_id' => 116,
            ),
            202 => 
            array (
                'id' => 217,
                'name' => 'Togo',
                'code' => 'TG',
                'nationality' => 'Togolese',
                'language_id' => 34,
            ),
            203 => 
            array (
                'id' => 218,
                'name' => 'Tokelau',
                'code' => 'TK',
                'nationality' => 'Tokelauan',
                'language_id' => 30,
            ),
            204 => 
            array (
                'id' => 219,
                'name' => 'Tonga',
                'code' => 'TO',
                'nationality' => 'Tongan',
                'language_id' => 121,
            ),
            205 => 
            array (
                'id' => 220,
                'name' => 'Trinidad and Tobago',
                'code' => 'TT',
                'nationality' => 'Trinidadian / Tobagonian',
                'language_id' => 30,
            ),
            206 => 
            array (
                'id' => 221,
                'name' => 'Tunisia',
                'code' => 'TN',
                'nationality' => 'Tunisian',
                'language_id' => 6,
            ),
            207 => 
            array (
                'id' => 222,
                'name' => 'Turkey',
                'code' => 'TR',
                'nationality' => 'Turkish',
                'language_id' => 122,
            ),
            208 => 
            array (
                'id' => 223,
                'name' => 'Turkmenistan',
                'code' => 'TM',
                'nationality' => 'Turkmen',
                'language_id' => 118,
            ),
            209 => 
            array (
                'id' => 224,
                'name' => 'Turks and Caicos Islands',
                'code' => 'TC',
                'nationality' => 'Turks and Caicos Islander',
                'language_id' => 30,
            ),
            210 => 
            array (
                'id' => 225,
                'name' => 'Tuvalu',
                'code' => 'TV',
                'nationality' => 'Tuvaluan',
                'language_id' => 30,
            ),
            211 => 
            array (
                'id' => 226,
                'name' => 'Uganda',
                'code' => 'UG',
                'nationality' => 'Ugandan',
                'language_id' => 30,
            ),
            212 => 
            array (
                'id' => 227,
                'name' => 'Ukraine',
                'code' => 'UA',
                'nationality' => 'Ukrainian',
                'language_id' => 126,
            ),
            213 => 
            array (
                'id' => 228,
                'name' => 'United Arab Emirates',
                'code' => 'AE',
                'nationality' => 'Emirati',
                'language_id' => 6,
            ),
            214 => 
            array (
                'id' => 229,
                'name' => 'United Kingdom',
                'code' => 'GB',
                'nationality' => 'British',
                'language_id' => 30,
            ),
            215 => 
            array (
                'id' => 230,
                'name' => 'United States of America',
                'code' => 'US',
                'nationality' => 'American',
                'language_id' => 30,
            ),
            216 => 
            array (
                'id' => 232,
                'name' => 'Uruguay',
                'code' => 'UY',
                'nationality' => 'Uruguayan',
                'language_id' => 27,
            ),
            217 => 
            array (
                'id' => 233,
                'name' => 'Uzbekistan',
                'code' => 'UZ',
                'nationality' => 'Uzbek',
                'language_id' => 128,
            ),
            218 => 
            array (
                'id' => 234,
                'name' => 'Vanuatu',
                'code' => 'VU',
                'nationality' => 'Ni-Vanuatu',
                'language_id' => 14,
            ),
            219 => 
            array (
                'id' => 236,
                'name' => 'Venezuela',
                'code' => 'VE',
                'nationality' => 'Venezuelan',
                'language_id' => 27,
            ),
            220 => 
            array (
                'id' => 237,
                'name' => 'Vietnam',
                'code' => 'VN',
                'nationality' => 'Vietnamese',
                'language_id' => 129,
            ),
            221 => 
            array (
                'id' => 238,
            'name' => 'Virgin Islands (British)',
                'code' => 'VG',
                'nationality' => 'Virgin Islander',
                'language_id' => 30,
            ),
            222 => 
            array (
                'id' => 239,
            'name' => 'Virgin Islands (U.S.)',
                'code' => 'VI',
                'nationality' => 'Virgin Islander',
                'language_id' => 30,
            ),
            223 => 
            array (
                'id' => 240,
                'name' => 'Wallis and Futuna Islands',
                'code' => 'WF',
                'nationality' => 'Wallisian/Futunan',
                'language_id' => 30,
            ),
            224 => 
            array (
                'id' => 242,
                'name' => 'Yemen',
                'code' => 'YE',
                'nationality' => 'Yemeni / Yemenite',
                'language_id' => 6,
            ),
            225 => 
            array (
                'id' => 244,
                'name' => 'Zambia',
                'code' => 'ZM',
                'nationality' => 'Zambian',
                'language_id' => 30,
            ),
            226 => 
            array (
                'id' => 245,
                'name' => 'Zimbabwe',
                'code' => 'ZW',
                'nationality' => 'Zimbabwean',
                'language_id' => 30,
            ),
        ));
        
        
    }
}