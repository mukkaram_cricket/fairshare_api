<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        foreach (range(1,5) as $index) {
            $title = $faker->name;
            $slug = str_slug($title, '-');

            Product::create([
                'name' => $title,
                'slug' => $slug,
                'brand_id' => 1,
                'category_id' => 10,
                'actual_price' => $faker->randomNumber(2),
                'status' => 1,
            ]);

        }

    }
}
