<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
            'name' => 'Trustpilot',
            'status' => 1,
            'slug' => 'trustpilot'
        ]);
        Brand::create([
            'name' => 'Elgiganten',
            'status' => 1,
            'slug' => 'elgiganten'
        ]);
        Brand::create([
            'name' => 'Proshop',
            'status' => 1,
            'slug' => 'proshop',
        ]);
        Brand::create([
            'name' => 'QXL',
            'status' => 1,
            'slug' => 'qxl',
        ]);
    }
}
