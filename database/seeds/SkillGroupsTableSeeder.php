<?php

use Illuminate\Database\Seeder;

class SkillGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('skill_groups')->delete();
        
        \DB::table('skill_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Technical',
                'sport_id' => 1,
                'date_created' => '2018-04-10 15:22:22',
                'date_updated' => '2018-04-10 15:22:22',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Mental',
                'sport_id' => 1,
                'date_created' => '2018-04-10 15:22:22',
                'date_updated' => '2018-04-10 15:22:22',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Physical',
                'sport_id' => 1,
                'date_created' => '2018-04-10 15:22:22',
                'date_updated' => '2018-04-10 15:22:22',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Skills',
                'sport_id' => 2,
                'date_created' => '2018-04-10 15:22:22',
                'date_updated' => '2018-04-10 15:22:22',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Batting',
                'sport_id' => 3,
                'date_created' => '2018-05-25 16:45:52',
                'date_updated' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Fast bowling',
                'sport_id' => 3,
                'date_created' => '2018-05-25 16:45:54',
                'date_updated' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Spin bowling',
                'sport_id' => 3,
                'date_created' => '2018-05-25 16:45:56',
                'date_updated' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Fielding',
                'sport_id' => 3,
                'date_created' => '2018-05-25 16:45:57',
                'date_updated' => NULL,
            ),
        ));
        
        
    }
}