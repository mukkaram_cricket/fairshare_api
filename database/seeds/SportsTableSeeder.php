<?php

use Illuminate\Database\Seeder;

class SportsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sports')->delete();
        
        \DB::table('sports')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Football',
                'status' => 'active',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Handball',
                'status' => 'active',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cricket',
                'status' => 'active',
            ),
        ));
        
        
    }
}