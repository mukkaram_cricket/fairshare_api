<?php

use Illuminate\Database\Seeder;

class AutocompleteTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('autocomplete')->delete();
        
        \DB::table('autocomplete')->insert(array (
            0 => 
            array (
                'id' => 4,
                'name' => 'PSL',
                'image' => NULL,
                'type' => 'tournament',
            ),
            1 => 
            array (
                'id' => 6,
                'name' => 'PSL 2018',
                'image' => NULL,
                'type' => 'tournament',
            ),
            2 => 
            array (
                'id' => 9,
                'name' => 'BPL Tournament',
                'image' => NULL,
                'type' => 'tournament',
            ),
            3 => 
            array (
                'id' => 10,
                'name' => 'Pakistan Super League',
                'image' => NULL,
                'type' => 'tournament',
            ),
            4 => 
            array (
                'id' => 11,
                'name' => 'PSL 2019',
                'image' => NULL,
                'type' => 'tournament',
            ),
            5 => 
            array (
                'id' => 12,
                'name' => 'Exhausted',
                'image' => 'FeelingActivity/Exhausted.png',
                'type' => 'feeling_activity',
            ),
            6 => 
            array (
                'id' => 13,
                'name' => 'Happy',
                'image' => 'FeelingActivity/Happy.png',
                'type' => 'feeling_activity',
            ),
            7 => 
            array (
                'id' => 14,
                'name' => 'Invincible',
                'image' => 'FeelingActivity/Invincible.png',
                'type' => 'feeling_activity',
            ),
            8 => 
            array (
                'id' => 15,
                'name' => 'Angry',
                'image' => 'FeelingActivity/Angry.png',
                'type' => 'feeling_activity',
            ),
            9 => 
            array (
                'id' => 16,
                'name' => 'Sick',
                'image' => 'FeelingActivity/Sick.png',
                'type' => 'feeling_activity',
            ),
        ));
        
        
    }
}