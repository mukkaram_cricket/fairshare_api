<?php

use Illuminate\Database\Seeder;

class FtagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ftags')->delete();
        
        \DB::table('ftags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'match',
                'name' => 'Match',
                'date_created' => '2017-12-18 18:00:31',
                'date_updated' => '2018-03-15 19:23:47',
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'training',
                'name' => 'Training',
                'date_created' => '2018-03-15 11:19:44',
                'date_updated' => '2018-03-15 11:19:44',
            ),
            2 => 
            array (
                'id' => 3,
                'slug' => 'goal',
                'name' => 'Goal',
                'date_created' => '2018-03-15 11:19:50',
                'date_updated' => '2018-03-15 11:19:50',
            ),
            3 => 
            array (
                'id' => 4,
                'slug' => 'pass',
                'name' => 'Pass',
                'date_created' => '2018-03-15 11:19:55',
                'date_updated' => '2018-03-15 11:19:55',
            ),
            4 => 
            array (
                'id' => 5,
                'slug' => 'defending',
                'name' => 'Defending',
                'date_created' => '2018-03-15 11:20:07',
                'date_updated' => '2018-03-15 11:20:07',
            ),
            5 => 
            array (
                'id' => 6,
                'slug' => 'cross',
                'name' => 'Cross',
                'date_created' => '2018-03-15 11:20:16',
                'date_updated' => '2018-03-15 11:20:16',
            ),
            6 => 
            array (
                'id' => 7,
                'slug' => 'teamwork',
                'name' => 'Teamwork',
                'date_created' => '2018-03-15 11:20:26',
                'date_updated' => '2018-03-15 11:20:26',
            ),
            7 => 
            array (
                'id' => 8,
                'slug' => 'attacking',
                'name' => 'Attacking',
                'date_created' => '2018-03-15 11:20:35',
                'date_updated' => '2018-03-15 11:20:35',
            ),
            8 => 
            array (
                'id' => 9,
                'slug' => 'heading',
                'name' => 'Heading',
                'date_created' => '2018-03-15 11:20:56',
                'date_updated' => '2018-03-15 11:20:56',
            ),
            9 => 
            array (
                'id' => 10,
                'slug' => 'general',
                'name' => 'General',
                'date_created' => '2018-03-15 11:21:04',
                'date_updated' => '2018-03-15 11:21:04',
            ),
            10 => 
            array (
                'id' => 11,
                'slug' => 'finishing',
                'name' => 'Finishing',
                'date_created' => '2018-03-15 11:21:22',
                'date_updated' => '2018-03-15 11:21:22',
            ),
            11 => 
            array (
                'id' => 12,
                'slug' => 'free-kick',
                'name' => 'Free-Kick',
                'date_created' => '2018-03-15 11:21:33',
                'date_updated' => '2018-03-15 11:21:33',
            ),
            12 => 
            array (
                'id' => 13,
                'slug' => 'dribbling',
                'name' => 'Dribbling',
                'date_created' => '2018-03-15 11:21:51',
                'date_updated' => '2018-03-15 11:21:51',
            ),
            13 => 
            array (
                'id' => 14,
                'slug' => 'acceleration',
                'name' => 'Acceleration',
                'date_created' => '2018-03-15 11:22:27',
                'date_updated' => '2018-03-15 11:22:27',
            ),
            14 => 
            array (
                'id' => 15,
                'slug' => 'corners',
                'name' => 'Corners',
                'date_created' => '2018-03-15 11:22:35',
                'date_updated' => '2018-03-15 11:22:35',
            ),
            15 => 
            array (
                'id' => 16,
                'slug' => 'tackling',
                'name' => 'Tackling',
                'date_created' => '2018-03-15 11:28:35',
                'date_updated' => '2018-03-15 11:28:35',
            ),
        ));
        
        
    }
}