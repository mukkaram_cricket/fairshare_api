<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('skills')->delete();
        
        \DB::table('skills')->insert(array (
            0 => 
            array (
                'id' => 8,
                'name' => 'Corners',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            1 => 
            array (
                'id' => 9,
                'name' => 'Crossing',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            2 => 
            array (
                'id' => 10,
                'name' => 'Dribbling',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            3 => 
            array (
                'id' => 11,
                'name' => 'Finishing',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            4 => 
            array (
                'id' => 12,
                'name' => 'First Touch',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            5 => 
            array (
                'id' => 13,
                'name' => 'Free Kick Taking',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            6 => 
            array (
                'id' => 14,
                'name' => 'Heading',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            7 => 
            array (
                'id' => 15,
                'name' => 'Long Shots',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            8 => 
            array (
                'id' => 16,
                'name' => 'Long Throws',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            9 => 
            array (
                'id' => 17,
                'name' => 'Marking',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            10 => 
            array (
                'id' => 18,
                'name' => 'Passing',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            11 => 
            array (
                'id' => 19,
                'name' => 'Penalty Taking',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            12 => 
            array (
                'id' => 20,
                'name' => 'Tackling',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            13 => 
            array (
                'id' => 21,
                'name' => 'Technique',
                'group_id' => 1,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            14 => 
            array (
                'id' => 22,
                'name' => 'Aggression',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            15 => 
            array (
                'id' => 23,
                'name' => 'Anticipation',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            16 => 
            array (
                'id' => 24,
                'name' => 'Bravery',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            17 => 
            array (
                'id' => 25,
                'name' => 'Composure',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            18 => 
            array (
                'id' => 26,
                'name' => 'Concentration',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            19 => 
            array (
                'id' => 27,
                'name' => 'Decisions',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            20 => 
            array (
                'id' => 28,
                'name' => 'Determination',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            21 => 
            array (
                'id' => 29,
                'name' => 'Flair',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            22 => 
            array (
                'id' => 30,
                'name' => 'Leadership',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            23 => 
            array (
                'id' => 31,
                'name' => 'Off The Ball',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            24 => 
            array (
                'id' => 32,
                'name' => 'Positioning',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            25 => 
            array (
                'id' => 33,
                'name' => 'Teamwork',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            26 => 
            array (
                'id' => 34,
                'name' => 'Vision',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            27 => 
            array (
                'id' => 35,
                'name' => 'Work Rate',
                'group_id' => 2,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            28 => 
            array (
                'id' => 36,
                'name' => 'Acceleration',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            29 => 
            array (
                'id' => 37,
                'name' => 'Agility',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            30 => 
            array (
                'id' => 38,
                'name' => 'Balance',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            31 => 
            array (
                'id' => 39,
                'name' => 'Jumping',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            32 => 
            array (
                'id' => 40,
                'name' => 'Natural Fitness',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            33 => 
            array (
                'id' => 41,
                'name' => 'Pace',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            34 => 
            array (
                'id' => 42,
                'name' => 'Stamina',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            35 => 
            array (
                'id' => 43,
                'name' => 'Strength',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            36 => 
            array (
                'id' => 44,
                'name' => 'Goalkeeper Rating',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            37 => 
            array (
                'id' => 45,
                'name' => 'Discipline',
                'group_id' => 3,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            38 => 
            array (
                'id' => 46,
                'name' => 'Ball control',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            39 => 
            array (
                'id' => 47,
                'name' => 'Passing',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            40 => 
            array (
                'id' => 48,
                'name' => 'Shooting',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            41 => 
            array (
                'id' => 49,
                'name' => 'Off the ball',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            42 => 
            array (
                'id' => 50,
                'name' => 'Technique',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            43 => 
            array (
                'id' => 51,
                'name' => 'Playmaking',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            44 => 
            array (
                'id' => 52,
                'name' => 'Marking',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            45 => 
            array (
                'id' => 53,
                'name' => 'Defence',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            46 => 
            array (
                'id' => 54,
                'name' => 'Blocking',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            47 => 
            array (
                'id' => 55,
                'name' => 'Reflexes',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            48 => 
            array (
                'id' => 56,
                'name' => 'One by one',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            49 => 
            array (
                'id' => 57,
                'name' => 'Agility',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            50 => 
            array (
                'id' => 58,
                'name' => 'Speed',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            51 => 
            array (
                'id' => 59,
                'name' => 'Strength',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            52 => 
            array (
                'id' => 60,
                'name' => 'Jumping',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            53 => 
            array (
                'id' => 61,
                'name' => 'Stamina',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            54 => 
            array (
                'id' => 62,
                'name' => 'Aggression',
                'group_id' => 4,
                'date_created' => '2018-04-10 15:22:49',
                'date_updated' => '2018-04-10 15:22:49',
            ),
            55 => 
            array (
                'id' => 63,
                'name' => 'Block',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            56 => 
            array (
                'id' => 64,
                'name' => 'Cut',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            57 => 
            array (
                'id' => 65,
                'name' => 'Drive',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            58 => 
            array (
                'id' => 66,
                'name' => 'Hook',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            59 => 
            array (
                'id' => 67,
                'name' => 'Leg Glance',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            60 => 
            array (
                'id' => 68,
                'name' => 'Paddle Sweep',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            61 => 
            array (
                'id' => 69,
                'name' => 'Pull',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            62 => 
            array (
                'id' => 70,
                'name' => 'Sweep',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            63 => 
            array (
                'id' => 71,
                'name' => 'Reverse Sweep',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            64 => 
            array (
                'id' => 72,
                'name' => 'Slog Sweep',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            65 => 
            array (
                'id' => 73,
                'name' => 'Slog',
                'group_id' => 5,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            66 => 
            array (
                'id' => 74,
                'name' => 'Seam Bowling',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            67 => 
            array (
                'id' => 75,
                'name' => 'Swing Bowling',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            68 => 
            array (
                'id' => 76,
                'name' => 'Bouncer',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            69 => 
            array (
                'id' => 77,
                'name' => 'In dipper',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            70 => 
            array (
                'id' => 78,
                'name' => 'In swinger',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            71 => 
            array (
                'id' => 79,
                'name' => 'Leg Cutter',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            72 => 
            array (
                'id' => 80,
                'name' => 'Off Cutter',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            73 => 
            array (
                'id' => 81,
                'name' => 'Slow Ball',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            74 => 
            array (
                'id' => 82,
                'name' => 'Reverse',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            75 => 
            array (
                'id' => 83,
                'name' => 'Full Toss',
                'group_id' => 6,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            76 => 
            array (
                'id' => 84,
                'name' => 'Off-Spin',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            77 => 
            array (
                'id' => 85,
                'name' => 'Leg-Spin',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            78 => 
            array (
                'id' => 86,
                'name' => 'Chinaman',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            79 => 
            array (
                'id' => 87,
                'name' => 'Doosra',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            80 => 
            array (
                'id' => 88,
                'name' => 'Googles',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            81 => 
            array (
                'id' => 89,
                'name' => 'Leg Break',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            82 => 
            array (
                'id' => 90,
                'name' => 'Teesra',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            83 => 
            array (
                'id' => 91,
                'name' => 'Arm Ball',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            84 => 
            array (
                'id' => 92,
                'name' => 'Slips',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            85 => 
            array (
                'id' => 93,
                'name' => 'Gully',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            86 => 
            array (
                'id' => 94,
                'name' => 'Point',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            87 => 
            array (
                'id' => 95,
                'name' => 'Cover',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            88 => 
            array (
                'id' => 96,
                'name' => 'Mid Off',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            89 => 
            array (
                'id' => 97,
                'name' => 'Mid On',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            90 => 
            array (
                'id' => 98,
                'name' => 'Mid Wicket',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            91 => 
            array (
                'id' => 99,
                'name' => 'Square Leg',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            92 => 
            array (
                'id' => 100,
                'name' => 'Fine Leg',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            93 => 
            array (
                'id' => 101,
                'name' => 'Third man',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            94 => 
            array (
                'id' => 102,
                'name' => 'Deep Backward',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            95 => 
            array (
                'id' => 103,
                'name' => 'Deep Cover Point',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            96 => 
            array (
                'id' => 104,
                'name' => 'Long Off',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            97 => 
            array (
                'id' => 105,
                'name' => 'Long On',
                'group_id' => 8,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
            98 => 
            array (
                'id' => 106,
                'name' => 'Flipper',
                'group_id' => 7,
                'date_created' => NULL,
                'date_updated' => NULL,
            ),
        ));
        
        
    }
}