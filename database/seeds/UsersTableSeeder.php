<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'NEROc0E2K2ljeDVLdzdIeTRVQkpIZkFmKzRxVWhkdlBsVWhrWUd3WFNEZz0=',
                'name' => 'Support',
                'slug' => 'player_match_support',
                'avatar' => '1526033522594.png',
                'password' => '$2y$10$UZu6ceptWogumM0vif22LuduFegOMxxkfgySJSW0kUGL9VipBfYUy',
                'activation_key' => NULL,
                'sport_id' => 1,
                'role_id' => 3,
                'in_panel' => 0,
                'rated_by' => 0,
                'country_id' => 167,
                'phone' => '',
                'is_email_verified' => 1,
                'is_phone_verified' => 0,
                'locale' => 'en_US',
                'activity_privacy' => 'public',
                'is_pm_admin' => 1,
                'register_date' => '2016-06-23 10:19:29',
                'status' => 'Active',
                'date_created' => '2016-06-23 10:19:29',
                'date_updated' => '2019-04-15 06:09:01',
            )
        ));
        
        
    }
}