<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Living',
            'status' => 1,
            'slug' => 'living',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Clothes',
            'status' => 1,
            'slug' => 'clothes',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Electronic',
            'status' => 1,
            'slug' => 'electronic',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Experiences',
            'status' => 1,
            'slug' => 'experiences',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Kids',
            'status' => 1,
            'slug' => 'kids',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Others',
            'status' => 1,
            'slug' => 'others',
            'parent_id' => 0
        ]);
        Category::create([
            'name' => 'Furniture',
            'status' => 1,
            'slug' => 'furniture',
            'parent_id' => 1
        ]);
        Category::create([
            'name' => 'Table',
            'status' => 1,
            'slug' => 'table',
            'parent_id' => 1
        ]);
        Category::create([
            'name' => 'Kitchen',
            'status' => 1,
            'slug' => 'kitchen',
            'parent_id' => 1
        ]);
        Category::create([
            'name' => 'Jeans',
            'status' => 1,
            'slug' => 'jeans',
            'parent_id' => 2
        ]);
    }
}
