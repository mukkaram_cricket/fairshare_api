<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
//        $this->call(FtagsTableSeeder::class);
//        $this->call(LanguagesTableSeeder::class);
//        $this->call(CountriesTableSeeder::class);
//        $this->call(SportsTableSeeder::class);
//        $this->call(RolesTableSeeder::class);
//        $this->call(FormFieldsTableSeeder::class);
//        $this->call(FieldOptionsTableSeeder::class);
//        $this->call(SkillGroupsTableSeeder::class);
//        $this->call(SkillsTableSeeder::class);
//        $this->call(AutocompleteTableSeeder::class);
//        $this->call(CitiesTableSeeder::class);
        $this->call(BrandTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
    }
}
