<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('languages')->delete();
        
        \DB::table('languages')->insert(array (
            0 => 
            array (
                'id' => 1,
            'name' => '(Afan)/Oromoor/Oriya',
                'code' => 'om',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Abkhazian',
                'code' => 'ab',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Afar',
                'code' => 'aa',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Afrikaans',
                'code' => 'af',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Albanian',
                'code' => 'sq',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Amharic',
                'code' => 'am',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Arabic',
                'code' => 'ar',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Armenian',
                'code' => 'hy',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Assamese',
                'code' => 'as',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Aymara',
                'code' => 'ay',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Azerbaijani',
                'code' => 'az',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Bashkir',
                'code' => 'ba',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Basque',
                'code' => 'eu',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Belarusian',
                'code' => 'be',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Bengali/Bangla',
                'code' => 'bn',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Bhutani',
                'code' => 'dz',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Bihari',
                'code' => 'bh',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bislama',
                'code' => 'bi',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Breton',
                'code' => 'br',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Bulgarian',
                'code' => 'bg',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Burmese',
                'code' => 'my',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Cambodian',
                'code' => 'km',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Catalan',
                'code' => 'ca',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Chinese',
                'code' => 'zh',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Corsican',
                'code' => 'co',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Croatian',
                'code' => 'hr',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Czech',
                'code' => 'cs',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Danish',
                'code' => 'da',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Dutch',
                'code' => 'nl',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'English',
                'code' => 'en',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Esperanto',
                'code' => 'eo',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Estonian',
                'code' => 'et',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Faeroese',
                'code' => 'fo',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Fiji',
                'code' => 'fj',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Finnish',
                'code' => 'fi',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'French',
                'code' => 'fr',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Frisian',
                'code' => 'fy',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Galician',
                'code' => 'gl',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Georgian',
                'code' => 'ka',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'German',
                'code' => 'de',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Greek',
                'code' => 'el',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Greenlandic',
                'code' => 'kl',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Guarani',
                'code' => 'gn',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Gujarati',
                'code' => 'gu',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Hausa',
                'code' => 'ha',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Hebrew',
                'code' => 'iw',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Hindi',
                'code' => 'hi',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Hungarian',
                'code' => 'hu',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Icelandic',
                'code' => 'is',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Igbo',
                'code' => 'ig',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Indonesian',
                'code' => 'in',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Interlingua',
                'code' => 'ia',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Interlingue',
                'code' => 'ie',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Inupiak',
                'code' => 'ik',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Irish',
                'code' => 'ga',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Italian',
                'code' => 'it',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Japanese',
                'code' => 'ja',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Javanese',
                'code' => 'jw',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Kannada',
                'code' => 'kn',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Kashmiri',
                'code' => 'ks',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Kazakh',
                'code' => 'kk',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Kinyarwanda',
                'code' => 'rw',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Kirghiz',
                'code' => 'ky',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Kirundi',
                'code' => 'rn',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Korean',
                'code' => 'ko',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Kurdish',
                'code' => 'ku',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Laothian',
                'code' => 'lo',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Latin',
                'code' => 'la',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Latvian/Lettish',
                'code' => 'lv',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Lingala',
                'code' => 'ln',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Lithuanian',
                'code' => 'lt',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Macedonian',
                'code' => 'mk',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Malagasy',
                'code' => 'mg',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Malay',
                'code' => 'ms',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Malayalam',
                'code' => 'ml',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Maltese',
                'code' => 'mt',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Maori',
                'code' => 'mi',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Marathi',
                'code' => 'mr',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Moldavian',
                'code' => 'mo',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Mongolian',
                'code' => 'mn',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Nauru',
                'code' => 'na',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Nepali',
                'code' => 'ne',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Norwegian',
                'code' => 'no',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Occitan',
                'code' => 'oc',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Pashto/Pushto',
                'code' => 'ps',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Persian',
                'code' => 'fa',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Polish',
                'code' => 'pl',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Portuguese',
                'code' => 'pt',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Punjabi',
                'code' => 'pa',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Quechua',
                'code' => 'qu',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Rhaeto-Romance',
                'code' => 'rm',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Romanian',
                'code' => 'ro',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Russian',
                'code' => 'ru',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Samoan',
                'code' => 'sm',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Sangro',
                'code' => 'sg',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Sanskrit',
                'code' => 'sa',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Scots/Gaelic',
                'code' => 'gd',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Serbian',
                'code' => 'sr',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Serbo-Croatian',
                'code' => 'sh',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Sesotho',
                'code' => 'st',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Setswana',
                'code' => 'tn',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'Shona',
                'code' => 'sn',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Sindhi',
                'code' => 'sd',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Singhalese',
                'code' => 'si',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Siswati',
                'code' => 'ss',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Slovak',
                'code' => 'sk',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Slovenian',
                'code' => 'sl',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Somali',
                'code' => 'so',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Spanish',
                'code' => 'es',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Sundanese',
                'code' => 'su',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Swahili',
                'code' => 'sw',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Swedish',
                'code' => 'sv',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Tagalog',
                'code' => 'tl',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Tajik',
                'code' => 'tg',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Tamil',
                'code' => 'ta',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Tatar',
                'code' => 'tt',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Telugu',
                'code' => 'te',
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Thai',
                'code' => 'th',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Tibetan',
                'code' => 'bo',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Tigrinya',
                'code' => 'ti',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Tonga',
                'code' => 'to',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Tsonga',
                'code' => 'ts',
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Turkish',
                'code' => 'tr',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Turkmen',
                'code' => 'tk',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Twi',
                'code' => 'tw',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Ukrainian',
                'code' => 'uk',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Urdu',
                'code' => 'ur',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Uzbek',
                'code' => 'uz',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Vietnamese',
                'code' => 'vi',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Volapuk',
                'code' => 'vo',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Welsh',
                'code' => 'cy',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Wolof',
                'code' => 'wo',
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Xhosa',
                'code' => 'xh',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Yiddish',
                'code' => 'ji',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Yoruba',
                'code' => 'yo',
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Zulu',
                'code' => 'zu',
            ),
        ));
        
        
    }
}